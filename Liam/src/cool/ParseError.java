package cool;
public class ParseError{
	private int column;
	private String message;
	private int line;
	
	public ParseError(int line,int column,String message){
		this.line = line;
		this.column = column;
		this.message = message;
	}
	void print(){
		System.err.println("ERROR "+ line+" "+ column+ " "+ message);
	}
}