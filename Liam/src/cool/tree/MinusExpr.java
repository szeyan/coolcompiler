package cool.tree;

public class MinusExpr extends BinExpr
{
	public MinusExpr(Expr left, Expr right)
	{
		super(left, right);
	}
}