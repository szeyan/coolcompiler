package cool.tree;

public class NotExpr extends Expr
{
	public final Expr e;
	
	public NotExpr(Expr e){
		super();
		this.e = e;
	}

}