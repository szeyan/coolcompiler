package cool.tree;

public abstract class UnaryExpr extends Expr
{
	public final Expr expr;
	
	protected UnaryExpr(Expr expr){
		super();
		this.expr = expr;
	}
}