package cool.tree;

public class AssignExpr extends Expr
{
	public final Expr e;
	
	public AssignExpr(Expr e){
		super();
		this.e = e;
	}

}