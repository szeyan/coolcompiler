package cool.tree;

public class MatchExpr extends Expr
{
	public final Expr e;
	public final Cases c;
	public MatchExpr(Expr e, Cases c)
	{
		super();
		this.c = c;
		this.e = e;
	}

}