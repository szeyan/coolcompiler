package cool.tree;

public class NegExpr extends Expr
{
	public final Expr e;
	
	public NegExpr(Expr expr)
	{
		super();
		this.e = expr;
	}
	
}