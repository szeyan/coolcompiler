package cool.tree;

import java.util.ArrayList;
import java.util.Arrays;

import beaver.Symbol;

public abstract class Node extends Symbol
{
	public ArrayList<Symbol> symbols;

	public Node(Symbol... s) {
		super();
		this.symbols = new ArrayList<Symbol>(Arrays.asList(s));
	}
}