package cool;
import java.io.*;

import cool.tree.*;
import beaver.Symbol;
public class Driver {
//	static class Accumulator extends TreeWalker
//	{
//		double[] stack = new double[16]; // it's an example only!
//		int top = -1;
//
//		public void visit(ErrExpr expr)
//		{
//			stack[++top] = 0.0; // treat discarded expression as 0
//		}
//		
//		public void visit(NumExpr expr)
//		{
//			stack[++top] = (double) expr.value;
////			System.out.println(stack[top]);
//		}
//
//		public void visit(NegExpr expr)
//		{
//			super.visit(expr);
//			double v = stack[top--];
//			stack[++top] = -v;
////			System.out.println("+/-");
////			System.out.println(stack[top]);
//		}
//
//		public void visit(MultExpr expr)
//		{
//			super.visit(expr);
//			double r = stack[top--];
//			double l = stack[top--];
//			stack[++top] = l * r;
////			System.out.println("*");
////			System.out.println(stack[top]);
//		}
//
//		public void visit(DivExpr expr)
//		{
//			super.visit(expr);
//			double r = stack[top--];
//			double l = stack[top--];
//			stack[++top] = l / r;
////			System.out.println("/");
////			System.out.println(stack[top]);
//		}
//
//		public void visit(PlusExpr expr)
//		{
//			super.visit(expr);
//			double r = stack[top--];
//			double l = stack[top--];
//			stack[++top] = l + r;
////			System.out.println("+");
////			System.out.println(stack[top]);
//		}
//
//		public void visit(MinusExpr expr)
//		{
//			super.visit(expr);
//			double r = stack[top--];
//			double l = stack[top--];
//			stack[++top] = l - r;
////			System.out.println("-");
////			System.out.println(stack[top]);
//		}
//
//		double getResult()
//		{
//			return stack[top];
//		}
//	}
	public static void walk(Node node){
		String value =node.value.toString();
		System.out.print("{\""+ value.substring(value.lastIndexOf(".")+1,value.indexOf("@"))+"\": ");
		for(Symbol symbol : node.symbols ){
			if(symbol != null){
				Object obj = symbol.value;
				
				if(obj != null && !(obj instanceof String)){
					Node node1 = (Node)obj;
					if(node1.getId()< Terminals.NAMES.length){
						System.out.print("\""+ Terminals.NAMES[node1.getId()]+"\": "+node1.value);
					}else{
						
						walk((Node)symbol);
					}
				}else{
					//catches any undeclared tokens that are returned
					if(obj != null && !obj.equals("none")){
						//System.out.print("\""+obj+ "\", ");
					}
				}
			}
		}
		System.out.print("},");
	}
	public static void main( String[] args ) {

		String dirName = null;

		try {
			
			if ( args.length != 0) {
				dirName= args[0];
			}else{
				throw new Error("Usage: <input file path> ");
			}
			
			if ( dirName == null )
				throw new Error( "path not specified" );
			
			FileInputStream fileInputStream = new FileInputStream(	new File( dirName ));
			
			Grammar parser = new Grammar();
			LanguageScanner input =  new LanguageScanner(fileInputStream);
			Object program = parser.parse(input);
			if(!(program instanceof ErrorProgram)){
				Program prog = (Program)program;
				walk(prog);
			}else{
				System.out.println("Error in Program or Parsing, Please Try Another File.");
			}
			
			
		}
		catch ( Exception exception ) {
			System.err.println( "Exception in Main " + exception.toString() );
			exception.printStackTrace();
		}
	}
}