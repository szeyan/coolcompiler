package cool;
import java.io.*;
import beaver.Symbol;
public class LexerDriver {
	
	public static void main( String[] args ) {

		String dirName = null;

		try {
			
			if ( args.length != 0) {
				dirName= args[0];
			}else{
				throw new Error("Usage: <input file path> ");
			}
			
			if ( dirName == null )
				throw new Error( "path not specified" );
			
			FileInputStream fileInputStream = new FileInputStream(	new File( dirName ));
			
			LanguageScanner input =  new LanguageScanner(fileInputStream);
			
			while(true){
				Symbol token = input.nextToken();
				System.out.println("ID:" +token.getId() + " Line:"+ Symbol.getLine(token.getStart())+ " Column:"+ Symbol.getColumn(token.getStart())+ " Value:"+token.value.toString() ); 
				if(token.getId() == Terminals.EOF){
					break;
				}
			}
		}
		catch ( Exception exception ) {
			System.err.println( "Exception in Main " + exception.toString() );
			exception.printStackTrace();
		}
	}
}