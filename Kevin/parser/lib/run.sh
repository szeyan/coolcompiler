# Shortcut for running the Driver program
SRCPATH=cool
LIBPATH=lib
BEAVRT=beaver-rt-0.9.11.jar
DRIVER=cool.Driver

if [ $# -ne 1 ]
then
	echo "Usage: ./run.sh <cool source file>"
	exit
fi

java -cp .:$LIBPATH/$BEAVRT $DRIVER $1
