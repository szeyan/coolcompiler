package cool.ast;

import beaver.Symbol;
import java.util.Vector;
import java.util.Collections;

public class Node extends Symbol {

	public String 	type;
	public Vector<Symbol>	children;

	public Node() {
		children= new Vector<Symbol>();
		type= "empty";
	}

	public Node(String type, Object... args) {
		super();
		this.type= type;
		children= new Vector<Symbol>();
		for (Object o : args) {
			if (o.toString().equals("")) { continue; }			
			if (o instanceof Vector) {
				Vector<Object> nlist= (Vector<Object>) o;
				for (Object elem : nlist) {
					children.add( (Symbol) elem);
				}	
			} else {
				children.add((Symbol) o);
			}
		}
		Symbol[] list= new Symbol[children.size()];
		setPositions(children.toArray(list));	
	}

	public String accept(Traversal t) {
		return t.visit(this);
	}

	public void setPositions(Symbol... args) {
		int basepos= args[0].getStart();
		int rstart= args[0].getLine(basepos);
		int cstart= args[0].getColumn(basepos);		

		int rend=	-1;
		int cend=	-1;

		for (Symbol arg : args) {
			int pos= args[0].getStart();
			int curr_r= args[0].getLine(pos);
			int curr_c= args[0].getColumn(pos);
		
			if (curr_r < rstart) {
				rstart= curr_r;
				cstart= curr_c;
			} else if (curr_r == rstart && curr_c < cstart) {
				cstart= curr_c;
			}

			if (curr_r > rend) {
				rend= curr_r;
				cend= curr_c;
			} else if (curr_r == rend && curr_c > cend) {
				cend= curr_c;
			}
		}

		start= 	makePosition(rstart, cstart);
		end=	makePosition(rend, cend);
	}
}
