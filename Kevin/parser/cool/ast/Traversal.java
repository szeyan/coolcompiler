package cool.ast;

import beaver.Symbol;

public class Traversal {

	private int level= 0;

	public String visit(Node node) {
		String tabs= "";
		for (int j= 0; j < level; j++) { tabs += "  "; }
		String ret= tabs + "{ \"rule\" : \"" + node.type + "\" ";
		ret += ", \"children\" : [ \n";
		level++;

		for (int i= 0; i < node.children.size(); i++) {
			Object child= node.children.get(i);

			if (child instanceof Node) {
				Node n= (Node) child;
				if (n.children.size() > 0) {
					ret += n.accept(this);
				} else {
					continue;
				}
			} else if (child instanceof Symbol) {
				Symbol s= (Symbol) child;
				tabs= "";
				for (int j= 0; j < level; j++) { tabs += "  "; }
				ret += tabs + "{ \"value\" : \"" + s.value + "\" }";
			}
			
			if (i != node.children.size() - 1) {
				ret += " , \n";
			}
		}
		level--;
		
		tabs= "";
		for (int j= 0; j < level; j++) { tabs += "  "; }
		ret += "\n" + tabs + " ] }";
		return ret;
	}

}
