/*
	Driver - Continuously scans a given source
	file for tokenized symbols until EOF.
*/

package cool;

import beaver.Symbol;
import beaver.Scanner;
import beaver.Parser;
import cool.Terminals;
import cool.CoolError;
import cool.Coolscan;
import cool.Coolparser;
import cool.ast.*;
import java.io.*;

public class Driver {
	
	public static void main(String[] args) {
		if (args.length < 1) {
			printUsage();
			System.exit(1);
		}

		// Capture input file and errorcheck		
		FileInputStream src= null;
		Coolscan l= null;
		Coolparser parser= new Coolparser();

		try {
			src= new FileInputStream( args[0] );
			l= new Coolscan(src);
		} catch (FileNotFoundException e) {
			CoolError.onError("[ERR]: File " + args[0] + " not found.\nExiting...");
		}

		Node ast= null;
		try {		
			ast= (Node) parser.parse(l);
		} catch (Exception e) {
			System.out.println( e.toString() );
		}

		Traversal t= new Traversal();
		System.out.println(ast.accept(t));
	}

	// Display file info and scanner key
	public static void printFileInfo(String file) {
		System.out.println(" Source Filename: " + file +
			"\n--------------------------------------------------------");
		System.out.format("%10s %11s  %s\n", "Symbol", "Position", "Value");
		System.out.println("--------------------------------------------------------");
	}


	// Display basic program information
	public static void printIntro() {
		System.out.println("--------------------------------------------------------\n" +
			" :: Driver - COOL SCANNER ::\n" +
			"\tAuthor: Kevin Garsjo\n" +
			"--------------------------------------------------------");
	}


	// Display usage in case of incorrect argcount
	public static void printUsage() {
		System.out.println("Usage: javac Driver <COOL source file>");
	}

}
