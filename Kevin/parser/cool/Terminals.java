package cool;

/**
 * This class lists terminals used by the
 * grammar specified in "Coolparser.grammar".
 */
public class Terminals {
	static public final short EOF = 0;
	static public final short ID = 1;
	static public final short LPAREN = 2;
	static public final short LBRACE = 3;
	static public final short SUPER = 4;
	static public final short NEW = 5;
	static public final short NOT = 6;
	static public final short INTEGER = 7;
	static public final short STRING = 8;
	static public final short BOOLEAN = 9;
	static public final short THIS = 10;
	static public final short MINUS = 11;
	static public final short IF = 12;
	static public final short WHILE = 13;
	static public final short SEMI = 14;
	static public final short TYPE = 15;
	static public final short VAR = 16;
	static public final short RPAREN = 17;
	static public final short COLON = 18;
	static public final short RBRACE = 19;
	static public final short DEF = 20;
	static public final short ASSIGN = 21;
	static public final short NATIVE = 22;
	static public final short COMMA = 23;
	static public final short CASE = 24;
	static public final short ARROW = 25;
	static public final short CLASS = 26;
	static public final short DOT = 27;
	static public final short OVERRIDE = 28;
	static public final short EXTENDS = 29;
	static public final short ELSE = 30;
	static public final short NULL = 31;
	static public final short MATCH = 32;
	static public final short LE = 33;
	static public final short LT = 34;
	static public final short EQUALS = 35;
	static public final short PLUS = 36;
	static public final short TIMES = 37;
	static public final short DIV = 38;
}
