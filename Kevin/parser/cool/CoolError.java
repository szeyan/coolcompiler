package cool;

public class CoolError {

	public static void onError(String message) {
		System.err.println(message);
	}

	public static void onError(Throwable t) {
		System.err.println(t.toString());
	}

}
