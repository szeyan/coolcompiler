package cool;

import beaver.Symbol;
import beaver.Scanner;

import cool.Terminals;

%%

// :: JFLEX / BEAVER SETUP CONFIG
%class Coolscan
%extends Scanner
%function nextToken
%type Symbol
%yylexthrow Scanner.Exception
%unicode
%line
%column
%eofval{
	return new Symbol(Terminals.EOF);
%eofval}


// :: REGEX DEFINITIONS 
illegals=		abstract | catch | do | final | finally | for | forSome | implicit | import | lazy | object | package | private | protected | requres | return | sealed | throw | trait | try | type | val | with | yield

LineTerminator= \r|\n|\r\n
WhiteSpace= 	{LineTerminator} | [ \t\f]

ident=			[a-z][a-zA-Z0-9_]*
type=			[A-Z][a-zA-Z0-9_]*

integer=		{int} | {hex} | {octal}
int=			0 | [1-9][0-9]*
hex=			0[xX][0-9a-fA-F]*
octal=			0[0-7]*
boolean=		true | false


// :: STATE DEFINITIONS
%state comment
%state linecomment
%state string
%state tripstring


// :: JAVA SOURCE LITERAL
%{
	StringBuffer strbuf= new StringBuffer();
%}

%%

<YYINITIAL> {
	// Illegal Keywords
	{illegals}		{ throw new Scanner.Exception("Illegal Symbol: " + yytext()); }

	// Keywords
	"case"			{ return new Symbol(Terminals.CASE, yyline, yycolumn, yytext().length(), yytext()); }
	"class"			{ return new Symbol(Terminals.CLASS, yyline, yycolumn, yytext().length(), yytext()); }
	"def"			{ return new Symbol(Terminals.DEF, yyline, yycolumn, yytext().length(), yytext()); }
	"else"			{ return new Symbol(Terminals.ELSE, yyline, yycolumn, yytext().length(), yytext()); }
	"extends"		{ return new Symbol(Terminals.EXTENDS, yyline, yycolumn, yytext().length(),yytext()); }
	"if"			{ return new Symbol(Terminals.IF, yyline, yycolumn, yytext().length(), yytext()); }
	"match"			{ return new Symbol(Terminals.MATCH, yyline, yycolumn, yytext().length(), yytext()); }
	"native"		{ return new Symbol(Terminals.NATIVE, yyline, yycolumn, yytext().length(), yytext()); }
	"new"			{ return new Symbol(Terminals.NEW, yyline, yycolumn, yytext().length(), yytext()); }
	"override"		{ return new Symbol(Terminals.OVERRIDE, yyline, yycolumn, yytext().length(), yytext()); }
	"super"			{ return new Symbol(Terminals.SUPER, yyline, yycolumn, yytext().length(), yytext()); }
	"this"			{ return new Symbol(Terminals.THIS, yyline, yycolumn, yytext().length(), yytext()); }
	"var"			{ return new Symbol(Terminals.VAR, yyline, yycolumn, yytext().length(), yytext()); }
	"while"			{ return new Symbol(Terminals.WHILE, yyline, yycolumn, yytext().length(), yytext()); }

	// Ignore... 
	{WhiteSpace}	{}
	"//"			{ yybegin(linecomment); }
	"/*"			{ yybegin(comment); }
	

	// Symbols
	"("				{ return new Symbol(Terminals.LPAREN, yyline, yycolumn, yytext().length(), yytext()); }
	")"				{ return new Symbol(Terminals.RPAREN, yyline, yycolumn, yytext().length(), yytext()); }
	"{"				{ return new Symbol(Terminals.LBRACE, yyline, yycolumn, yytext().length(), yytext()); }
	"}"				{ return new Symbol(Terminals.RBRACE, yyline, yycolumn, yytext().length(), yytext()); }
	":"				{ return new Symbol(Terminals.COLON, yyline, yycolumn, yytext().length(), yytext()); }
	";"				{ return new Symbol(Terminals.SEMI, yyline, yycolumn, yytext().length(), yytext()); }
	","				{ return new Symbol(Terminals.COMMA, yyline, yycolumn, yytext().length(), yytext()); }
	"=>"			{ return new Symbol(Terminals.ARROW, yyline, yycolumn, yytext().length(), yytext()); }
	"."				{ return new Symbol(Terminals.DOT, yyline, yycolumn, yytext().length(), yytext()); }
	
	// Operators
	"="				{ return new Symbol(Terminals.ASSIGN, yyline, yycolumn, yytext().length(), yytext()); }
	"/"				{ return new Symbol(Terminals.DIV, yyline, yycolumn, yytext().length(), yytext()); }
	"=="			{ return new Symbol(Terminals.EQUALS, yyline, yycolumn, yytext().length(), yytext()); }
	"<="			{ return new Symbol(Terminals.LE, yyline, yycolumn, yytext().length(), yytext()); }
	"<"				{ return new Symbol(Terminals.LT, yyline, yycolumn, yytext().length(), yytext()); }
	"-"				{ return new Symbol(Terminals.MINUS, yyline, yycolumn, yytext().length(), yytext()); }
	"!"				{ return new Symbol(Terminals.NOT, yyline, yycolumn, yytext().length(), yytext()); }
	"+"				{ return new Symbol(Terminals.PLUS, yyline, yycolumn, yytext().length(), yytext()); }
	"*"				{ return new Symbol(Terminals.TIMES, yyline, yycolumn, yytext().length(), yytext()); }

	// Values
	{integer}		{ return new Symbol(Terminals.INTEGER, yyline, yycolumn, yytext().length(), yytext()); }
	{boolean}		{ return new Symbol(Terminals.BOOLEAN, yyline, yycolumn, yytext().length(), yytext()); }
	"\""			{ strbuf.setLength(0);
						yybegin(string); }
	"\"\"\""		{ strbuf.setLength(0);
						yybegin(tripstring); }
	"null"			{ return new Symbol(Terminals.NULL, yyline, yycolumn, yytext().length(), yytext()); }

	// Identifiers
	{ident}			{ return new Symbol(Terminals.ID, yyline, yycolumn, yytext().length(), yytext()); }
	{type}			{ return new Symbol(Terminals.TYPE, yyline, yycolumn, yytext().length(), yytext()); }
}

<comment> {
	"*/"			{ yybegin(YYINITIAL); }
	{LineTerminator} {}
	[^\r\n]			{}
	<<EOF>>			{ return new Symbol(Terminals.EOF); }
}

<linecomment> {
	"\n"			{ yybegin(YYINITIAL); }
	[^\n]			{}
}

<string> {
	"\""			{ yybegin(YYINITIAL);
						return new Symbol(Terminals.STRING, yyline, yycolumn, strbuf.length(), strbuf); }
	"\\0"			{ strbuf.append("\0"); }
	"\\b"			{ strbuf.append("\b"); }
	"\\t"			{ strbuf.append("\t"); }
	"\\n"			{ strbuf.append("\n"); }
	"\\r"			{ strbuf.append("\r"); }
	"\\\""			{ strbuf.append("\""); }
	"\\"			{ strbuf.append("\\"); }
	[^\0\b\t\n\r\\\"]	{strbuf.append(yytext()); }
}

<tripstring> {
	"\"\"\""		{ yybegin(YYINITIAL);
						return new Symbol(Terminals.STRING, yyline, yycolumn, strbuf.length(), strbuf); }
	"\\0"			{ strbuf.append("\\0"); }
	"\\b"			{ strbuf.append("\\b"); }
	"\\t"			{ strbuf.append("\\t"); }
	"\\n"			{ strbuf.append("\\n"); }
	"\\r"			{ strbuf.append("\\r"); }
	"\\\""			{ strbuf.append("\\\""); }
	"\\"			{ strbuf.append("\\\\"); }
	[^\0\b\t\n\r\\\"]	{strbuf.append(yytext()); }
}
