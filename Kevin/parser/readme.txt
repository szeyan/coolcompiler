C(lassroom) O(bject-) O(riented) L(anguage) Parser
::	Powered by JFlex, Beaver
::
:: Author	- Kevin Garsjo
:: Course	- CS461, UO 13S
:: Created	- 4/23/2013
:: Modified	- 4/23/2013


:: Citations
Melody and I were pseudo-partners for this assignment. We shared ideas
when we ran into trouble and looked at each others' implementations
for comparison. However, everything in this package was ultimately
implemented by myself.


:: Notes
I'm still having parser issues. I managed to resolve the parser not
being able to match the "case" token, but now I have other issues and
I'm at a loss on how to resolve them currently.

I've included a stripped-down test file called _basic.cool to demonstrate
that my AST generation does work via JSON strings, despite my parser's 
problems.


:: This project contains...
parser
|-- cool
|	|-- ast
|	|	|-- Node.java			# The AST Tree Node class
|	|	|-- Traversal.java		# The Visitor Pattern implementation
|	|
|	|-- CoolError.java			# The Cool Error-handling class
|	|-- Driver.java				# The Driver class
|	|-- Terminals.java			# Provided Terminals Class
|
|-- grammar
|	|-- Coolparser.grammar		# The Beaver grammar source file
|	|-- Coolscan.jflex			# The JFlex lexer source file
|
|-- lib
|	|-- beaver-cc-0.9.11.jar	# The Beaver compilation library
|	|-- beaver-rt-0.9.11.jar	# The Beaver Runtime Library
|	|-- JFlex.jar				# The JFlex compilation library
|	|-- run.sh					# Shortcut runner for Driver
|
|-- Makefile					# Makefile for building/cleaning
|-- readme.txt					# This readme
|-- test						# The test cool files
	|-- *.cool


:: How to build...
`make` the provided Makefile.
`make jflex`	- To build only the JFlex Lexer
`make beaver`	- To build only the Beaver Parser
`make java` 	- To compile all java source files
`make clean`	- To remove all make-created files

:: How to run...
`java -cp .:lib/beaver-rt-0.9.11.jar cool.Driver <cool-src>`
`./run.sh <cool-src>`	- Provided for convenience
