
%token	CLASS
%token	EXTENDS
%token	VAR
%token	OVERRIDE
%token	DEF
%token	NATIVE
%token	NEW
%token	NULL
%token	THIS
%token	CASE
%token	MATCH
%token	IF
%token	ELSE
%token	WHILE
%token	SUPER

%token	LPAREN
%token	RPAREN
%token	LBRACE
%token	RBRACE
%token	COLON
%token	COMMA
%token	ASSIGN
%token	SEMI
%token	ARROW
%token	DOT
%token	PLUS
%token	MINUS
%token	TIMES
%token	DIV
%token	LE
%token 	LT
%token	EQUALS
%token	NOT
%token	TYPE
%token	ID
%token	INTEGER
%token	STRING
%token	BOOLEAN
%token	STUB

%%
/* --------------------------------------- //
	<< program >> 
// --------------------------------------- */
program:	classdecl_loop;

classdecl_loop:	classdecl
		|	classdecl_loop classdecl
		;

/*program :	bogus;  Something to use all the terminals  */ 



/* --------------------------------------- //
	<< classdecl >> 
// --------------------------------------- */
classdecl:	CLASS TYPE varformals classbody
		|	CLASS TYPE varformals EXTENDS TYPE actuals classbody
		|	CLASS TYPE varformals EXTENDS NATIVE classbody
		;



/* --------------------------------------- //
	<< varformals >> 
// --------------------------------------- */
varformals:	LPAREN vf_loop RPAREN
		|	LPAREN RPAREN
		;

vf_loop:	vf_unit
		|	vf_unit COMMA vf_loop
		;

vf_unit:		VAR ID COLON TYPE ;



/* --------------------------------------- //
	<< classbody >> 
// --------------------------------------- */
classbody:	LBRACE feat_loop RBRACE
		|	LBRACE RBRACE
		;

feat_loop:	feature
		|	feat_loop feature
		;



/* --------------------------------------- //
	<< feature >> 
// --------------------------------------- */
feature:	feat_funct
		|	feat_var
		|	feat_block
		;

feat_funct:	f_override DEF ID formals COLON TYPE ASSIGN f_enswitch SEMI ; 

f_override:	OVERRIDE
		|	empty
		;

f_enswitch:	expr
		|	NATIVE
		;

feat_var:	VAR ID v_enswitch SEMI ;

v_enswitch:	COLON TYPE ASSIGN expr
		|	ASSIGN NATIVE
		;

feat_block:	LBRACE block RBRACE SEMI ;



/* --------------------------------------- //
	<< formals >> 
// --------------------------------------- */
formals:	LPAREN fo_loop RPAREN
		|	LPAREN RPAREN
		;

fo_loop:	fo_unit
		|	fo_loop COMMA fo_unit
		;

fo_unit:	ID COLON TYPE ;



/* --------------------------------------- //
	<< actuals >> 
// --------------------------------------- */
actuals:	LPAREN act_loop RPAREN
		|	LPAREN RPAREN
		;

act_loop:	expr
		|	act_loop COMMA expr
		;



/* --------------------------------------- //
	<< block >> 
// --------------------------------------- */
block:		block_loop expr
		|	expr
		|	empty
		;

block_loop:	block_unit
		|	block_loop block_unit
		;

block_unit:	expr SEMI
		|	VAR ID COLON TYPE ASSIGN expr SEMI
		;



/* --------------------------------------- //
	<< expr >> 
// --------------------------------------- */
expr:		lexpr primary rexpr ;

lexpr:		empty
		|	lexp_loop
		;

lexp_loop:	lexp_unit
		|	lexp_loop lexp_unit
		;

lexp_unit:	lexp_while
		|	lexp_if
		|	MINUS
		|	NOT
		|	lexp_id
		;

lexp_while:	WHILE LPAREN expr RPAREN ;

lexp_if:	IF LPAREN expr RPAREN expr ELSE ;

lexp_id:	ID ASSIGN ;



/* --------------------------------------- //
	<< rexpr >> 
// --------------------------------------- */
rexpr:		empty
		|	rexp_loop
		;

rexp_loop:	rexp_unit
		|	rexp_loop rexp_unit
		;

rexp_unit:	rexp_id
		|	rexp_match
		|	rexp_ops
		;

rexp_id:	DOT ID actuals ;

rexp_match:	MATCH cases ;

rexp_ops:	rexp_op expr ;

rexp_op:	MINUS
	|		PLUS
	|		DIV
	|		TIMES
	|		EQUALS
	|		LT
	|		LE
	;



/* --------------------------------------- //
	<< primary >> 
// --------------------------------------- */
primary:	THIS
		|	BOOLEAN
		|	STRING
		|	INTEGER
		|	ID
		|	LPAREN RPAREN
		|	NULL
		|	LPAREN expr RPAREN
		|	LBRACE block RBRACE
		|	NEW TYPE actuals
		|	SUPER DOT ID actuals
		|	ID actuals
		;


/* --------------------------------------- //
	<< cases >> 
// --------------------------------------- */
cases:		LBRACE case_loop RBRACE ;
case_loop:	case_unit
		|	case_loop case_unit
		;

case_unit:	CASE ID COLON TYPE ARROW block
		|	CASE NULL ARROW block
		;



/* 
 * Managing the grammar.  Use STUB for parts of the grammar that have 
 * not been filled in yet; the scanner never returns a STUB token. 
 * empty is to make productions with empty 
 * right hand sides clearer.  
 */ 

empty:  ;


/* 
 * While I'm developing the grammar or testing small parts of it, Beaver will complain if I don't
 * have productions using some of the terminal symbols, but I want all of the tokens to be in 
 * Terminals.java for the scanner. So, I create a bogus grammar rule that just gobbles all the tokens
 * I haven't found a place for yet.  This grammar rule will never be matched.  It does make the CFSM ugly, 
 * but it's the cleanest workaround I know for incomplete grammars with complete scanners. 
 */ 
/*bogus : STUB 
		 	CLASS  EXTENDS
			VAR  OVERRIDE  DEF  NATIVE  
			NEW  NULL  THIS  CASE  MATCH  
			IF  ELSE  WHILE  SUPER  
			LPAREN  RPAREN 
			LBRACE  RBRACE 
			COLON  COMMA   	 
		 	ASSIGN  SEMI   
			ARROW  DOT    
			PLUS  MINUS  TIMES  DIV 
			LE  LT  EQUALS  NOT   
			TYPE  ID  
			INTEGER  STRING  BOOLEAN ;
*/
%%
