# Shortcut for running the Driver program
BEAVPTH=lib/Beaver
BEAVRT=beaver-rt-0.9.11.jar
DRIVER=cool.Driver
LLC=llc		# Modify to your path if these don't work
CLANG=clang # Modify to your path if these don't work
CC=gcc		# Modify to your path if these don't work
NATIVEDIR=native

if [ $# -lt 1 ]
then
	echo "Usage: ./run.sh <cool source file>"
	echo "Running compiler over basic.cool definition file..."
fi

java -cp .:$BEAVPTH/$BEAVRT $DRIVER $*

if [ $? -eq 0 ]
then
	$CLANG $NATIVEDIR/main.c -c
	$LLC default.ll -filetype=obj
	$CC main.o default.o
fi
