package cool;

/**
 * A Driver Class for Cool CIS461
 *
 * @author Sze Yan Li
 */
import cool.ast.*;
import cool.codegen.*;
import cool.typeCheck.ClassObj;
import cool.typeCheck.M;
import cool.typeCheck.M.ClassException;
import cool.typeCheck.SymbolStack;
import cool.typeCheck.TypeCheck;
import cool.typeCheck.MasterTable;
import cool.typeCheck.TypeCheck.TypeException;
import java.io.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class Driver {

    public static void main(String[] args) {
        ErrorMaster errorMaster = new ErrorMaster(); //create new master error reporting class
        M m = new M(errorMaster); //create method table
        SymbolStack stack = new SymbolStack(m, errorMaster);

        //add in basic.cool by default
        try {
            String basic = "test/basic.cool";
            CoolScanner scanner = new CoolScanner(new FileReader(basic));

            //set the current scannor error reporting class
            ErrReport err = scanner.getErrReport();
            err.setFileName(basic);
            errorMaster.setCurrER(err);

            CoolParser parser = new CoolParser();
            ProgramNode pgm = (ProgramNode) parser.parse(scanner);
            pgm.setFN(basic);
            m.add(pgm);

            //add current list of classes (and the filename) to the error report class
            Set cl = m.cm.classList.keySet();
            Iterator<String> itr = cl.iterator();
            while (itr.hasNext()) {
                errorMaster.addC2F(itr.next(), basic);
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.exit(1);
        }

        //get each file and check it
        for (int i = 0; i < args.length; i++) {
            try {
                // Get tokens and display each on a line
                CoolScanner scanner = new CoolScanner(new FileReader(args[i]));

                //set the current scannor error reporting class
                ErrReport err = scanner.getErrReport();
                err.setFileName(args[i]);
                errorMaster.setCurrER(err);

                //Create a parser
                CoolParser parser = new CoolParser();
                //retrieve AST from parser
                ProgramNode pgm = (ProgramNode) parser.parse(scanner);
                pgm.setFN(args[i]);

                //walk the ast with a pre-order traversal
                //TreeWalker tw = new TreeWalker(pgm);
                //System.out.println(tw.toString());

                //add classes from this file to the method table
                m.add(pgm);

                //add current list of classes (and the filename) to the error report class
                Set cl = m.cm.classList.keySet();
                Iterator<String> itr = cl.iterator();
                while (itr.hasNext()) {
                    errorMaster.addC2F(itr.next(), args[i]);
                }

            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.exit(1);
            }
        }
        try {
            m.build();
        } catch (ClassException e) {
            e.printStackTrace();
        }
        
        /*
        TreeWalker tw = new TreeWalker(m.getClass("Main").getNode());
        System.out.println(tw.toString()); 
        
        //m.printClasses();
        //m.printAll();
        System.out.println(errorMaster.classtofile.toString());

        //Demonstrate a flattened node
        System.out.println("Original Node: ");
        TreeWalker tw = new TreeWalker(m.getClass("Canvas").getNode());
        //System.out.println(tw.toString());
        Node unflat = (Node) tw.get("FORMALSLIST"); //comment this and uncomment the next 4 lines to see BLOCKLIST demo
        //Node unflat = (Node) tw.get("BLOCKLIST");
        //unflat = (Node) ((Node) unflat.getList().get(0)).getList().get(1);
        //tw.set(unflat);
        //unflat = (Node) tw.get("BLOCKLIST");
        System.out.println(tw.toString(unflat));
        System.out.println("-------------------");
        System.out.println("Flattened Node: ");
        unflat = tw.flatten(unflat);
        System.out.println(tw.toStringFlat(unflat));
        System.out.println("-------------------");
        
        // System.out.println("---");
        //System.out.println(master.toString());
        //System.out.println(master.get(m.getClass("Canvas").getName(), m.getClass("Canvas").getNode()).toString());

        //testing method table:
        //checking Any (native isn't actually a node, so there is no classobj for it)
        //System.out.println(m.getClass("Any").getParentName());
        //see if we can get the parent's name
        //System.out.println(m.getClass("Sieve").getParentName());
        //print the class hierarchy in json (preorder)
        //System.out.println(m.cm.toString());
        //print the class hierarchy as a string array (preorder)
        //System.out.println(m.cm.getClassList().toString());
        //walk a node and print out the tree
        //System.out.println(new TreeWalker(m.getClass("Statistics").getNode()).toString());
        //print all the methods in each class
        //m.printAll();

        // */
        
        //TreeWalker twk= new TreeWalker( m.getClass("Int").getNode() );
        //System.out.println(twk.toString());
        
        //Create master symbol table
        MasterTable master = new MasterTable();

        // Attempt type-checking
        TypeCheck typeChecker = new TypeCheck(m, master, stack, errorMaster);
        try {
            typeChecker.checkType();
            //typeChecker.printAttributes();
            //System.err.println(master.toString()); //err instead of out so i can get output in right order
        } catch (TypeException e) {
            e.printStackTrace();
        } catch (ClassException e) {
            e.printStackTrace();
        }
        // Attempt code generation
        try {
            CodeGenerator codegen = new CodeGenerator(m, master, errorMaster);
            codegen.generate();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        System.out.println("Finished.");
         // Sample llvm code generation
        /*
        try {
            GenerateSampleLLVM llvmgen = new GenerateSampleLLVM(m, master, errorMaster);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }             
        //*/
    }
}
