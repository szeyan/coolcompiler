package cool.typeCheck;

/**
 * This class is part of the type-checking functionality of the Cool Compiler.
 *
 * ClassMaster checks the AST for all classes and ensures the following:
 * //- no cycles (ie: extended class does not extend its subclass)
 * //- no missing nodes (ie: extending a class that doesn't exist)
 * //- no redefinitions (ie: declaring a class multiple times)
 *
 * Constraints of a class:
 * //- a class can be used before it is defined.
 * //- a class can only extend one other class
 * //- all classes by default extends Any()
 *
 * CIS461
 *
 * @author Sze Yan Li
 */
import beaver.Symbol;
import cool.ErrorMaster;
import cool.ast.*;
import cool.typeCheck.ClassObj;
import cool.typeCheck.M.ClassException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class ClassMaster {
    private ErrorMaster errorMaster;
    public KTree hierarchy;
    public LinkedHashMap<String, ClassObj> classList;

    /**
     * Constructor
     */
    public ClassMaster(ErrorMaster errorMaster) {
        this.errorMaster = errorMaster;
        this.classList = new LinkedHashMap<String, ClassObj>();
        this.hierarchy = new KTree();
    }

    /**
     * extracts the classes from a given program and adds the classes to the
     * class hierarchy list
     *
     * all makes sure classes declared are unique
     */
    public void add(ProgramNode node) throws ClassException {
        String fileName = node.getFN();
        String filePath = node.getFP();

        //retrieve the node: ClassList
        TreeWalker tw = new TreeWalker(node);

        ClassListNode clNode = (ClassListNode) tw.get("Classlist");

        for (int i = 0; i < clNode.getList().size(); i++) {
            Node cNode = (Node) clNode.getList().get(i);

            //get class name
            String key = cNode.getType();

            //get extends name 
            TreeWalker tw2 = new TreeWalker(cNode);
            Node exNode = tw2.get("extends");
            String value = exNode.getType();

            //Check usage of extends          
            if (!fileName.equals("basic.cool")) {

                //only IO may be used as a superclass (with exception of Any)
                if (value.equals("Unit")
                        || value.equals("Int")
                        || value.equals("String")
                        || value.equals("Boolean")
                        || value.equals("ArrayAny")
                        || value.equals("Symbol")) {
                    errorMaster.send("Incorrect usage of extends", filePath, "", exNode);
                    throw new ClassException("");
                }
            }

            //create a ClassObj to store this information
            ClassObj co = new ClassObj(key, value, fileName, cNode);

            //make sure key is unique and was not already declared in the list of classes
            if (!classList.containsKey(key)) {
                //add class name and class obj
                classList.put(key, co);
            } else {
                //error, classes declared must be unique
                errorMaster.send("Already a defined class", filePath, key, cNode);
                throw new ClassException("");
            }

        }
    }

    /**
     * checks the class hierarchy if a cycle exists and if all extended classes
     * exist
     */
    public void check() throws ClassException {

        Set keys = classList.keySet();
        Iterator itr = keys.iterator();

        //check to make sure all keys do not contain a cycle and all extended classes exist
        while (itr.hasNext()) {
            String masterKey = (String) itr.next();
            String chkValue = (String) classList.get(masterKey).getParentName();

            //create a list in which to check for loops
            LinkedList checker = new LinkedList<String>();
            checker.add(masterKey);

            //check for a cycle (stop if the extended class is Any or native)
            while (!chkValue.equals("Any") && !chkValue.equals("native")) {
                //check if the chkValue is a declared class (key) in the class list
                if (classList.containsKey(chkValue)) {

                    //check if chkValue has appeared in the list of associated classes so far
                    for (int i = 0; i < checker.size(); i++) {
                        String temp = (String) checker.get(i);

                        if (chkValue.equals(temp)) {
                            //true: we have a cycle
                            errorMaster.send("Declared classes contain a cycle of references", 
                                    "", chkValue, classList.get(chkValue).getNode());
                            throw new ClassException("");
                        }
                    }
                    //add chkValue to the checker List to for next round of checks
                    checker.add(chkValue);

                    //get the super class based on using chkValue as a key (class)
                    chkValue = (String) classList.get(chkValue).getParentName();
                } else {
                    //we have extended a class that hasn't been found in our class list
                    errorMaster.send("Extended class is not found in class list", 
                            "", chkValue, null);
                    throw new ClassException("");
                }
            }
        }

        //hashtable shows that classes are correctly define. create the hierarchy now
        convert();


    }

    /**
     * converts the classlist hashtable to ktree nodes
     */
    public void convert() {
        Queue q = new LinkedList();

        Set keys = classList.keySet();
        Iterator itr = keys.iterator();

        //check to make sure all keys do not contain a cycle and all extended classes exist
        while (itr.hasNext()) {
            String child = (String) itr.next();
            String parent = (String) classList.get(child).getParentName();

            //create tree nodes
            KTree pT = new KTree(parent);
            KTree cT = new KTree(child);

            //add the subclass (child) to the parent node
            pT.add(cT);

            //add the node to the queue
            q.offer(pT);
        }

        //construct the hierarchy tree
        construct(q);

    }

    /**
     * constructs the class hierarchy via a given queue of ktrees
     */
    public void construct(Queue q) {
        //ensures we don't add t1 back into the q if t1 ends up as a child of t2
        boolean addOkay = true;

        while (!q.isEmpty()) {
            addOkay = true;
            KTree t1 = (KTree) q.poll();

            //q is empty after extracting t1 
            if (q.isEmpty()) {
                //we are done constructing our tree
                this.hierarchy = t1;
                return;
            }

            //else: check the rest of the q with t1
            int counter = 0;
            int size = q.size();

            while (counter != q.size()) {
                KTree t2 = (KTree) q.poll();

                //just to make sure t2 is not empty
                if (t2 != null) {
                    //case 1: t1 and t2 are the same class nodes
                    if (t1.getName().equals(t2.getName())) {
                        //combine the two nodes

                        //add all the subclasses of t2 into t1
                        for (int i = 0; i < t2.getList().size(); i++) {
                            KTree t2t = (KTree) t2.getList().get(i);

                            //add the subclass to t1
                            t1.add(t2t);
                        }

                        //done (t1 is offered back outside of this while loop)
                        break;
                    } else {
                        //case 2: t1 is a subclass within t2

                        //create a traversal class for t2
                        KTreeTraverse kt = new KTreeTraverse(t2);

                        KTree tt = kt.get(t1.getName());

                        //t1 is a subclass within t2
                        if (tt != null) {

                            //add all of t1's children to tt
                            for (int i = 0; i < t1.getList().size(); i++) {
                                KTree t1t = (KTree) t1.getList().get(i);

                                //add the subclass to t1
                                tt.add(t1t);
                            }

                            //done
                            addOkay = false;
                            q.offer(t2);
                            break;
                        }

                        //case 3: t1 is a super class of t2

                        //create a traversal class for t1
                        kt = new KTreeTraverse(t1);

                        tt = kt.get(t2.getName());

                        //t2 is a subclass within t1
                        if (tt != null) {

                            //add all of t2's children to tt
                            for (int i = 0; i < t2.getList().size(); i++) {
                                KTree t2t = (KTree) t2.getList().get(i);

                                //add the subclass to t1
                                tt.add(t2t);
                            }

                            //done (t1 is offered back outside of this while loop)
                            break;
                        }

                        //case 4: t1 and t2 are not related
                        q.offer(t2);
                        counter++;
                    }

                }
            }
            //push t1 back into q because we're done checking it
            if (addOkay) {
                q.offer(t1);
            }
        }
    }

    /**
     * @return the list of classes in order of their hierarchy (highest to lowest)
     */
    public LinkedList getClassList() {
        KTreeTraverse ktt = new KTreeTraverse(this.hierarchy);
        return ktt.toList();
    }

    /**
     * @return the class map of class names to class objects
     */
    public LinkedHashMap getClassMap() {
        return this.classList;
    }

    /**
     * @return a String representation of the class hierarchy printed in JSON
     * format
     */
    public String toString() {
        KTreeTraverse kt = new KTreeTraverse(this.hierarchy);
        return kt.toString();
    }

    /**
     * inner class KTree constructs a tree with k-children
     */
    public class KTree {

        private String name;
        private LinkedList children;

        /**
         * Constructor instantiates empty class variables
         */
        public KTree() {
            this("");
        }

        /**
         * Constructor takes in a name and instantiates empty class variables
         */
        public KTree(String name) {
            this.name = name;
            this.children = new LinkedList<KTree>();
        }

        /**
         * adds a KTree as a children of this KTree
         */
        public void add(KTree k) {
            this.children.add(k);
        }

        /**
         * @return the name of the KTree
         */
        public String getName() {
            return this.name;
        }

        /*
         * Retrieves the list of children of the KTree
         */
        public LinkedList getList() {
            return this.children;
        }
    }

    /**
     * inner class KTreeTraverse is used to print, walk, and retrieve elements
     * of a KTree
     */
    public class KTreeTraverse {

        private LinkedList<String> nodeList;
        private int level;
        private KTree master;
        private KTree find;

        /**
         * Constructor takes in a KTree
         */
        public KTreeTraverse(KTree kt) {
            this.master = kt;
            this.find = null;
            this.level = 0;
            this.nodeList = new LinkedList<String>();
        }

        /**
         * Finds a node within the KTree.
         *
         * @param match the node to look for.
         * @return the KTree based on a given name. NULL if not found
         */
        public KTree get(String match) {
            this.find = null;
            get(match, master);
            return find;
        }

        /**
         * Returns the node based on given string and root to walk
         */
        public void get(String match, KTree root) {
            //see if this is the node we're looking for
            if (root != null && root.getName().equals(match)) {
                this.find = root;
                return;
            }

            //loop through the children
            for (int i = 0; i < root.getList().size(); i++) {

                //get the ith child
                KTree check = (KTree) root.getList().get(i);

                //see if child is the node we're looking for
                if (check.getName().equals(match)) {
                    this.find = check;
                    return;
                }

                //hasn't been found, continue traversal
                this.get(match, check);

            }

        }

        /**
         * performs a pre-order (Depth first search) traversal on the tree,
         * saving the walk as a string
         *
         * altered from Kevin Garsjo's Traversal.java
         */
        public String walk(KTree root) {
            String tabs = "";
            for (int j = 0; j < level; j++) {
                tabs += "\t";
            }
            String ret = tabs + "{ \"Node\" : \"" + root.getName() + "\" ";
            ret += ", \"Children\" : [ \n";
            level++;

            for (int i = 0; i < root.getList().size(); i++) {
                KTree child = (KTree) root.getList().get(i);

                //check if child is a leaf node
                if (!child.getList().isEmpty()) {
                    //child is not a leaf, continue traversal
                    ret += this.walk(child);
                } else {
                    tabs = "";
                    for (int j = 0; j < level; j++) {
                        tabs += "\t";
                    }

                    //child is a leaf. print
                    ret += tabs + "{ \"Node\" : \"" + child.getName() + "\" ";
                    ret += ", \"Children\" : [ ] }";
                }

                if (i != root.getList().size() - 1) {
                    ret += " , \n";
                }
            }
            level--;

            tabs = "";
            for (int j = 0; j < level; j++) {
                tabs += "\t";
            }
            ret += "\n" + tabs + " ] }";
            return ret;

        }

        /**
         * @return a string list of the tree in order of hierarchy
         */
        public LinkedList<String> toList() {
            this.nodeList = new LinkedList<String>();
            this.toList(master);
            return nodeList;
        }

        /**
         * adds each node of the ktree to the nodeList
         */
        private void toList(KTree root) {
            this.nodeList.add(root.getName());

            for (int i = 0; i < root.getList().size(); i++) {
                KTree child = (KTree) root.getList().get(i);
                //check if child is a leaf node
                if (!child.getList().isEmpty()) {
                    //child is not a leaf, continue traversal
                    this.toList(child);
                } else {
                    //child is a leaf
                    this.nodeList.add(child.getName());
                }
            }
        }

        /**
         * @return String of the Tree in a pre-order traversal in JSON format
         */
        public String toString() {
            this.level = 0;
            return (this.walk(master));
        }

        /**
         * @return String of the given Tree in a pre-order traversal in JSON
         * format
         */
        public String toString(KTree kt) {
            this.level = 0;
            return (this.walk(kt));
        }
    }
}
