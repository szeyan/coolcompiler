package cool.typeCheck;

import beaver.Symbol;
import cool.ErrorMaster;
import cool.Terminals;
import cool.ast.*;
import cool.typeCheck.M.ClassException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author Kevin Garsjo, Sze Yan Li, Liam Maier
 */
public class TypeCheck {

    public static class TypeException extends Exception {

        public TypeException(String error) {
            super(error);
        }
    }
    private M mtable;
    private MasterTable master;
    private SymbolStack stack;
    private ErrorMaster errorMaster;
    private ClassObj Any = null;
    private ClassObj Nothing = null;
    private ClassObj Unit = null;
    private ClassObj Null = null;
    private ClassObj Boolean = null;
    private ClassObj Integer = null;
    private ClassObj String = null;
    private ClassObj Unhandled = new ClassObj("Unhandled");
    private ClassObj Native = new ClassObj("Any");   // TODO Reset after test

    public TypeCheck(M mtable, MasterTable master, SymbolStack stack, ErrorMaster errorMaster) {
        this.mtable = mtable;
        this.master = master;
        this.stack = stack;
        this.errorMaster = errorMaster;
        Any = mtable.getClass("Any");
        Nothing = new ClassObj("Nothing");
        Unit = mtable.getClass("Unit");
        Null = new ClassObj("Null");   // TODO Reset after Test
        Boolean = mtable.getClass("Boolean");
        Integer = mtable.getClass("Int");
        String = mtable.getClass("String");
    }

    public void checkType() throws TypeException, ClassException {
        checkMethods();
    }
    //NOTE this actually checks classes and their methods

    public void checkMethods() throws TypeException, ClassException {
        Collection<ClassObj> classes = mtable.getClasses().values();

        for (ClassObj cls : classes) {
            stack.pushClone();

            //add to stack and master table
            VariableObj va = new VariableObj(cls.getName(), cls, cls, cls.getNode());
            stack.addToCurrent("this", va);
            master.addRef(va);

            //check varformals and attributes of the class
            getAndCheckAttributes(cls.getName(), cls.getNode());

            //check the methods of the class
            Collection<MethodObj> methods = cls.getMethods().values();
            for (MethodObj method : methods) {
                stack.pushClone();

                //get return type and all parameters of this method
                ClassObj rettype = mtable.getClass(method.getReturnType());
                LinkedHashMap<String, String> params = method.getParams();

                //if method is a constructor skip it if it's not defined in actual code
                if (method.getName().equals(cls.getName()) && method.getNode() == null) {
                    stack.pop();
                    continue;
                }

                //if method was inherited skip it (it'll be checked in the super class)
                if (method.isInherited()) {
                    stack.pop();
                    continue;
                }

                //check the parameters of the method
                for (String param : params.keySet()) {
                    va = new VariableObj(param, mtable.getClass(params.get(param)), cls, method.getNode());
                    stack.addToCurrent(true, param, va);
                    master.addRef(va);
                }

                //check the body method to confirm its return type
                Symbol body = (Symbol) method.getNode().getList().get(3);

                if (body.value.equals("native")) {
                    stack.pop();
                    continue;
                }
                checkType(rettype, (Node) body, cls.getName());

                stack.pop();
            }
            stack.pop();
        }
    }

    public void getAndCheckAttributes(String className, Node classnode) throws TypeException, ClassException {

        //Inherit attributes from parent class
        //Inherit varformals from parent class
        ClassObj curr = mtable.getClass(className);
        //skip native because it's not actually a class
        if (!curr.getParentName().equals("native")) {
            //get parent obj
            ClassObj pClass = (ClassObj) mtable.getClass(curr.getParentName());

            //get parent methods
            LinkedHashMap pAttrib = pClass.getAttributes();
            Iterator<VariableObj> itr1 = pAttrib.values().iterator();
            while (itr1.hasNext()) {
                VariableObj va = itr1.next();
                va.setInherited(true);
                curr.getAttributes().put(va.name, va);
            }

            //get parent varformals
            LinkedHashMap pVarformals = pClass.getVarformals();
            Iterator<VariableObj> itr2 = pVarformals.values().iterator();
            while (itr2.hasNext()) {
                VariableObj va = itr2.next();
                va.setInherited(true);
                curr.getVarformals().put(va.name, va);
            }
        }

        //Get the varformals (class declaration variables)
        LinkedList varformals = new TreeWalker(classnode).filter("VARFORMALS");
        for (int j = 0; j < varformals.size(); j++) {
            Node varformalsNode = (Node) varformals.get(j);

            //get the id and type (the formal param)
            Symbol id = (Symbol) varformalsNode.getList().get(0);
            String ident = id.value.toString();
            Symbol tp = (Symbol) varformalsNode.getList().get(1);
            String type = tp.value.toString();

            VariableObj va = new VariableObj(ident, mtable.getClass(type), mtable.getClass(className), varformalsNode);
            mtable.getClass(className).addVarformal(va.name, va, errorMaster);
            stack.addToCurrent(ident, va);
            master.addRef(va);
        }

        // Parse and capture classbody feature declarations
        Node features = (Node) classnode.getList().get(3);
        if (features instanceof FeatureListNode) {
            for (Object obj : features.getList()) {
                FeatureNode feature = (FeatureNode) obj;
                Symbol id = (Symbol) feature.getList().get(0);


                //get the attribute declarations
                if (id != null && id.getId() == Terminals.ID) {
                    Node vbody = (Node) feature.getList().get(1);
                    if (vbody.getList().size() > 1) {   // Distinguish between native assn and actual type
                        String type = (String) ((Symbol) vbody.getList().get(0)).value;
                        ExprNode e = (ExprNode) vbody.getList().get(1);

                        ClassObj t1 = mtable.getClass(type);
                        ClassObj t2 = eval(e, className);

                        if (!conformsTo(t2, t1)) {
                            errorMaster.send("Variable declaration for " + id.toString() + " invalid for type " + t2.getName(), "", className, e);
                            throw new TypeException("");
                        }
                        VariableObj va = new VariableObj(id.value.toString(), mtable.getClass(type), mtable.getClass(className), feature);
                        stack.addToCurrent(id.value.toString(), va);
                        mtable.getClass(className).addAttribute(va.name, va, errorMaster);
                        master.addRef(va);

                    } else {
                        VariableObj va = new VariableObj(id.value.toString(), Native, mtable.getClass(className), feature);
                        stack.addToCurrent(id.value.toString(), va);

                        mtable.getClass(className).addAttribute(va.name, va, errorMaster);
                        master.addRef(va);

                    }

                } else {
                    //this is for anything that's not a method (methods are checked via another place)
                    if (!(id instanceof MethodNode) && !id.value.equals("override")) {                        
                        eval(id, className);
                        mtable.getClass(className).addExecutionBlock((Node) id);
                    }
                }
            }
        }
        //end of the class pop your local stuff!
    }

    public void checkType(ClassObj expected, Node body, String className) throws TypeException {
        ClassObj actual = eval(body, className);

        if (!expected.equals(actual) && !this.conformsTo(actual, expected)) {
            errorMaster.send("Type " + expected.getName() + " did not match type " + actual.getName()
                    + " evaluated from body " + body.toString(), "", className, body);
            throw new TypeException("");
        }
    }

    // TODO Do actual evaluation
    public ClassObj eval(Symbol body, String className) throws TypeException {

        // Edge case, null node
        if (body == null) {
            return null;
        }

        // Use terminal IDs to type literals
        int term = body.getId();

        // Use node child lists to type complex expressions
        LinkedList children = null;
        Node currNode = null;


        // Literals and Other Easy Cases
        switch (term) {
            case Terminals.BOOLEAN:
                return Boolean;
            case Terminals.INTEGER:
                return Integer;
            case Terminals.NULL:
                return Null;
            case Terminals.STRING:
                return String;
            case Terminals.ID:
            case Terminals.THIS:

                //look up current class to see if it's an inherited varformal
                VariableObj t0 = mtable.getClass(className).getVarformal(body.value.toString());
                if (t0 != null && t0.isInherited()) {
                    return t0.type;
                }

                //look up current class to see if it's an inherited attribute
                VariableObj t1 = mtable.getClass(className).getAttribute(body.value.toString());
                if (t1 != null && t1.isInherited()) {
                    return t1.type;
                }

                //look up stack to see if it's a variable declared
                VariableObj t2 = stack.lookupCurrent((String) body.value);
                if (t2 != null) {
                    return stack.lookupCurrent((String) body.value).type;
                }

                System.err.println(stack.toString());
                errorMaster.send("Identifer not found in symbol table for \"" + body.value + "\"", "", className, body);
                throw new TypeException("");

            case Terminals.NATIVE:
                return Any;
        }


        // Binary Arithmetic Expression Subtypes
        if (body instanceof PlusNode // Plus Expr
                || body instanceof MinusNode // Minus Expr
                || body instanceof TimesNode // Mult Expr
                || body instanceof DivideNode) {   // Div Expr
            currNode = (Node) body;

            children = currNode.getList();
            ClassObj t1 = eval((Symbol) children.get(0), className);
            ClassObj t2 = eval((Symbol) children.get(1), className);
            if (t1.equals(Integer) && t2.equals(Integer)) {
                return Integer;
            }

            errorMaster.send("Arithmetic undefined for " + t1.getName() + " and " + t2.getName(), "", className, currNode);
            throw new TypeException("");
        }


        // Binary Boolean Expression Subtypes
        if (body instanceof LessThanNode // Less Than
                || body instanceof LessEqualNode) {    // Less than or Equals
            currNode = (Node) body;
            children = currNode.getList();
            ClassObj t1 = eval((Symbol) children.get(0), className);
            ClassObj t2 = eval((Symbol) children.get(1), className);
            if (t1.equals(Integer) && t2.equals(Integer)) {
                return Boolean;
            }
            errorMaster.send("Relationals undefined for " + t1.getName() + " and " + t2.getName(), "", className, currNode);
            throw new TypeException("");
        }


        // Binary Equality Type
        if (body instanceof EqualNode) {
            currNode = (Node) body;

            children = currNode.getList();
            ClassObj t1 = eval((Symbol) children.get(0), className);
            ClassObj t2 = eval((Symbol) children.get(1), className);

            //check for conformity incase it's Class == Class

            if (t1.equals(t2)) {
                if (t1.equals(Boolean) || t1.equals(Integer) || t1.equals(String)) {
                    return Boolean;
                } else if (this.conformsTo(t1, t2)) {
                    //Melody: another hackish fix for L244 in GFX.cool
                    return Boolean;
                }
            }
            errorMaster.send("Equality undefined for " + t1.getName() + " and " + t2.getName(), "", className, currNode);
            throw new TypeException("");
        }


        // Unary Expression Subtypes
        if (body instanceof NotNode) {          // Not Expr
            currNode = (Node) body;
            ClassObj t1 = eval((Symbol) currNode.getList().get(0), className);
            if (t1.equals(Boolean)) {
                return Boolean;
            }

            errorMaster.send("Boolean negation undefined for " + t1.getName(), "", className, currNode);
            throw new TypeException("");
        }
        if (body instanceof UnaryMinusNode) {    // Integer Negation Expr
            currNode = (Node) body;
            ClassObj t1 = eval((Symbol) currNode.getList().get(0), className);
            if (t1.equals(Integer)) {
                return Integer;
            }

            errorMaster.send("Integer negation undefined for " + t1.getName(), "", className, currNode);
            throw new TypeException("");
        }


        // Control Expression Subtype
        if (body instanceof IfElseNode) {       // If Statement
            currNode = (Node) body;
            children = currNode.getList();
            ClassObj t1 = eval((Symbol) children.get(0), className);
            ClassObj t2 = eval((Symbol) children.get(1), className);
            ClassObj t3 = eval((Symbol) children.get(2), className);

            if (t1.equals(Boolean)) {
                return join(t2, t3);
            }
            errorMaster.send("If condition undefined for type " + t1.getName(), "", className, currNode);
            throw new TypeException("");
        }
        if (body instanceof WhileNode) {        // While Loop
            currNode = (Node) body;
            children = currNode.getList();
            ClassObj t1 = eval((Symbol) children.get(0), className);
            ClassObj t2 = eval((Symbol) children.get(1), className);
            if (t1.equals(Boolean)) {
                // TODO Check some specific type rules for LOOP
                return Unit;
            }
            errorMaster.send("While condition undefined for type " + t1.getName(), "", className, currNode);
            throw new TypeException("");
        }


        // Dot Access Expression Subtype
        if (body instanceof DotNode) {



            currNode = (Node) body;
            children = currNode.getList();
            ClassObj cls = eval((Symbol) children.get(0), className);
            Symbol member = (Symbol) children.get(1);

            if (member instanceof PrimaryNode) {
                currNode = (Node) member;
                children = currNode.getList();
                if (children.size() == 2) {         // Static Method Dispatch

                    // Fetch method, continue if not null
                    String id = (String) ((Symbol) children.get(0)).value;
                    MethodObj method = cls.getMethod(id);
                    if (method != null) {
                        LinkedHashMap<String, String> params = method.getParams();
                        LinkedList<Node> given = new LinkedList<Node>();

                        //flatten the actualslist                      
                        Node acl_list = (new TreeWalker((Node) children.get(1))).flatten((Node) children.get(1));

                        //get each actual and check and get its type
                        LinkedList<ClassObj> args = new LinkedList<ClassObj>();
                        LinkedList aChilds = acl_list.getFlatList();

                        //the following redundant cases exist for the sake of readability                       
                        //if the only argument is not actualslistnode then just evaluate it
                        if (acl_list instanceof DotNode) {
                            args.add(eval((Symbol) acl_list, className));
                        } else if (acl_list instanceof PrimaryNode) {
                            //new case
                            args.add(eval((Symbol) acl_list, className));
                        } else if (acl_list instanceof ActualsListNode) {
                            //for each child, check get its type                     
                            for (Object o : aChilds) {
                                //child could be either primarynode
                                if (o instanceof PrimaryNode) {
                                    ClassObj temp = (ClassObj) eval((Symbol) o, className);
                                    args.add(temp);
                                } else {
                                    //could be single argument with just ID
                                    //or it could be a dotnode
                                    args.add(eval((Symbol) o, className));
                                }
                            }
                        } else {
                            //could be some other node (minus, plus, etc)
                            args.add(eval((Symbol) acl_list, className));
                        }

                        /*
                         if (className.equals("ArrayList")) {
                         System.err.println(new TreeWalker(currNode).toString());
                         //System.err.println(new TreeWalker(acl_list).toStringFlat(acl_list));
                         System.err.println("Given: "+args.size() + " Children: " + aChilds.size()+" Expected: "+params.size());
                         }// */

                        //check the args conform to the parameters of the method
                        if (args.size() == params.size()) {
                            int i = 0;
                            for (String t2str : params.values()) {
                                ClassObj t1 = args.get(i);
                                ClassObj t2 = mtable.getClass(t2str);
                                if (!conformsTo(t1, t2)) {
                                    errorMaster.send("Invalid parameters for " + method.getName(), "", className, currNode);
                                    throw new TypeException("");
                                }
                                i++;
                            }

                            //just in case
                            //check Nothing case
                            if (method.getReturnType().equals("Nothing")) {
                                return Nothing;
                            }


                            return mtable.getClass(method.getReturnType());

                        } else {

                            errorMaster.send("Invalid parameters for " + method.getName(), "", className, currNode);
                            throw new TypeException("");
                        }
                    }
                } else {                            // Attribute Access
                    // Not sure what do
                }
            }

            errorMaster.send("Unknown member access for " + member.value, "", className, currNode);
            throw new TypeException("");
        }


        if (body instanceof MatchNode) {            // Match 
            currNode = (Node) body;
            children = currNode.getList();

            // Parse out all the case nodes
            Node head = (CasesListNode) children.get(1);
            LinkedList<CasesNode> cases = new LinkedList<CasesNode>();
            while (!(head instanceof CasesNode)) {
                cases.addFirst((CasesNode) head.getList().get(1));
                head = (Node) head.getList().get(0);
            }
            cases.addFirst((CasesNode) head);

            // Evaluate all case expressions for their types,
            // accounting for the local declarations
            Stack<ClassObj> classStack = new Stack<ClassObj>();
            for (CasesNode node : cases) {
                // If match has its own arguments, load them and evaluate
                if (node.getList().size() == 4) {
                    String name = (String) ((Symbol) node.getList().get(1)).value;
                    String type = (String) ((Symbol) node.getList().get(2)).value;
                    stack.pushClone();

                    VariableObj va = new VariableObj(name, mtable.getClass(type), mtable.getClass(className), node);
                    stack.addToCurrent(true, name, va);
                    master.addRef(va);

                    classStack.push(eval((Symbol) node.getList().get(3), className));
                    stack.pop();
                    // If match's params are null, just evaluate as-is
                } else {
                    classStack.push(eval((Symbol) node.getList().get(2), className));
                }
            }

            // Apply join to all stack pairs
            while (classStack.size() > 1) {
                classStack.push(join(classStack.pop(), classStack.pop()));
            }

            return classStack.pop();

        }


        // Block Node Types
        if (body instanceof BlockListNode) {
            currNode = (Node) body;
            children = currNode.getList();
            if (children.size() == 0) {         // Empty Block
                return Unit;
            }

            if (className.equals("Canvas")) {
                //System.out.println(new TreeWalker(currNode).toString());
                //System.exit(1);
            }

            // ------------------------------>  // Block Multi-Expr

            LinkedList<Symbol> leaves = new LinkedList<Symbol>();
            Node head = (BlockListNode) body;
            Symbol last = null;
            while (head instanceof BlockListNode) {
                leaves.addFirst((Symbol) head.getList().get(1));
                try {
                    head = (Node) head.getList().get(0);
                } catch (Exception e) {
                    last = (Symbol) head;
                    break;
                }
            }
            if (last == null) {
                leaves.addFirst((Symbol) head);
            } else {
                leaves.addFirst(last);
            }

            stack.pushClone();
            ClassObj retclass = null;
            for (Symbol snode : leaves) {
                if (snode instanceof BlockNode) {
                    BlockNode bnode = (BlockNode) snode;
                    String ident = (String) ((Symbol) bnode.getList().get(0)).value;
                    String type = (String) ((Symbol) bnode.getList().get(1)).value;
                    ClassObj t1 = mtable.getClass(type);
                    ClassObj t2 = eval((Symbol) bnode.getList().get(2), className);
                    if (!conformsTo(t2, t1)) {
                        errorMaster.send("Local variable assignment problem, cannot cast " + t2.getName() + " to " + t1.getName(), "", className, bnode);
                        throw new TypeException("");
                    }

                    if (className.equals("Canvas")) {
                        //System.out.println(ident + " " + className);
                    }

                    VariableObj va = new VariableObj(ident, mtable.getClass(type), mtable.getClass(className), bnode);
                    stack.addToCurrent(ident, va);
                    master.addRef(va);

                    retclass = t1;
                } else {
                    retclass = eval(snode, className);
                }
            }

            stack.pop();
            return retclass;

        }
        // Primary Node Types
        if (body instanceof PrimaryNode) {
            currNode = (Node) body;

            children = currNode.getList();
            if (children.size() == 0) {
                return Unit;
            }  // Empty LPAREN / RPAREN pair

            Symbol s1 = (Symbol) children.get(0);
            if (children.size() == 1) {
                return eval(s1, className);
            }

            if (s1.getId() == Terminals.NEW) {       // New 
                String type = ((Symbol) children.get(1)).value.toString();
                if (type.equals("Any") || type.equals("Unit") || type.equals("IO")) {   // Handle Declarable Native Types
                    if (mtable.getClass(type) != null) {
                        return mtable.getClass(type);
                    }
                }
                if (!type.equals("Any") && !type.equals("Integer")
                        && !type.equals("Unit") && !type.equals("Boolean")
                        && !type.equals("String") && !type.equals("IO")) {
                    if (mtable.getClass(type) == null) {
                        errorMaster.send("Cannot instantiate class " + type, "", className, s1);
                        throw new TypeException("");
                    }

                    //get the class type
                    ClassObj toMake = mtable.getClass(type);

                    //get constructor method
                    MethodObj constructor = toMake.getConstructor();

                    if (constructor != null) {
                        LinkedHashMap<String, String> params = constructor.getParams();
                        LinkedList<Node> given = new LinkedList<Node>();

                        //flatten the actualslist                      
                        Node acl_list = (new TreeWalker((Node) children.get(2))).flatten((Node) children.get(2));

                        //get each actual and check and get its type
                        LinkedList<ClassObj> args = new LinkedList<ClassObj>();
                        LinkedList aChilds = acl_list.getFlatList();

                        //the following redundant cases exist for the sake of readability                       
                        //if the only argument is not actualslistnode then just evaluate it
                        if (acl_list instanceof DotNode) {
                            args.add(eval((Symbol) acl_list, className));
                        } else if (acl_list instanceof PrimaryNode) {
                            //new case
                            args.add(eval((Symbol) acl_list, className));
                        } else if (acl_list instanceof ActualsListNode) {
                            //for each child, check get its type                     
                            for (Object o : aChilds) {
                                //child could be either primarynode
                                if (o instanceof PrimaryNode) {
                                    ClassObj temp = (ClassObj) eval((Symbol) o, className);
                                    args.add(temp);
                                } else {
                                    //could be single argument with just ID
                                    //or it could be a dotnode
                                    args.add(eval((Symbol) o, className));
                                }
                            }
                        } else {
                            //could be some other node (minus, plus, etc)
                            args.add(eval((Symbol) acl_list, className));
                        }

                        //check the args conform to the parameters of the method
                        if (args.size() == params.size()) {
                            int i = 0;
                            for (String t2str : params.values()) {
                                ClassObj t1 = args.get(i);
                                ClassObj t2 = mtable.getClass(t2str);
                                if (!conformsTo(t1, t2)) {
                                    errorMaster.send("Invalid parameters for " + constructor.getName(), "", className, currNode);
                                    throw new TypeException("");
                                }
                                i++;
                            }
                            //check Nothing case
                            if (constructor.getReturnType().equals("Nothing")) {
                                return Nothing;
                            }

                            return mtable.getClass(constructor.getReturnType());

                        } else {

                            errorMaster.send("Invalid parameters for " + constructor.getName(), "", className, currNode);
                            throw new TypeException("");
                        }
                    } else {
                        errorMaster.send("Cannot instantiate class " + type, "", className, s1);
                        throw new TypeException("");
                    }
                }

            }

            if (s1.getId() == Terminals.SUPER) {    // Superclass Dispatch
                //Melody's attempt
                String mname = (String) s1.value;

                //grab the parent class from this current class 
                ClassObj ths = mtable.getClass(stack.lookupCurrent("this").thisClass.getParentName());

                MethodObj method = mtable.getMethod(ths.getName(), mname);
                if (method != null) {
                    LinkedHashMap<String, String> params = method.getParams();
                    LinkedList<Node> given = new LinkedList<Node>();

                    //flatten the actualslist                      
                    Node acl_list = (new TreeWalker((Node) children.get(1))).flatten((Node) children.get(1));

                    //get each actual and check and get its type
                    LinkedList<ClassObj> args = new LinkedList<ClassObj>();
                    LinkedList aChilds = acl_list.getFlatList();

                    //the following redundant cases exist for the sake of readability                       
                    //if the only argument is not actualslistnode then just evaluate it
                    if (acl_list instanceof DotNode) {
                        args.add(eval((Symbol) acl_list, className));
                    } else if (acl_list instanceof PrimaryNode) {
                        //new case
                        args.add(eval((Symbol) acl_list, className));
                    } else if (acl_list instanceof ActualsListNode) {
                        //for each child, check get its type                     
                        for (Object o : aChilds) {
                            //child could be either primarynode
                            if (o instanceof PrimaryNode) {
                                ClassObj temp = (ClassObj) eval((Symbol) o, className);
                                args.add(temp);
                            } else {
                                //could be single argument with just ID
                                //or it could be a dotnode
                                args.add(eval((Symbol) o, className));
                            }
                        }
                    } else {
                        //could be some other node (minus, plus, etc)
                        args.add(eval((Symbol) acl_list, className));
                    }

                    /*
                     if (className.equals("Canvas")) {
                     System.err.println(new TreeWalker(currNode).toString());
                     //System.err.println(new TreeWalker(acl_list).toStringFlat(acl_list));
                     System.err.println("Given: " + args.size() + " Children: " + aChilds.size() + " Expected: " + params.size());
                     }// */

                    //check the args conform to the parameters of the method
                    if (args.size() == params.size()) {
                        int i = 0;
                        for (String t2str : params.values()) {
                            ClassObj t1 = args.get(i);
                            ClassObj t2 = mtable.getClass(t2str);
                            if (!conformsTo(t1, t2)) {
                                errorMaster.send("Invalid parameters for " + method.getName(), "", className, currNode);
                                throw new TypeException("");
                            }
                            i++;
                        }
                        //check Nothing case
                        if (method.getReturnType().equals("Nothing")) {
                            return Nothing;
                        }

                        return mtable.getClass(method.getReturnType());

                    } else {

                        errorMaster.send("Invalid parameters for " + method.getName(), "", className, currNode);
                        throw new TypeException("");
                    }
                }


            }

            if (s1.getId() == Terminals.ID) {       // Regular Dispatch
                String mname = (String) s1.value;
                ClassObj ths = stack.lookupCurrent("this").type;
                MethodObj method = mtable.getMethod(ths.getName(), mname);
                if (method != null) {
                    LinkedHashMap<String, String> params = method.getParams();
                    LinkedList<Node> given = new LinkedList<Node>();

                    //flatten the actualslist                      
                    Node acl_list = (new TreeWalker((Node) children.get(1))).flatten((Node) children.get(1));

                    //get each actual and check and get its type
                    LinkedList<ClassObj> args = new LinkedList<ClassObj>();
                    LinkedList aChilds = acl_list.getFlatList();

                    //the following redundant cases exist for the sake of readability                       
                    //if the only argument is not actualslistnode then just evaluate it
                    if (acl_list instanceof DotNode) {
                        args.add(eval((Symbol) acl_list, className));
                    } else if (acl_list instanceof PrimaryNode) {
                        //new case
                        args.add(eval((Symbol) acl_list, className));
                    } else if (acl_list instanceof ActualsListNode) {
                        //for each child, check get its type                     
                        for (Object o : aChilds) {
                            //child could be either primarynode
                            if (o instanceof PrimaryNode) {
                                ClassObj temp = (ClassObj) eval((Symbol) o, className);
                                args.add(temp);
                            } else {
                                //could be single argument with just ID
                                //or it could be a dotnode
                                args.add(eval((Symbol) o, className));
                            }
                        }
                    } else {
                        //could be some other node (minus, plus, etc)
                        args.add(eval((Symbol) acl_list, className));
                    }

                    /*
                     if (className.equals("Canvas")) {
                     System.err.println(new TreeWalker(currNode).toString());
                     //System.err.println(new TreeWalker(acl_list).toStringFlat(acl_list));
                     System.err.println("Given: " + args.size() + " Children: " + aChilds.size() + " Expected: " + params.size());
                     }// */

                    //check the args conform to the parameters of the method
                    if (args.size() == params.size()) {
                        int i = 0;
                        for (String t2str : params.values()) {
                            ClassObj t1 = args.get(i);
                            ClassObj t2 = mtable.getClass(t2str);
                            if (!conformsTo(t1, t2)) {
                                errorMaster.send("Invalid parameters for " + method.getName(), "", className, currNode);
                                throw new TypeException("");
                            }
                            i++;
                        }
                        //check Nothing case
                        if (method.getReturnType().equals("Nothing")) {
                            return Nothing;
                        }

                        return mtable.getClass(method.getReturnType());

                    } else {

                        errorMaster.send("Invalid parameters for " + method.getName(), "", className, currNode);
                        throw new TypeException("");
                    }
                }

            }
        }

        // TODO Block-Expr   // -- Melody: ??

        // TODO Attribute Definition -- Melody: I think it's already defined up in getAndCheckAttributes
        // TODO Method Override Dispatch -- Melody: unless it's completely inherited, it is actually being checked as a regular method

        // Undefined Node Subtype
        return Unit;    // Probably should not do this, and should be Unhandled. Ye have been warned.
    }

    // TODO Implement conformation checking as described in the COOL manual
    public boolean conformsTo(ClassObj c1, ClassObj c2) {
        if (c1 == null || c2 == null) {
            return false;
        }

        if (c1.equals(Null)) {
            return true;
        }

        if (c2.equals(Any)) {
            return true;
        }

        //Melody: Handling Nothing
        if (c1.equals(Nothing)) {
            return true;
        }

        while (!c1.equals(Any)) {
            if (c1.equals(c2)) {
                return true;
            }
            c1 = mtable.getClass(c1.getParentName());
        }

        return false;
    }

    // TODO Implement join checking as described in the COOL Manual
    public ClassObj join(ClassObj c1, ClassObj c2) {
        HashSet<ClassObj> seen = new HashSet<ClassObj>();

        /*if (c1.equals(Unit) && c2.equals(Unit)) {
         return c1;
         }*/

        if (c2.equals(Unit)) {
            return c1;
        }

        if (c1.equals(Unit)) {
            return c2;
        }

        //Melody: hackish fix for while (!(p == pend)) in GFX
        if (c1.equals(Nothing)) {
            return c2;
        }

        if (c2.equals(Nothing)) {
            return c1;
        }

        while (true) {
            if (seen.contains(c1) && !Any.equals(c1)) {
                return c1;
            }
            seen.add(c1);
            if (!Any.equals(c1)) {
                c1 = mtable.getClass(c1.getParentName());
            }

            if (seen.contains(c2) && !Any.equals(c2)) {
                return c2;
            }
            seen.add(c2);
            if (!Any.equals(c2)) {
                c2 = mtable.getClass(c2.getParentName());
            }

            if (c1.equals(c2)) {
                return c1;
            }
        }

    }

    //prints all the attributes within each class
    public void printAttributes() {
        Collection classes = mtable.getClasses().values();
        Iterator itr = classes.iterator();
        while (itr.hasNext()) {
            ClassObj co = (ClassObj) itr.next();
            System.out.println("Class: " + co.getName());
            System.out.println("\t Attributes: " + co.getAttributes().toString());
        }
    }
}
