/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.typeCheck;

import cool.ErrorMaster;
import cool.ast.Node;
import cool.typeCheck.TypeCheck.TypeException;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author kgarsjo
 */
public class SymbolStack {
    private ErrorMaster errorMaster;
    public M mtable;
    public Stack<SymbolTable> stack = new Stack<SymbolTable>();

    public SymbolStack(M mtable, ErrorMaster errorMaster) {
        this.mtable = mtable;
        this.errorMaster = errorMaster;
        stack.push(new SymbolTable(mtable, errorMaster));
    }

    public void push(SymbolTable table) {
        stack.push(table);
    }

    public void pushClone() throws TypeException{
        if (stack.peek() == null) {
            return;
        }

        SymbolTable curr = stack.peek();
        SymbolTable next = new SymbolTable(mtable, errorMaster);
        Set<String> keys = curr.table.keySet();
        for (String key : keys) {
            VariableObj v = curr.lookup(key);
            //name, type, class, node
            VariableObj add = next.add(key, v);
        }

        stack.push(next);
    }

    public SymbolTable pop() {
        return stack.pop();
    }
    public void addToCurrent(Boolean suppressError, String name, VariableObj va)throws TypeException {
        SymbolTable curr = stack.peek();
        curr.add(suppressError,name, va);

    }
    public void addToCurrent(String name, VariableObj va)throws TypeException {
        SymbolTable curr = stack.peek();
        curr.add(name, va);
    }

    public VariableObj lookupCurrent(String name) {
        SymbolTable curr = stack.peek();
        return curr.lookup(name);
    }
    
    @Override
    public String toString(){
         SymbolTable curr = stack.peek();
         if(curr != null){
             return curr.toString();
         }
        return null;
    }
    
}
