/*
 * Master symbol table which stores all references for a COOL program
 */
package cool.typeCheck;

import cool.ast.Node;
import java.util.LinkedList;

/**
 *
 * @author Sze Yan Li
 */
public class MasterTable {
    private int itemNumber = 0;
    private LinkedList<VariableObj> master;

    /**
     * Constructor
     */
    public MasterTable() {
        this.itemNumber = 0;
        this.master = new LinkedList<VariableObj>();
    }

    /**
     * Get MasterTable
     */
    public LinkedList<VariableObj> getTable() {
        return this.master;
    }

    /**
     * add reference to table
     */
    public void addRef(VariableObj v) {
        v.setItemNumber(this.itemNumber);
        this.master.add(v);
        this.itemNumber++;
    }

    /**
     * get by node and name
     *
     * @return the VariableObj matched if found, null if not found
     */
    public VariableObj get(String name, Node node) {
        for (int i = 0; i < master.size(); i++) {
            VariableObj temp = master.get(i);

            //check the name
            if (temp.name.equals(name)) {
                //check to make sure the node is the same
                if (temp.node.equals(node)) {
                    return temp;
                }
            }
        }
        return null;
    }

    /**
     * get by name and itemnumber
     *
     * @return the VariableObj matched if found, null if not found
     */
    public VariableObj get(String name, int item) {
        for (int i = 0; i < master.size(); i++) {
            VariableObj temp = master.get(i);

            //check the name
            if (temp.name.equals(name)) {
                //check to make sure the node is the same
                if (temp.itemNumber == item) {
                    return temp;
                }
            }
        }
        return null;
    }

    /**
     * @return toString representation of table
     */
    @Override
    public String toString() {
        String s = "Master Symbol Table: " + "{\n";
        for (int i = 0; i < this.master.size(); i++) {
            s += "\t" + this.master.get(i).toString();

            if (i != this.master.size() - 1) {
                s += ", \n";
            }
        }

        s += "\n}";
        return s;

    }
}
