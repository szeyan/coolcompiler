/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Code Generator
 *
 * @author Noah Cooper
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.codegen;

import beaver.Symbol;
import cool.ErrorMaster;
import cool.Terminals;
import java.io.*;
import cool.ast.*;
import cool.typeCheck.*;
import java.util.*;

public class CodeGenerator {

    String TARGET_TRIPLE_CONF = "etc/target-triple.conf";
    String TARGET;
    String _llvm_main;
    M mtable;
    LinkedHashMap<String, ClassObj> classMap;
    private StringBuilder outbuffer;
    private BufferedWriter outfile;
    private LLTypes llTypes = new LLTypes();
    private ErrorMaster errorMaster;
    private MasterTable masterTable;
    private LinkedHashMap<String, String> strLiterals= new LinkedHashMap<String,String>();

    public static class CodeGeneratorException extends Exception {

        public CodeGeneratorException(String msg) {
            super(msg);
        }
    }

    public CodeGenerator(
            M mtable,
            MasterTable masterTable,
            ErrorMaster errorMaster)
            throws CodeGeneratorException, IOException, FileNotFoundException {
        this("default.ll", mtable, masterTable, errorMaster);
    }

    public CodeGenerator(
            String outfilename,
            M mtable,
            MasterTable masterTable,
            ErrorMaster errorMaster)
            throws CodeGeneratorException, IOException, FileNotFoundException {

        this(outfilename, mtable, null, masterTable, errorMaster);
    }

    public CodeGenerator(
            M mtable,
            String targetTriple,
            MasterTable masterTable,
            ErrorMaster errorMaster)
            throws CodeGeneratorException, IOException, FileNotFoundException {
        this("default.ll", mtable, targetTriple, masterTable, errorMaster);
    }

    public CodeGenerator(
            String outfilename,
            M mtable,
            String targetTriple,
            MasterTable masterTable,
            ErrorMaster errorMaster)
            throws CodeGeneratorException, IOException, FileNotFoundException {

        this.masterTable = masterTable;

        if (targetTriple != null) {
            TARGET = targetTriple;
        } else {
            BufferedReader confReader = new BufferedReader(new FileReader(TARGET_TRIPLE_CONF));
            while (true) {
                String line = confReader.readLine();
                if (line == null) {
                    TARGET = "UnknownArch-UnknownVendor-UnknownOS";
                    break;
                } else if (!(line.startsWith("#")) && !(line.isEmpty())) {
                    TARGET = line;
                    break;
                }
            }
        }

        /* File op help: http://www.mkyong.com/java/how-to-write-to-file-in-java-bufferedwriter-example/ */
        File file = new File(outfilename);
        if (file.exists()) {
            //errorMaster.send("llvm code output file already exists", null);
            //throw new CodeGeneratorException("");
            file.delete();
        }
        file.createNewFile();

        this.outfile = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
        this.outbuffer = new StringBuilder();
        this.mtable = mtable;
        this.classMap = mtable.cm.getClassMap();
        this.errorMaster = errorMaster;
    }

    public void generate() throws CodeGeneratorException, IOException {
        IOException e = null;
        try {
            startMainLLVM();
            writePreamble();
            writePlaceholders();
            writeStringLiterals();
            for (Map.Entry<String, ClassObj> classPair : classMap.entrySet()) {
                writeDefs(classPair);
            }
            //Melody: testingBlockNum
            //walkBody();
            endMainLLVM();

        } catch (IOException ex) {
            e = ex;
        } finally {
            outfile.close();
            if (e != null) {
                throw (e);
            }
        }
    }
    
    public void startMainLLVM() {
        _llvm_main= "\n;; --- Program Entry From C: _llvm_main() --- ;;";
        _llvm_main += ("\ndefine i32 @_llvm_main() nounwind {");
    }
    
    public void addToMainLLVM(String s) {
        _llvm_main += ("\n\t" + s);
    }
    
    public void addBlockToMainLLVM(String s) {
        _llvm_main += ("\n\tcall void " + s + "()");
    }
    
    public void endMainLLVM() throws IOException {
        _llvm_main += ("\n\tret i32 100");
        _llvm_main += ("\n}\n");
        outbuffer.append(_llvm_main);
        flush();
    }

    /*
     * Flushes the output StringBuffer to the output file.
     */
    private void flush() throws IOException {
        String out = outbuffer.toString();
        out = out.replace("%struct.obj_Nothing*", "void");
        outfile.write(out);
        outfile.flush();
        outbuffer.setLength(0);
    }

    /*
     * Writes an obligatory block of code independent of the Cool source
     */
    void writePreamble() throws IOException {
        outbuffer.append("\n; This section generated by writePreamble():\n\n");

        outbuffer.append("declare i8* @malloc(i32)\n");
        outbuffer.append("target triple = \"" + TARGET + "\"\n");

        flush();
    }

    /*
     * Writes the basic cool type prototypes and native function declarations into the intermediate file
     */
    void writePlaceholders() throws IOException {
        outbuffer.append("\n;; --- Declaring Native Classtypes to prevent definition issues --- ;;");
        outbuffer.append("\n%struct.class_native = type i32");
        outbuffer.append("\n%struct.class_Any = type { i8*, %struct.obj_String* (%struct.obj_Any*)*, %struct.obj_Boolean* (%struct.obj_Any*, %struct.obj_Any*)* }");
        outbuffer.append("\n%struct.obj_String = type { %struct.class_String*, %struct.obj_Int*, i8* }");
        outbuffer.append("\n%struct.class_String = type { i8*, %struct.obj_String* (%struct.obj_String*)*, %struct.obj_Boolean* (%struct.obj_String*, %struct.obj_Any*)*, %struct.obj_Int* (%struct.obj_String*)*, %struct.obj_String* (%struct.obj_String*, %struct.obj_String*)*, %struct.obj_String* (%struct.obj_String*, %struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_String*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_String*, %struct.obj_String*)* }");
        outbuffer.append("\n%struct.obj_Boolean = type { %struct.class_Boolean*, i32 }");
        outbuffer.append("\n%struct.class_Boolean = type { i8*, %struct.obj_String* (...)*, %struct.obj_Boolean* (%struct.obj_Boolean*, %struct.obj_Any*)* }");
        outbuffer.append("\n%struct.obj_Any = type { %struct.class_Any* }");
        outbuffer.append("\n%struct.obj_Int = type { %struct.class_Int*, i32 }");
        outbuffer.append("\n%struct.class_Int = type { i8*, %struct.obj_String* (%struct.obj_Int*)*, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Any*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Int*)* }");
        outbuffer.append("\n%struct.class_IO = type { i8*, void (%struct.obj_IO*, %struct.obj_String*)*, %struct.obj_IO* (%struct.obj_IO*, %struct.obj_String*)*, %struct.obj_Boolean* (%struct.obj_IO*, %struct.obj_Any*)*, %struct.obj_IO* (%struct.obj_IO*, %struct.obj_Any*)*, %struct.obj_String* (%struct.obj_IO*)*, %struct.obj_Symbol* (%struct.obj_IO*, %struct.obj_String*)*, %struct.obj_String* (%struct.obj_IO*, %struct.obj_Symbol*)* }");
        outbuffer.append("\n%struct.obj_IO = type { %struct.class_IO* }");
        outbuffer.append("\n%struct.obj_Symbol = type { %struct.class_Symbol*, i32, %struct.obj_String*, %struct.obj_Int* }");
        outbuffer.append("\n%struct.class_Symbol = type { i8*, %struct.obj_String* (%struct.obj_Symbol*)*, %struct.obj_Int* (%struct.obj_Symbol*)* }");
        outbuffer.append("\n%struct.class_Unit = type { i8* }");
        outbuffer.append("\n%struct.obj_Unit = type { %struct.class_Unit* }");
        
        outbuffer.append("\n;; --- Declaring Native Methods to prevent usage issues --- ;;");
        outbuffer.append("\ndeclare %struct.obj_String* @Any_toString(%struct.obj_Any* %self) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Any_equals(%struct.obj_Any* %self, %struct.obj_Any* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Any* @Any_constructor() nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Boolean_equals(%struct.obj_Boolean* %self, %struct.obj_Any* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Boolean_constructor(i32 %value) nounwind");
        outbuffer.append("\ndeclare %struct.obj_String* @Int_toString(%struct.obj_Int* %self) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Int_equals(%struct.obj_Int* %self, %struct.obj_Any* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Int* @Int_plus(%struct.obj_Int* %self, %struct.obj_Int* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Int* @Int_minus(%struct.obj_Int* %self, %struct.obj_Int* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Int* @Int_mult(%struct.obj_Int* %self, %struct.obj_Int* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Int* @Int_div(%struct.obj_Int* %self, %struct.obj_Int* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Int_lt(%struct.obj_Int* %self, %struct.obj_Int* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Int_le(%struct.obj_Int* %self, %struct.obj_Int* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Int* @Int_constructor(i32 %value) nounwind");
        outbuffer.append("\ndeclare void @IO_abort(%struct.obj_IO* %self, %struct.obj_String* %message) nounwind");
        outbuffer.append("\ndeclare %struct.obj_IO* @IO_out(%struct.obj_IO* %self, %struct.obj_String* %arg) nounwind");
        outbuffer.append("\ndeclare %struct.obj_String* @IO_in(%struct.obj_IO* %self) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Symbol* @IO_symbol(%struct.obj_IO* %self, %struct.obj_String* %name) nounwind");
        outbuffer.append("\ndeclare %struct.obj_String* @IO_symbol_name(%struct.obj_IO* %self, %struct.obj_Symbol* %sym) nounwind");
        outbuffer.append("\ndeclare %struct.obj_IO* @IO_constructor() nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @String_equals(%struct.obj_String* %self, %struct.obj_Any* %other) nounwind");
        outbuffer.append("\ndeclare %struct.obj_String* @String_concat(%struct.obj_String* %self, %struct.obj_String* %arg) nounwind");
        outbuffer.append("\ndeclare %struct.obj_String* @String_substring(%struct.obj_String* %self, %struct.obj_Int* %start, %struct.obj_Int* %end) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Int* @String_charAt(%struct.obj_String* %self, %struct.obj_Int* %index) nounwind");
        outbuffer.append("\ndeclare %struct.obj_String* @String_constructor(i8* %s) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Symbol* @Symbol_constructor(%struct.obj_String* %str) nounwind");
        outbuffer.append("\ndeclare %struct.obj_Unit* @Unit_constructor() nounwind");
        outbuffer.append("\ndeclare %struct.obj_Boolean* @Boolean_not(%struct.obj_Boolean* %self) nounwind");
        
        outbuffer.append("\n\n");
        flush();
    }
    
    void writeStringLiterals() {
        outbuffer.append("\n\n;; --- Defining String Literal Values for Later Use (and Great Justice) --- ;;");
        int count= 0;
        
        for (ClassObj classObj : classMap.values()) {
            Node classnode= classObj.getNode();
            LinkedList<Node> primaries = new TreeWalker(classnode).filter("PRIMARY");
            
            for (Node primary : primaries) {
                LinkedList<Symbol> children= primary.getList();
                if (children.isEmpty()) {
                    continue;
                }
                
                Symbol first= children.getFirst();
                if (first.getId() == Terminals.STRING ) {
                    int size= first.value.toString().length() + 1;
                    outbuffer.append("\n@.str" + count + " = private unnamed_addr constant [" + 
                            size + "  x i8] c\"" + first.value.toString() + "\\00\", align 1");
                    strLiterals.put(first.value.toString(), "@.str" + count);
                    count++;
                }
            }
        }
    }

    /*
     * Unwraps a <String, ClassObj> pair, and calls other methods to write the
     * class structure, the object structure, and the method structures contained in it.
     *
     * Currently skips all non-trivial basic classes. (See LLTypes.basicClasses.)
     */
    void writeDefs(Map.Entry<String, ClassObj> classPair) throws IOException, CodeGeneratorException {
        String className = classPair.getKey();

        ClassObj classObj = classPair.getValue();
        String superclassName = classObj.getParentName();
        if (superclassName.isEmpty()) {
            superclassName = "Any";
        }
        LinkedHashMap<String, MethodObj> methods = classObj.getMethods();
        LinkedHashMap<String, VariableObj> formals = classObj.getVarformals();
        LinkedHashMap<String, VariableObj> attributes = classObj.getAttributes();

        // Skip EVERYTHING for these classes...
        // Skip this classdef and objdef if we are using C instead.
        if (!className.equals("ArrayAny") && !className.equals("Any") && !className.equals("Statistics")) {
            if (!llTypes.isBasicClass(className)) {
                writeClassDef(className, superclassName, methods);
                writeObjDef(className, formals, attributes);
                writeClassObj(className, superclassName, methods);
            }
            // Main is special case; use its static blocks as executable blocks
            if (className.equals("Main")) {
                writeExecBlocks(classObj);
            }
            
            for (Map.Entry<String, MethodObj> methodPair : methods.entrySet()) {
                writeMethodDef(classObj, className, superclassName, methodPair);
            }
        }
      
        flush();
    }

    /*
     * "; Declare the layout of the class structure" (cf. pair.ll)
     */
    private void writeClassDef(String className, String superclassName,
            LinkedHashMap<String, MethodObj> methods) throws IOException {
        outbuffer.append("\n; ***This section generated by writeClassDef() for " + className + ":\n");
        outbuffer.append("%struct.class_" + className + " = type {\n");
        outbuffer.append("\t%struct.class_" + superclassName + "*");

        Iterator methodPairIter = methods.entrySet().iterator();
        while (methodPairIter.hasNext()) {
            String[] sig = getLLSignature(className, (Map.Entry<String, MethodObj>) methodPairIter.next());
            String tempstr = outbuffer.toString();
            if (!tempstr.contains(sig[2])) {
                continue;
            }
            outbuffer.append(",\n");
            outbuffer.append("\t" + sig[0] + " " + sig[2] + "* ");
        }
        outbuffer.append("\n");

        outbuffer.append("\t}\n");
        outbuffer.append("; ***\n");

        flush();
    }

    /*
     * "; Declare layout of the object structure" (cf. pair.ll)
     */
    private void writeObjDef(String className, LinkedHashMap<String, VariableObj> formals,
            LinkedHashMap<String, VariableObj> attributes) throws IOException {
        outbuffer.append("\n; ***This section generated by writeObjDef() for " + className + ":\n");

        outbuffer.append("%struct.obj_" + className + " = type {\n");
        outbuffer.append("\t%struct.class_" + className + "*");

        Iterator attrIterator;
        attrIterator = formals.entrySet().iterator();
        while (attrIterator.hasNext()) {
            outbuffer.append(",\n");
            String[] sig = getLLSignature((Map.Entry<String, VariableObj>) attrIterator.next());
            outbuffer.append("\t" + sig[0]);
        }
        attrIterator = attributes.entrySet().iterator();
        while (attrIterator.hasNext()) {
            outbuffer.append(",\n");
            String[] sig = getLLSignature((Map.Entry<String, VariableObj>) attrIterator.next());
            outbuffer.append("\t" + sig[0]);
        }
        outbuffer.append("\n");

        outbuffer.append("\t}\n");
        outbuffer.append("; ***\n");

        flush();
    }

    /*
     * "; The actual class object..." (cf. pair.ll)
     */
    private void writeClassObj(String className, String superclassName, LinkedHashMap methods)
            throws IOException {
        outbuffer.append("\n; ***This section generated by writeClassObj() for " + className + ":\n");

        outbuffer.append("@" + className + " = global %struct.class_" + className + " {\n");
        outbuffer.append("\t%struct.class_" + superclassName + "* null");

        Iterator methodPairIter = methods.entrySet().iterator();
        while (methodPairIter.hasNext()) {
            String[] sig = getLLSignature(className, (Map.Entry<String, MethodObj>) methodPairIter.next());
            String tempstr = outbuffer.toString();
            if (!tempstr.contains(sig[2])) {
                continue;
            }
            outbuffer.append(",\n");
            outbuffer.append("\t" + sig[0] + " " + sig[2] + "* " + sig[1]);
        }
        outbuffer.append("\n");

        outbuffer.append("\t}\n");
        outbuffer.append("; ***\n");

        flush();
    }

    /*
     * "; Methods in ..." (cf. pair.ll)
     */
    private void writeMethodDef(ClassObj classObj, String className, String superclassName, Map.Entry<String, MethodObj> methodPair)
            throws IOException {
        MethodObj methodObj = methodPair.getValue();
        String returnType = methodObj.getReturnType();
        String methodName = methodObj.getName();

        outbuffer.append("\n; ***This section generated by writeMethodDef() for " + className + "_" + methodName + ":\n");
        String[] sig = getLLSignature(className, methodPair);
        Symbol methodBody = null;
        String bodyValue = null;
        // Avoid generating natively-defined methods
        try {
            methodBody = (Symbol) methodObj.getNode().getList().get(3);
            bodyValue = methodBody.value.toString();
            if (bodyValue.equals("native")) { return; }
        } catch (NullPointerException e) {
            System.err.println("Method " + className + "_" + methodName + "has no method Node");
        }

        //INHERITANCE CASE (except constructors):
        //TODO: Skip writing this when we are not implementing the inherited method?
        //      Probably a little complicated, and we can probably just let that fail.
        //System.out.println(superclassName);
        if ( returnType != null && !(returnType.equals(className)) && methodObj.isInherited()
            && mtable.getClass(superclassName).getMethod(methodName) != null) {

            outbuffer.append(sig[1] + " = ");
            //TODO Need to modify the function pointer args to list the methods arguments and type
            // i.e. (%struct.obj_A*, i32)*
            outbuffer.append("alias %struct.obj_" + returnType + " (%struct.obj_" + className + "*, i32)* ");
            outbuffer.append("bitcast (%struct.obj_" + returnType + "* (%struct.obj_" + superclassName + "*, i32)* ");

            outbuffer.append("@" + superclassName + "_" + methodName + " to %struct.obj_" + returnType);
            outbuffer.append("(%struct.obj_" + className + "*, i32)*)");
            outbuffer.append("\n");

            outbuffer.append(";\n");
            outbuffer.append("; ^^^Inherited method\n");
        //if we are not inherting and we are native then we do nothing
        }else if( bodyValue != null && bodyValue.equals("native")){
            outbuffer.append("\n; ***Avoided generating native implementation for " + className + "_" + methodName + ":\n");
            flush();
            return;
        }

        outbuffer.append("define " + sig[0] + " " + sig[1] + " " + sig[2] + " {\n");

        // Number of attributes and varformals plus 1 for the class pointer.
        int numWords = classObj.getAttributes().size() + classObj.getVarformals().size() + 1;
        int numBytes = numWords * 4;


        //CONSTRUCTOR CASE:
        if (returnType.equals(className)) {
            //TODO: Always 2 4-byte words? No. pair.ll with inheritance has a 3-word object.
            //Somehow calulate object size with this method from the LLVM API?
            //http://llvm.org/doxygen/classllvm_1_1ConstantExpr.html#a778163e6ec80716a12ab3282cb97f0d9
            // Allocation:
            outbuffer.append("\t%memchunk = call i8* @malloc(i32 " + numBytes + ")\n");
            outbuffer.append("\t%as_obj = bitcast i8* %memchunk to " + sig[0] + "\n");
            outbuffer.append("\tret %struct.obj_" + className + "* %as_obj\n");

            // TODO: Initialization with recursive superclass constructor calls

            // TODO: Static blocks (they're implictly part of the constructor)
            //       (Just need to write generateClassStaticBlocks(). Easy!)
            outbuffer.append(writeClassStaticBlocks(classObj));
        } //GENERAL CASE:
        else {
            outbuffer.append("\t%memchunk = call i8* @malloc(i32 " + numBytes + ")\n");
            outbuffer.append("\t%as_obj = bitcast i8* %memchunk to " + sig[0] + "\n");
            outbuffer.append("\tret %struct.obj_" + returnType + "* %as_obj\n");
        }

        outbuffer.append("\t}\n");
        outbuffer.append("; ***\n");

        flush();
    }

    /*
     *  @return a (multiline) String containing the llvm representation
     *           of the ClassObj's static blocks, including a trailing newline
     */
    private String writeClassStaticBlocks(ClassObj classObj) {
        //TODO: This method.
        return "";
    }
    
    private void writeExecBlocks(ClassObj classObj) throws IOException, CodeGeneratorException {
        LinkedList<Node> blocks= classObj.getExecBlocks();
        if (blocks == null || blocks.isEmpty()) {
            return;
        }
        
        // TODO handle more than just the first block
        for (int i= 0; i < blocks.size(); i++) {
            Node block= blocks.get(i);
            block= new TreeWalker().flatten(block);
            outbuffer.append("\ndefine void @_exec_block_" + i + "() {");
            
            // TODO Actually recursively fill this block with sauce
            emitBody( (Symbol) block , null, classObj);
            
            outbuffer.append("\n\tret void");
            outbuffer.append("\n}\n");
            addBlockToMainLLVM("@_exec_block_" + i);
        }
        
    }

    private String[] getLLSignature(String className, Map.Entry<String, MethodObj> methodPair) {
        String[] sigArray = new String[3];
        StringBuilder sigElement = new StringBuilder();
        MethodObj methodObj = methodPair.getValue();

        LinkedHashMap<String, String> params = methodObj.getParams();
        String methodName = methodPair.getKey();
        String returnType = methodObj.getReturnType();

        if (methodName.equals(className) && returnType.equals(className)) {
            sigArray[0] = llTypes.getLLType(className);
        } else {
            sigArray[0] = llTypes.getLLType(returnType);
        }

        if (methodName.equals(className) && returnType.equals(className)) {
            sigArray[1] = "@" + className + "_constructor";
        } else {
            sigArray[1] = "@" + className + "_" + methodName;
        }

        sigElement.append("(%struct.obj_" + className + "*");
        Iterator i = params.keySet().iterator();
        if (i.hasNext()) {
            sigElement.append(", ");
        }
        while (i.hasNext()) {
            sigElement.append(llTypes.getLLType((String) params.get(i.next())));
            if (i.hasNext()) {
                sigElement.append(", ");
            }
        }
        sigElement.append(" )");
        sigArray[2] = new String(sigElement);

        return sigArray;
    }

    private String[] getLLSignature(Map.Entry<String, VariableObj> attrPair) {
        String[] sigArray = new String[1];

        sigArray[0] = llTypes.getLLType(attrPair.getValue().type.getName());

        return sigArray;
    }
    /*
     * ////////////////////////////////////////////////////////////
     * ///////////////Melody's testingBlockNum//////////////////////////
     * ////////////////////////////////////////////////////////////
     *
     * The following might not be in correct llvm format.
     *
     * Handles:
     * - Ints
     * - Booleans
     * - Created a lookup for registers
     *
     * Includes:
     * - int declaration
     * - boolean declaration
     * - variable assignment
     * - complex arithmetic
     * - unary minus
     * - basic if statements (not ,<,<=,==)
     * - while loops
     *
     * To-do:
     * - Strings
     * - correct format
     * - what about scoping? multiple variables with the same name
     * - item number from mastertable, variableobj, register
     * - is masterTable even needed?
     * - error checking or throw some exceptions for nulls
     */
    private int testingBlockNum = 3;
    private int regCount = 0;
    private String ret = "";
    private String currClass = "";
    private String i32 = "i32";
    private LinkedHashMap<String, Register> memory = new LinkedHashMap<String, Register>();

    /*
     * look up for register (don't worry about scoping, i think i know how to solve that)
     *
     * Note: linkedhashmap overwrites when adding, so the most recent instance of a register name will be retrieved...
     *
     */
    private Register lookup(String name, String header) {
        Register reg = null;

        Collection<Register> registers = memory.values();
        Iterator<Register> itr = registers.iterator();
        while (itr.hasNext()) {
            Register r = itr.next();
            if (r.header.equals(header) && r.name.equals(name)) {
                return r;
            }
        }

        return reg;
    }

    //emit example: %tmp_3 =
    private Register newTmp() {
        Register reg = new Register();
        reg.header = reg.tmpHeader;
        reg.name = Integer.toString(this.regCount);

        this.regCount++;

        return reg;
    }

    private String assign(Register reg) {
        String ret = "";
        ret += reg.getName();
        ret += " =";
        return ret;
    }

    //emit example: %var_a = alloca i32, align 4
    private String alloca(Register reg) throws CodeGeneratorException {
        String ret = "";

        ret += "alloca ";       //allocate memory on heap
        ret += reg.emitType();   //for type
        ret += ", ";
        ret += "align ";
        ret += reg.align;       //with alignment of 'align' bytes

        return ret;
    }

    //emit example: store i32 10, i32* %var_a, align 4
    private String storeInt(Register storeTo, String value) throws CodeGeneratorException {
        String ret = "";

        ret += "store ";
        ret += i32; //D:?
        ret += " ";
        ret += value;
        ret += ", ";
        ret += storeTo.getPtr();
        ret += ", ";
        ret += "align ";
        ret += storeTo.align;

        return ret;
    }

    private String storeInt(Register storeTo, Register from) throws CodeGeneratorException {
        String ret = "";

        ret += "store ";
        ret += from.getObjName();
        ret += ", ";
        ret += storeTo.getPtr();
        ret += ", ";
        ret += "align ";
        ret += storeTo.align;

        return ret;
    }

    //emit example: %tmp_2 = load i32* %a, align 4
    private String loadInt(Register reg) throws CodeGeneratorException {
        String ret = "";

        ret += "load ";
        ret += reg.getPtr();
        ret += ", ";
        ret += "align ";
        ret += reg.align;

        return ret;
    }

    //emit example:  add nsw i32 %tmp_2, 2
    private String binOp(String op, Register r1, Register r2) {
        String ret = "";

        ret += op;
        ret += " ";
        ret += i32; //D:?
        ret += " ";
        ret += r1.getName();
        ret += ", ";
        ret += r2.getName();

        return ret;
    }

    //emit example:   br i1 %3, label %4, label %7
    private String branch(Register base, Register branchTrue, Register branchFalse) {
        String ret = "";

        ret += "br il";
        ret += " ";
        ret += base.getName();
        ret += ", ";
        ret += "label";
        ret += " ";
        ret += branchTrue.getName();
        ret += ", ";
        ret += "label";
        ret += " ";
        ret += branchFalse.getName();

        return ret;
    }

    //emit example:   br label <dest> ; Unconditional branch
    private String branchUncond(Register jumpTo) {
        String ret = "";

        ret += "br label";
        ret += " ";
        ret += jumpTo.getName();

        return ret;
    }

    private void writeC(String s) {
        ret += s + " \n";
    }

    private void writeI(String s) {
        ret += s + " ";
    }

    //example of walking a static block inside t5.cool
    private void walkBody() throws IOException, CodeGeneratorException {
        ClassObj co = mtable.getClass("Main");
        Node coNode = co.getNode();

        LinkedHashMap<String, MethodObj> methods = co.getMethods();
        LinkedList<Node> executionBlocks = co.getExecBlocks();

        //check example block 1;
        Node block = (Node) executionBlocks.get(testingBlockNum);

        //walk and flatten the block
        block = new TreeWalker().flatten(block);

        System.err.println(new TreeWalker().toStringFlat(block));

        //emit code from this block
        /*
        this.currClass = co.getName();
        emitBody((Symbol) block, null, co);
        System.err.println("--Example--");
        System.err.println(this.ret);

        System.err.println("Registers {");
        Iterator<Register> rs = memory.values().iterator();
        while (rs.hasNext()) {
            System.err.println("\t" + rs.next());
        }
        System.err.println("}");
        
        //*/
    }

    /*
     * Melody: sample on how to walk a block
     * Blocks can be found as just static execution blocks or found as the body of a method
     *
     * Note: uses .equals() instead of instanceof because instance of accepts all superclasses as an instance,
     * thus causing multiple walks (?)
     *
     * Status: may not contain all walks
     *
     * @param block is the current symbol or node we're evaluating
     * @param className the class the block belongs to
     *
     * @return void
     */
    private void emitBody(Symbol body, Register currReg, ClassObj classObj) throws IOException, CodeGeneratorException {
        int terminal = body.getId();
        LinkedList<Symbol> children = null;
        Node currNode = null;

        //-- emit only if not null
        if (body == null) {
            return;
        }


        //-- main body (BlockListNode)
        if (body.toString().equals("BLOCKLIST")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //print start of a block
            writeC("; -**- START BLOCK -**-");

            //emit code for each child
            for (int i = 0; i < children.size(); i++) {
                emitBody(children.get(i), currReg, classObj);
            }

            //print end of a block
            writeC("\n; -**- END BLOCK -**-");
        }

        //-- var def (BlockNode)
        if (body.toString().equals("BLOCK")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //write header
            writeC("\n; //-- WRITE variable declarion");

            //make a new register
            Register reg = new Register();
            reg.header = reg.varHeader;

            //first child = identifier
            reg.name = children.get(0).value.toString();

            //second child = type
            reg.type = children.get(1);

            //-- write allocation
            outbuffer.append("\n\t" + assign(reg));
            outbuffer.append(alloca(reg));

            //third child = evaluation / definition
            emitBody(children.get(2), reg, classObj);

            //-- write store
            //To-do: further checking of this so called "value" is needed
            writeC(storeInt(reg, reg.value));

            //store the register into memory of all registers (including temps)
            memory.put(reg.name, reg);

            //store the register into the var attribute associated with it
            VariableObj va = masterTable.get(reg.name, currNode);
            va.setRegister(reg);
            reg.itemNumber = va.itemNumber; //associate the register to the varattribute in mastertable
        }

        //-- primaries (PrimaryNode)
        //includes: integer, string, new, super, etc
        if (body.toString().equals("PRIMARY")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //check if terminal: INTEGER/STRING
            Symbol s1 = (Symbol) children.get(0);
            if (children.size() == 1) {
                emitBody(s1, currReg, classObj);
            }

            //To-do: check if NEW
            if (s1.value.toString().equals("new")) {
                String type= children.get(1).value.toString();
                Register reg= newTmp();
                
                // TODO Not do it stupidly; consider Varformals for the class when instantiating
                // ^ Until that happy TODO day, do it stupidly and just call the @TYPE_constructor() function
                outbuffer.append("\n\t"+ assign(reg) );
                outbuffer.append("call %struct.obj_" + type + "* @" + type + "_constructor()");
                outbuffer.append("\n\tstore %struct.obj_"+ type +"* "+ reg.getName() +", %struct.obj_"+ type +"** "+ currReg.getName() +", align 4");
            }

            //To-do: check if SUPER
        }

        //-- binary expressions (ExprNode)
        if (body.toString().equals("EXPR")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //first child = identifier
            String id = children.get(0).value.toString();

            //lookup and retrieve register from masterTable
            Register reg = new Register();
            Register temp= lookup(id, reg.varHeader);
            if (temp != null) {
                reg= temp;
            } else {
                outbuffer.append("\n\t" + assign(reg));
                outbuffer.append(alloca(reg));
            }

            //second child = assignment
            Symbol rExpr = children.get(1);

            writeC("\n; //-- WRITE variable assignment");

            //check if direct assignment
            if (rExpr.toString().equals("PRIMARY")) {

                Register r1 = newTmp();
                memory.put(r1.name, r1);
                //outbuffer.append("\n\t;; --- Assignment Goes Here --- ;;");
                //outbuffer.append("\n\t"+assign(r1));
                emitBody(rExpr, r1, classObj);

                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }

                //store into variable reg from assignment value r1
                writeC(storeInt(reg, r1));

            } else {
                //evaluate the assignment
                
                outbuffer.append("\n\t;; --- Assignment Goes Here --- ;;");
                emitBody(rExpr, reg, classObj);
            }
        }

        //-- plus (PlusNode)
        //-- minus (MinusNode)
        //-- times (TimesNode)
        //-- divide (DivideNode)
        if (body.toString().equals("PLUS")
                || body.toString().equals("MINUS")
                || body.toString().equals("TIMES")
                || body.toString().equals("DIVIDE")) {
            //no need to flatten node (recursively checks)
            currNode = (Node) body;
            //grab children
            children = currNode.getList();

            //get operation
            String op = "";
            if (body.toString().equals("PLUS")) {
                op = "plus";
            } else if (body.toString().equals("MINUS")) {
                op = "minus";
            } else if (body.toString().equals("TIMES")) {
                op = "mult";
            } else if (body.toString().equals("DIVIDE")) {
                op = "div";
            }

            //write header
            writeC("; -- start " + op + " instruction");
            outbuffer.append("\n\t;; --- Generating " + op + " instruction --- ;;");

            Node pleft= (Node) children.getFirst();
            Node pright= (Node) children.getLast();
            Symbol left= (Symbol)pleft.getList().getFirst();
            Symbol right= (Symbol)pright.getList().getFirst();
            
            // Handle left register
            Register lreg= newTmp();
            lreg.type= null;
            lreg.typeStr= "Int";
            if (left.getId() == Terminals.INTEGER) {
                outbuffer.append("\n\t" + assign(lreg));
                outbuffer.append(alloca(lreg));
                emitBody(left, lreg, classObj);
            } else {
                lreg= lookup(left.value.toString(), lreg.varHeader);
                if (lreg == null) {
                    System.err.println("Register for value " + left.value.toString() + " not found!");
                    System.exit(1);
                }
            }
            
            // Handle right register
            Register rreg= newTmp();
            rreg.type= null;
            rreg.typeStr= "Int";
            if (right.getId() == Terminals.INTEGER) {
                outbuffer.append("\n\t" + assign(rreg));
                outbuffer.append(alloca(rreg));
                emitBody(right, rreg, classObj);
            } else {
                rreg= lookup(right.value.toString(), rreg.varHeader);
                if (rreg == null) {
                    System.err.println("Register for value " + right.value.toString() + " not found!");
                    System.exit(1);
                }
            }
            
            // Make the function call
            Register t1= newTmp();
            outbuffer.append("\n\t" + assign(t1));
            outbuffer.append(loadInt(lreg));
            
            Register t2= newTmp();
            outbuffer.append("\n\t" + assign(t2));
            outbuffer.append(loadInt(rreg));
            
            Register t3= newTmp();
            outbuffer.append("\n\t" + assign(t3));
            outbuffer.append(" call %struct.obj_Int* @Int_"+ op +"(%struct.obj_Int* "+ t1.getName() +", %struct.obj_Int* "+ t2.getName() +")");
            outbuffer.append("\n\tstore %struct.obj_Int* "+ t3.getName() +", %struct.obj_Int** "+ currReg.getName() +", align 4");
            
            /*
            // Allocate and evaluate left
            // Register lreg= new Register();
            
            //first child = first item
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1, classObj);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1, classObj);
            }


            //second child = second item
            Symbol c2 = children.get(1);
            Register r2 = newTmp();
            memory.put(r2.name, r2);
            if (c2.toString().equals("PRIMARY")) {
                writeI(assign(r2));
                emitBody(c2, r2, classObj);
                if (!r2.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r2));
                    writeC(storeInt(r2, r2.value));
                }
            } else {
                writeI(assign(r2));
                writeC(alloca(r2));
                emitBody(c2, r2, classObj);
            }


            //operate on first and second child
            Register r3 = newTmp();
            memory.put(r3.name, r3);
            writeI(assign(r3)); //either load register or allocate and store a constant
            writeC(binOp(op, r1, r2));

            //store into original
            writeC(storeInt(currReg, r3));

            //write new line
            writeC("; -- end " + op + " instruction");

            */
        }

        //-- unary minus (UnaryMinusNode)
        if (body.toString().equals("UNARYMINUS")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("; -- start unary minus ");

            //first child -- integer expr to negate
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1, classObj);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1, classObj);
            }

            //write zero register
            Register valueOfZero = newTmp();
            memory.put(valueOfZero.name, valueOfZero);
            writeI(assign(valueOfZero));
            writeC(alloca(valueOfZero));
            writeC(storeInt(valueOfZero, "0"));

            //subtract 0 - first child to get negation
            Register r3 = newTmp();
            memory.put(r3.name, r3);
            writeI(assign(r3)); //either load register or allocate and store a constant
            writeC(binOp("sub", valueOfZero, r1));

            //store into original
            writeC(storeInt(currReg, r3));

            writeC("; -- end unary minus ");
        }

        //-- if else (IfElseNode)
        if (body.toString().equals("IFELSE")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("\n; //-- WRITE if-else statement");

            writeC("; -- start boolean evaluation");
            //first child = (boolean expression to evaluate)
            Symbol s1 = (Symbol) children.get(0);

            //write result register
            Register result = newTmp();
            memory.put(result.name, result);

            //check boolean expr
            if (s1.toString().equals("PRIMARY")) {
                //either true or false or load a single variable
                Register t1 = newTmp();
                memory.put(t1.name, t1);
                writeI(assign(t1));
                emitBody(s1, t1, classObj);

                if (!t1.value.isEmpty()) {
                    //allocate true or false
                    writeC(alloca(t1));
                    writeC(storeInt(t1, t1.value));
                }

                //write compareTo register
                Register valueOfOne = newTmp();
                memory.put(valueOfOne.name, valueOfOne);
                writeI(assign(valueOfOne));
                writeC(alloca(valueOfOne));
                writeC(storeInt(valueOfOne, "1"));

                //write comparison instruction
                writeI(assign(result));
                writeC(binOp("icmp eq", t1, valueOfOne)); //is t1 == true?
            } else {
                //left side and right side of bool expr must be computed
                emitBody(s1, result, classObj); //evaluate <, <=, or ==
            }
            writeC("; -- end boolean evaluation");

            //write branches
            Register branchTrue = newTmp();
            memory.put(branchTrue.name, branchTrue);

            Register branchFalse = newTmp();
            memory.put(branchFalse.name, branchFalse);

            Register branchEnd = newTmp();
            memory.put(branchEnd.name, branchEnd);

            //write where to branch
            writeC(branch(result, branchTrue, branchFalse));

            //second child = true case
            writeC("\n" + branchTrue.getLabel() + "\t\t\t\t; True Branch");
            Symbol s2 = (Symbol) children.get(1);
            emitBody(s2, currReg, classObj);
            writeC(branchUncond(branchEnd));

            //third child = false case
            writeC("\n" + branchFalse.getLabel() + "\t\t\t\t; False Branch");
            Symbol s3 = (Symbol) children.get(2);
            emitBody(s3, currReg, classObj);
            writeC(branchUncond(branchEnd));

            //get out of the branch
            writeC("; //-- END if-else statement");
            writeC("\n" + branchEnd.getLabel() + "\t\t\t\t; Exit If-Else");

            //To-do: if else if else, multiple operands, and
        }

        //-- less than (LessThanNode)
        //-- less than or equal (LessEqualNode)
        //-- equal  (EqualNode)
        if (body.toString().equals("LESSTHAN")
                || body.toString().equals("LESSEQUAL")
                || body.toString().equals("EQUAL")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //first child - left side
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1, classObj);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1, classObj);
            }


            //second child - right side
            Symbol c2 = children.get(1);
            Register r2 = newTmp();
            memory.put(r2.name, r2);
            if (c2.toString().equals("PRIMARY")) {
                writeI(assign(r2));
                emitBody(c2, r2, classObj);
                if (!r2.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r2));
                    writeC(storeInt(r2, r2.value));
                }
            }

            //write comparison instruction
            writeI(assign(currReg));

            if (body.toString().equals("LESSTHAN")) {
                writeC(binOp("icmp slt", r1, r2));
            } else if (body.toString().equals("LESSEQUAL")) {
                writeC(binOp("icmp sle", r1, r2));
            } else if (body.toString().equals("EQUAL")) {
                writeC(binOp("icmp eq", r1, r2));
            }
        }

        //-- not (NotNode)
        if (body.toString().equals("NOT")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("; -- start boolean negation ");

            //first child - the boolean expr to negate
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1, classObj);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1, classObj);
            }


            //write compareTo register
            Register valueOfOne = newTmp();
            memory.put(valueOfOne.name, valueOfOne);
            writeI(assign(valueOfOne));
            writeC(alloca(valueOfOne));
            writeC(storeInt(valueOfOne, "1"));

            //write comparison instruction
            writeI(assign(currReg));
            writeC(binOp("icmp ne", r1, valueOfOne)); //is t1 != true?

            writeC("; -- end boolean negation ");
        }

        //-- while (WhileNode)
        if (body.toString().equals("WHILE")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("\n; //-- WRITE while loop");

            //write branches
            Register branchStart = newTmp();
            memory.put(branchStart.name, branchStart);

            Register branchBody = newTmp();
            memory.put(branchBody.name, branchBody);

            Register branchEnd = newTmp();
            memory.put(branchEnd.name, branchEnd);

            //jump to loop start
            writeC(branchUncond(branchStart));

            //first child - boolean expr to evaluate
            writeC("\n" + branchStart.getLabel() + "\t\t\t\t; Loop Check Branch");
            Symbol s1 = (Symbol) children.get(0);

            //write result register
            Register result = newTmp();
            memory.put(result.name, result);

            //check boolean expr
            if (s1.toString().equals("PRIMARY")) {
                //either true or false or load a single variable
                Register t1 = newTmp();
                memory.put(t1.name, t1);
                writeI(assign(t1));
                emitBody(s1, t1, classObj);

                if (!t1.value.isEmpty()) {
                    //allocate true or false
                    writeC(alloca(t1));
                    writeC(storeInt(t1, t1.value));
                }

                //write compareTo register
                Register valueOfOne = newTmp();
                memory.put(valueOfOne.name, valueOfOne);
                writeI(assign(valueOfOne));
                writeC(alloca(valueOfOne));
                writeC(storeInt(valueOfOne, "1"));

                //write comparison instruction
                writeI(assign(result));
                writeC(binOp("icmp eq", t1, valueOfOne)); //is t1 == true?
            } else {
                //left side and right side of bool expr must be computed
                emitBody(s1, result, classObj); //evaluate <, <=, or ==
            }

            //write where to branch
            writeC(branch(result, branchBody, branchEnd));

            //second child - body of the loop
            writeC("\n" + branchBody.getLabel() + "\t\t\t\t; Loop Body Branch");
            Symbol s2 = (Symbol) children.get(1);
            emitBody(s2, currReg, classObj);        
            writeC(branchUncond(branchStart)); //jump back to loop start

            //write ending loop label
            writeC("\n" + branchEnd.getLabel() + "\t\t\t\t;  Exit Loop Branch");
        }

        //case

        //dot
        if (body.toString().equals("DOT")) {
            currNode = (Node) body;
            PrimaryNode primary= (PrimaryNode) currNode.getList().getLast();
            
            // TODO Not do it stupidly; loop through the left-hand-side dots to respect dot-chaining
            // ^ Until that happy TODO day, do it stupidly and just assume left-hand-side is an Identifier
            Node lhsprimary= (Node) currNode.getList().getFirst();
            Symbol lhs= (Symbol) lhsprimary.getList().getFirst();
            String attrName= lhs.value.toString();
            Register reg= memory.get(attrName);
            if (reg == null) {
                System.err.println("Could not find localvariable name " + attrName + " in memory for class " + classObj.getName());
            }
            String type= reg.getBaseType();
                
            // Case 1A: Method Call
            if (primary.getList().size() == 2) {
                Symbol methodId= (Symbol) primary.getList().getFirst();
                String methodName= methodId.value.toString();
                
                String returnType= "";
                if (currReg != null) {
                    returnType= currReg.getBaseType();
                } else {
                    MethodObj mobj= mtable.getClass(type).getMethod(methodName);
                    returnType= mobj.getReturnType();
                }

                Node args= new TreeWalker().flatten((Node)primary.getList().getLast());
                LinkedList<Symbol> argslist= args.getFlatList();

                LinkedList<Register> templist= new LinkedList<Register>();
                LinkedList<Register> rlist= new LinkedList<Register>();

                // Account for self
                Register selftmp= newTmp();
                outbuffer.append("\n\t"+ assign(selftmp));
                outbuffer.append(" "+ loadInt(reg));

                for (int i= 0; i < argslist.size(); i++) {
                    Symbol s= argslist.get(i);
                    Register tmp= newTmp();
                    Register r= memory.get(s.value.toString());
                    outbuffer.append("\n\t"+ assign(tmp));
                    outbuffer.append(" "+ loadInt(r));

                    templist.add(tmp);
                    rlist.add(r);
                }

                Register storageTemp= newTmp();
                if (currReg != null) {
                    outbuffer.append("\n\t"+ storageTemp.getName() +" = call %struct.obj_"+ returnType +"* @"+ type +"_"+ methodName 
                            +"(" + reg.emitType() + " "+ selftmp.getName());
                } else {
                    
                    outbuffer.append("\n\tcall %struct.obj_"+ returnType +"* @"+ type +"_"+ methodName 
                            +"(" + reg.emitType() + " "+ selftmp.getName());
                }
                
                if (!templist.isEmpty()) {
                    outbuffer.append(", ");
                }
                
                for (int i= 0; i < templist.size(); i++) {
                    Symbol s= argslist.get(i);
                    Register tmp= templist.get(i);
                    Register r= rlist.get(i);
                    outbuffer.append(r.emitType() + " " + tmp.getName());

                    if ((i+1) != templist.size()) {
                        outbuffer.append(", ");
                    }
                }

                outbuffer.append(")");
                if (currReg != null) {
                    outbuffer.append("\n\tstore "+ currReg.emitType() +" "+ storageTemp.getName() +", "+ currReg.getPtr() +", align 4");
                    //System.out.println(currReg.getName());
                }
            }
            
        }

        String name;
        Register reg = newTmp();        
        //-- terminals/literals
        switch (terminal) {
            case Terminals.BOOLEAN:
                if (body.value.toString().equals("true")) {
                    //1 for true
                    currReg.value = "1";
                } else {
                    //body.value == false
                    //0 for false
                    currReg.value = "0";
                }
                break;
            case Terminals.INTEGER:
                String intLiteral= body.value.toString();
                outbuffer.append("\n\t" + assign(reg));
                outbuffer.append("call %struct.obj_Int* @Int_constructor(i32 "+ intLiteral +")");
                outbuffer.append("\n\tstore %struct.obj_Int* "+ reg.getName() +", %struct.obj_Int** "+ currReg.getName() +", align 4");
                break;
            case Terminals.NULL:
            //return Null;
            case Terminals.STRING:
                String strLiteral= body.value.toString();
                String strLabel= strLiterals.get(strLiteral);
                if (strLabel == null) {
                    System.err.println("ERR: String literal without predefined label: " + strLiteral);
                }
                int size= strLiteral.length() + 1;
                
                outbuffer.append("\n\t"+ assign(reg) );
                outbuffer.append("call %struct.obj_String* @String_constructor(i8* getelementptr inbounds (["
                        + size + " x i8]* " + strLabel + ", i32 0, i32 0))");
                outbuffer.append("\n\tstore %struct.obj_String* "+ reg.getName() +", %struct.obj_String** "+ currReg.getName() +", align 4");
                
                //throw new CodeGeneratorException("Bogus Error to view application stack");
                break;
            case Terminals.ID:
                name = body.value.toString();
                reg = new Register();
                reg = lookup(name, reg.varHeader);
                writeC(loadInt(reg));
                break;
            case Terminals.THIS:
            //if this.(id)
            //look up varformals or varattributes of the class for the register
            case Terminals.NATIVE:
            //return ?;
                
            flush();
        }
    }
}