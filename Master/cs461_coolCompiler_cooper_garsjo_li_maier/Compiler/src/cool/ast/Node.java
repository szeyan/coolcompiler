/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;
import java.util.LinkedList;

public abstract class Node extends Symbol {

    /*
     * Retrieves the list of children of the node
     */
    public abstract LinkedList getList();

    /*
     * checks if the children are already flattened
     */
    public abstract boolean isFlat();

    /*
     * set the boolean if the children are already flattened
     */
    public abstract void setFlat(boolean flat);
    /*
     * Retrieves the list of children of the node (flattened)
     */

    public abstract LinkedList getFlatList();

    /*
     * Sets the flattened list
     * @param flat is the list to replace flatList
     */
    public abstract void setFlatList(LinkedList flat);

    /*
     * adds a number of Symbols to the current list of children of the node
     */
    public abstract void add(Symbol... s);

    /*
     * @return a String representation for the class name of the node
     */
    public abstract String toString();

    /*
     * Alters a production rule to its intermediate form
     */
    public abstract void intermediate();

    /*
     * returns the TYPE of a node
     */
    public abstract String getType();
}