/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;

public class FeatureNode extends FeatureListNode {

    /*
     * Constructor for a node which creates a list of children based on an abitrary list of Symbols
     */
    public FeatureNode(Symbol... s) {
        super(s);
    }
}