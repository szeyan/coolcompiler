/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 * Treewalker performs a preorder traversal on a given node and prints the preorder walk in JSON format
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;
import cool.Terminals;
import java.util.LinkedList;
import org.apache.commons.lang3.StringEscapeUtils;

public class TreeWalker {

    private int level;
    private Node master;
    private Node find;
    private LinkedList finds;
    private Node flat;

    /**
     * Constructor takes in a node
     */
    public TreeWalker(Node parent) {
        this.master = parent;
        this.find = null;
        this.flat = null;
        this.finds = new LinkedList<Node>();
        this.level = 0;
    }

    public TreeWalker() {
        this(null);
    }

    /**
     * Finds a node within the AST. (first instance)
     *
     * @param match the node to look for.
     * @Return the node if a node was found or null if it wasn't found
     */
    public LinkedList filter(String match) {
        this.finds = new LinkedList<Node>();
        filter(match.toUpperCase(), master);
        return this.finds;
    }

    /**
     * Returns the node based on given string and root to walk (first instance)
     */
    private void filter(String match, Node root) {

        //loop through the children
        for (int i = 0; i < root.getList().size(); i++) {

            //get the ith child
            Object check = (Object) root.getList().get(i);

            //check if the ith child is a node
            if (check instanceof Node) {
                Node child = (Node) check;

                //see if this is the node we're looking for
                if (child.toString().equals(match)) {
                    this.finds.add(child);
                }

                //hasn't been found, continue traversal
                this.filter(match, child);
            }
        }

    }

    /**
     * Finds all instances of a matching node
     *
     * @param match the node to look for.
     * @Return the node if a node was found or null if it wasn't found
     */
    public Node get(String match) {
        this.find = null;
        get(match.toUpperCase(), master);
        return this.find;
    }

    /**
     * Flattens a Node to obtain all it's children only
     *
     * @param Node to flatten
     * @Return flattened node
     */
    public Node flatten(Node node) {
        this.flat = node;
        if (!node.isFlat()) {
            this.flattenList(node);
            node.setFlat(true);
        }
        return flat;
    }

    private void flattenList(Node node) {
        LinkedList<Node> nodeList = node.getList();

        for (int i = 0; i < nodeList.size(); i++) {
            Symbol temp = nodeList.get(i);
            if (temp.toString().equals(flat.toString())) {
                //that means temp is still a list node
                this.flattenList((Node) temp);
            } else {
                if(!flat.getFlatList().contains((Symbol) temp)){
                    flat.getFlatList().add(temp);
                }
            }
        }
    }

    /**
     * Sets the new node as master
     */
    public void set(Node node) {
        this.master = node;
    }

    /**
     * Returns the node based on given string and root to walk (first instance)
     */
    private void get(String match, Node root) {
        //see if this is the node we're looking for
        if (root != null && root.toString().equals(match)) {
            this.find = root;
            return;
        }

        //loop through the children
        for (int i = 0; i < root.getList().size(); i++) {

            //get the ith child
            Object check = (Object) root.getList().get(i);

            //check if the ith child is a node
            if (check instanceof Node) {
                Node child = (Node) check;

                //see if this is the node we're looking for
                if (child.toString().equals(match)) {
                    this.find = child;
                    return;
                }

                //hasn't been found, continue traversal
                this.get(match, child);
            }
        }

    }

    /**
     * @return String of the pre-orderly walked root in JSON format
     */
    public String toString() {
        this.level = 0;
        return this.walk(master);
    }

    /**
     * @return String of the given node in a pre-order traversal in JSON format
     */
    public String toString(Node node) {
        this.level = 0;
        return this.walk(node);
    }

    /**
     * prints the pre-orderly walked root in JSON format
     *
     * altered from Kevin Garsjo's Traversal.java
     */
    private String walk(Node root) {
        String tabs = "";
        for (int j = 0; j < level; j++) {
            tabs += "\t";
        }
        String ret = tabs + "{ \"Rule\" : \"" + root.toString() + "\" ";
        ret += ", \"Children\" : [ \n";
        level++;

        for (int i = 0; i < root.getList().size(); i++) {
            Object child = root.getList().get(i);

            if (child instanceof Node) {
                Node n = (Node) child;

                ret += this.walk(n);
            } else if (child instanceof Symbol) {
                Symbol s = (Symbol) child;
                tabs = "";
                for (int j = 0; j < level; j++) {
                    tabs += "\t";
                }

                String childName = Terminals.NAMES[s.getId()];
                String childValue = s.value.toString();

                //adds escapes to Strings
                if (childName.equals("STRING")) {
                    childValue = StringEscapeUtils.escapeJava(childValue);
                }

                ret += tabs + "{ \"" + childName + "\" : \"" + childValue + "\" }";
            }

            if (i != root.getList().size() - 1) {
                ret += " , \n";
            }
        }
        level--;

        tabs = "";
        for (int j = 0; j < level; j++) {
            tabs += "\t";
        }
        ret += "\n" + tabs + " ] }";
        return ret;
    }

    /**
     * @return String of the given node in a pre-order traversal in JSON format
     */
    public String toStringFlat(Node node) {
        this.level = 0;
        return this.walkFlat(node);
    }

    /**
     * prints the pre-orderly walked root in JSON format
     *
     * altered from Kevin Garsjo's Traversal.java
     */
    private String walkFlat(Node root) {
        String tabs = "";
        for (int j = 0; j < level; j++) {
            tabs += "\t";
        }
        String ret = tabs + "{ \"Rule\" : \"" + root.toString() + "\" ";
        ret += ", \"Children\" : [ \n";
        level++;

        for (int i = 0; i < root.getFlatList().size(); i++) {
            Object child = root.getFlatList().get(i);

            if (child instanceof Node) {
                Node n = (Node) child;

                ret += this.walk(n);
            } else if (child instanceof Symbol) {
                Symbol s = (Symbol) child;
                tabs = "";
                for (int j = 0; j < level; j++) {
                    tabs += "\t";
                }

                String childName = Terminals.NAMES[s.getId()];
                String childValue = s.value.toString();

                //adds escapes to Strings
                if (childName.equals("STRING")) {
                    childValue = StringEscapeUtils.escapeJava(childValue);
                }

                ret += tabs + "{ \"" + childName + "\" : \"" + childValue + "\" }";
            }

            if (i != root.getFlatList().size() - 1) {
                ret += " , \n";
            }
        }
        level--;

        tabs = "";
        for (int j = 0; j < level; j++) {
            tabs += "\t";
        }
        ret += "\n" + tabs + " ] }";
        return ret;
    }
}
