#ifndef _INT_C
#define _INT_C

#include <stdlib.h>

#include "natives.c"
#include "any.c"
#include "boolean.c"
#include "string.c"

// COOL Compiler - Int
// See natives.c for struct Int

// Int implementation-only constants
const int INT_STR_SIZE= 1024;

// Int equals:
// override def equals(other : Any) : Boolean = native;
Boolean* Int_equals(Int *self, Any *other) {
	Int *i = (Int*) other;
	Boolean *ret= Boolean_constructor(false);
	
	if (self->value == i->value) {
		ret->value= true;
	} // Otherwise, new Boolean init'd to false

	return ret;
}

// Int toString:
// override def toString() : String = native;
String* Int_toString(Int *self) {
	String *ret= String_constructor("");
	ret->str_field= calloc(INT_STR_SIZE, sizeof(char));
	snprintf(ret->str_field, INT_STR_SIZE - 1, "%d", self->value);
	return ret;
}

// ----- Prototyping C-defined convenience Methods ----- //
// Int plus:
// Not COOL defined
Int* Int_plus(Int *self, Int *other) {
	int res= self->value + other->value;
	Int *ret= Int_constructor( res );
	return ret;
}

// Int minus:
// Not COOL defined
Int* Int_minus(Int *self, Int *other) {
	int res= self->value - other->value;
	Int *ret= Int_constructor( res );
	return ret;
}

// Int mult:
// Not COOL defined
Int* Int_mult(Int *self, Int *other) {
	int res= self->value * other->value;
	Int *ret= Int_constructor( res );
	return ret;
}

// Int div:
// Not COOL defined
Int* Int_div(Int *self, Int *other) {
	int res= self->value / other->value;
	Int *ret= Int_constructor( res );
	return ret;
}

// Int lt:
// Not COOL defined
Boolean* Int_lt(Int *self, Int *other) {
	int res= self->value < other->value;
	Boolean *ret= Boolean_constructor( res );
	return ret;
}

// Int le:
// Not COOL defined
Boolean* Int_le(Int *self, Int *other) {
	int res= self->value <= other->value;
	Boolean *ret= Boolean_constructor( res );
	return ret;
}
// ----- End Prototype C-defined convenience Methods ----- //

#endif
