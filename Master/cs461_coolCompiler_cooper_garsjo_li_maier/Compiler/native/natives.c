#ifndef _NATIVES_C
#define _NATIVES_C

#include <stdlib.h>
#include <string.h>

// Convenience definitons
#define true 1
#define false 0

// ----------------- PROTOTYPES ----------------- //
struct obj_Any;
struct obj_Boolean;
struct obj_Int;
struct obj_IO;
struct obj_String;
struct obj_Symbol;
struct obj_Unit;
typedef struct obj_Any Any;
typedef struct obj_Boolean Boolean;
typedef struct obj_Int Int;
typedef struct obj_IO IO;
typedef struct obj_String String;
typedef struct obj_Symbol Symbol;
typedef struct obj_Unit Unit;

// ----------------- CLASS ANY ----------------- //
// Any Class Struct:
struct class_Any {
	void *parent;
	String* (*Any_toString)(Any*);
	Boolean* (*Any_equals)(Any*, Any*);
};
typedef struct class_Any ClassAny;

// Any Class Method Prototypes
String* Any_toString(Any *self);
Boolean* Any_equals(Any*,Any*);

// Any Class Global Definition
ClassAny CLASS_ANY= {NULL, Any_toString, Any_equals};

// Any struct
struct obj_Any {
		ClassAny *cls;
};

// Any constructor:
// Not COOL defined
Any* Any_constructor() {
	Any *self= (Any*) calloc(1, sizeof(Any));
	self->cls= &CLASS_ANY;
	return self;
}


// ----------------- CLASS BOOLEAN ----------------- //
// Boolean Class Struct
struct class_Boolean {
	void *parent;
	String* (*Boolean_toString)();
	Boolean* (*Boolean_equals)(Boolean*, Any*);
};
typedef struct class_Boolean ClassBoolean;

// Boolean Class Method Prototypes
extern String* Boolean_toString(Boolean*);
Boolean* Boolean_equals(Boolean*, Any*);

// Boolean Class Global Definition
ClassBoolean CLASS_BOOLEAN= {&CLASS_ANY, Boolean_toString, Boolean_equals};

// Boolean Struct:
struct obj_Boolean {
	ClassBoolean *cls;
	int value;	// Remember: In C, there are no booleans
};

// Boolean constructor:
// Not COOL defined
Boolean* Boolean_constructor(int value) {
	Boolean *self= (Boolean*) calloc(1, sizeof(Boolean));
	self->cls= &CLASS_BOOLEAN;

	// Special error-checking to clamp legal C bool values to [0,1]
	if (value) {
		self->value= true;
	} else {
		self->value= false;
	}

	return self;
}


// ----------------- CLASS INT ----------------- //
// Int Class Struct:
struct class_Int {
	void *parent;
	String* (*Int_toString)(Int*);
	Boolean* (*Int_equals)(Int*,Any*);
	Int* (*Int_plus)(Int*, Int*);
	Int* (*Int_minus)(Int*, Int*);
	Int* (*Int_mult)(Int*, Int*);
	Int* (*Int_div)(Int*, Int*);
	Boolean* (*Int_lt)(Int*,Int*);
	Boolean* (*Int_le)(Int*,Int*);
	
};
typedef struct class_Int ClassInt;

// Int Class Method Prototypes
String* Int_toString(Int*);
Boolean* Int_equals(Int*, Any*);
Int* Int_plus(Int*, Int*);
Int* Int_minus(Int*, Int*);
Int* Int_mult(Int*, Int*);
Int* Int_div(Int*, Int*);
Boolean* Int_lt(Int*, Int*);
Boolean* Int_le(Int*, Int*);

// Int Class Global Definition
ClassInt CLASS_INT= {&CLASS_ANY, 
					Int_toString, 
					Int_equals,
					Int_plus,
					Int_minus,
					Int_mult,
					Int_div,
					Int_lt,
					Int_le };

// Int Struct:
struct obj_Int {
	ClassInt *cls;
	int value;
};

// Int constructor:
// Not COOL defined
Int* Int_constructor(int value) {
	Int *self= (Int*) calloc(1, sizeof(Int));
	self->cls= &CLASS_INT;
	self->value= value;
	return self;
}


// ----------------- CLASS IO ----------------- //
// IO Class Struct:
struct class_IO {
	void *parent;
	void (*IO_abort)(IO*,String*);
	IO* (*IO_out)(IO*,String*);
	Boolean* (*IO_is_null)(IO*,Any*);
	IO* (*IO_out_any)(IO*,Any*);
	String* (*IO_in)(IO*);
	Symbol* (*IO_symbol)(IO*,String*);
	String* (*IO_symbol_name)(IO*,Symbol*);
};
typedef struct class_IO ClassIO;

// IO Class Method Prototypes:
void IO_abort(IO*,String*);
IO* IO_out(IO*,String*);
extern Boolean* IO_is_null(IO*,Any*);
extern IO* IO_out_any(IO*,Any*);
String* IO_in(IO*);
Symbol* IO_symbol(IO*,String*);
String* IO_symbol_name(IO*,Symbol*);

// IO Class Global Definition
ClassIO CLASS_IO= {&CLASS_ANY, IO_abort, IO_out, IO_is_null, IO_out_any, IO_in, IO_symbol, IO_symbol_name};

// IO Struct:
struct obj_IO {
	ClassIO *cls;
};

// IO constructor:
// No COOL defintion
IO* IO_constructor() {
	IO *self= (IO*) calloc(1, sizeof(IO));
	self->cls= &CLASS_IO;
	return self;
}


// ----------------- CLASS STRING ----------------- //
// String Class Struct:
struct class_String {
	void *parent;
	String* (*String_toString)(String*);
	Boolean* (*String_equals)(String*,Any*);
	Int* (*String_length)(String*);
	String* (*String_concat)(String*,String*);
	String* (*String_substring)(String*,Int*,Int*);
	Int* (*String_charAt)(String*,Int*);
	Int* (*String_indexOf)(String*,String*);
};
typedef struct class_String ClassString;

// String Class Method Prototypes
extern String* String_toString(String*);
Boolean* String_equals(String*,Any*);
extern Int* String_length(String*);
String* String_concat(String*,String*);
String* String_substring(String*,Int*,Int*);
Int* String_charAt(String*,Int*);
extern Int* String_indexOf(String*, String*);

// String Class Global Definition
ClassString CLASS_STRING= {&CLASS_ANY, 
						String_toString,
						String_equals,
						String_length,
						String_concat,
						String_substring,
						String_charAt,
						String_indexOf};

// String Struct:
struct obj_String {
	ClassString *cls;
	Int *length;
	char *str_field;
};

// String constructor:
// Not COOL defined (technically not callable, as Strings should be non-instantiable)
// Lulz, I guess passing in a char array <?>might<?> be right
String* String_constructor(char *s) {
	String *self= (String*) calloc(1, sizeof(String));
	self->cls= &CLASS_STRING;
	self->length= Int_constructor(0);

	int len= strlen(s);
	self->str_field= (char*) malloc(sizeof(char) * (len+1));
	strncpy(self->str_field, s, len+1);

	return self;
}


// ----------------- CLASS SYMBOL ----------------- //
// Symbol Class Struct:
struct class_Symbol {
	void *parent;
	String* (*Symbol_toString)(Symbol*);
	Int* (*Symbol_hashCode)(Symbol*);
};
typedef struct class_Symbol ClassSymbol;

// Symbol Class Method Prototypes
extern String* Symbol_toString(Symbol*);
extern Int* Symbol_hashCode(Symbol*);

// Symbol Class Global Definition
ClassSymbol CLASS_SYMBOL= {&CLASS_ANY, Symbol_toString, Symbol_hashCode};

struct obj_Symbol {
	ClassSymbol *cls;
	int next;
	String *name;
	Int *hash;
};

// Symbol constructor:
// Not COOL defined
Symbol* Symbol_constructor(String *str) {
	Symbol *self= (Symbol*) calloc(1, sizeof(Symbol));
	self->cls= &CLASS_SYMBOL;
	// Unsure how to implement next

	self->name= String_constructor("");
	self->hash= Int_constructor(0);

	return self;
}


// ----------------- CLASS UNIT ----------------- //
// Unit Class Struct:
struct class_Unit {
	void *parent;
};
typedef struct class_Unit ClassUnit;

// Unit Class Global Definition
ClassUnit CLASS_UNIT= {&CLASS_ANY};

// Unit Struct:
struct obj_Unit {
	ClassUnit *cls;
};

// Unit constructor:
// Not COOL defined (technically not callable, as Units should be non-instantiable)
Unit* Unit_constructor() {
	Unit *self= (Unit*) calloc(1, sizeof(Unit));
	self->cls= &CLASS_UNIT;
	return self;
}

#endif
