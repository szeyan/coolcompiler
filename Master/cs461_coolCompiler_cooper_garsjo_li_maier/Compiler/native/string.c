#ifndef _STRING_C
#define _STRING_C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "natives.c"
#include "any.c"
#include "boolean.c"
#include "int.c"

// COOL Compiler - String
// See natives.c for struct String

// String charAt:
// def charAt(index : Int) : Int = native;
Int* String_charAt(String *self, Int *index) {
	Int *ret= Int_constructor(0);
	int idx= index->value;

	// Currently, do without bounds-checking
	ret->value= self->str_field[idx];
	return ret;
}

// String concat:
// def concat(arg : String) : String = native;
String* String_concat(String *self, String *arg) {
	String *ret= String_constructor(self->str_field);
	
	int len1= strlen(self->str_field);		// Ignore null-terminator
	int len2= strlen(arg->str_field) + 1;	// Capture null-terminator
	
	// Realloc the return String's str_field; the given string will be untouched
	// Then, concat the strings safely
	char *newmem= realloc(ret->str_field, len1 + len2);
	if (newmem == NULL) {
		exit(1);
	}

	strncat(ret->str_field, arg->str_field, len1 + len2);
	
	return ret;
}

// String equals:
// override def equals(other : Any) : Boolean = native;
Boolean* String_equals(String *self, Any *other) {
	String *s= (String*) other;
	Boolean *ret= Boolean_constructor(false);
	
	int res= strcmp(self->str_field, s->str_field);
	if (res == 0) {
		ret->value= true;
	} // Otherwise, returned Boolean init'd to false

	return ret;
}

// String substring:
// def substring(start : Int, end : int) : String = native;
String* String_substring(String *self, Int *start, Int *end) {
	String *ret= String_constructor(self->str_field);
	int istart= start->value;
	int iend= 	end->value;
	int len= 	istart - iend + 1;	// Account for soon-to-be null-terminator

	char *substart= &( self->str_field[istart] );
	ret->str_field= (char*) malloc(sizeof(char) * len);
	strncpy(ret->str_field, substart, len);
	ret->str_field[len-1]= '\0';

	return ret;
}

#endif
