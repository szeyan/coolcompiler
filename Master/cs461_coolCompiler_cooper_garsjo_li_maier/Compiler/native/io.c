#ifndef _IO_C
#define _IO_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "natives.c"
#include "any.c"
#include "string.c"

// COOL Compiler - IO
// See natives.c for struct IO

// IO implementation-only constants
const int IO_EXIT_STATUS= 0;
const int IO_IN_BUF_SIZE= 1024;

// IO abort:
// def abort(message : String) : Nothing = native;
void IO_abort(IO *self, String *message) {
	printf("%s", message->str_field);
	exit(IO_EXIT_STATUS);
}

// IO out:
// def out(arg : String) : IO = native;
// Returns itself as an IO reference
IO* IO_out(IO *self, String *arg) {
	printf("%s", arg->str_field);
	return self;
}

// IO in:
// def in() : String = native;
// Returns a string from stdin
// NOTE: Impelementation assumes POSIX compliance
// TODO handle input in a loop to fetch all input, not just N bytes
String* IO_in(IO *self) {
	char *buf= (char*) calloc(IO_IN_BUF_SIZE, sizeof(char));

	// Read in and reallocate if less than max
	int nread= read(STDIN_FILENO, buf, IO_IN_BUF_SIZE);
	if (nread < IO_IN_BUF_SIZE) {
		buf= (char*) realloc(buf, sizeof(char) * nread);
		buf[nread - 1]= '\0';
		buf[nread]= '\0';
	}
	
	String *ret= String_constructor(buf);

	return ret;
}

// IO symbol:
// def symbol(name : String) : Symbol = native;
// Returns a newly-allocated Symbol reference
Symbol* IO_symbol(IO *self, String *name) {
	// TODO Finish after figuring out Symbol implementation
	Symbol *ret= Symbol_constructor(name);
	return ret;
}

// IO symbol_name:
// def symbol_name(sym : Symbol) : String = native;
String* IO_symbol_name(IO *self, Symbol *sym) {
	// TODO Finish after figuring out Symbol implementation
	String *ret= String_constructor("");
	return ret;
}

#endif
