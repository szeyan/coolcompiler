#ifndef _ANY_C
#define _ANY_C

#include <stdio.h>
#include <stdlib.h>

#include "natives.c"
#include "string.c"

// COOL Compiler - Any
// See natives.c for struct Any

// Any implementation-only constants
const int ANY_STR_SIZE= 1024;

// Any toString:
// def toString() : String = native;
String* Any_toString(Any *self) {
	String *ret= String_constructor("");
	ret->str_field= calloc(ANY_STR_SIZE, sizeof(char));
	snprintf(ret->str_field, ANY_STR_SIZE - 1, "Any:<0x%x>", (unsigned int) self);
	return ret;
}

// Any equals:
// def equals(other : Any) : Boolean = native;
Boolean* Any_equals(Any *self, Any *other) {
	Boolean *ret= Boolean_constructor(false);
	ret->value= (self == other);

	return ret;
}

#endif
