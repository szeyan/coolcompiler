# Shortcut for running the Driver program
BEAVPTH=lib/Beaver
BEAVRT=beaver-rt-0.9.11.jar
DRIVER=cool.Driver
LLVMPATH=lib/llvm
LLC=./$LLVMPATH/llc
CLANG=./$LLVMPATH/clang
CC=gcc

if [ $# -lt 1 ]
then
	echo "Usage: ./run.sh <cool source file>"
	echo "Running compiler over basic.cool definition file..."
fi

java -cp .:$BEAVPTH/$BEAVRT $DRIVER $*
if [ $? -eq 0 ]
then
	$CLANG native/main.c -c
	$LLC default.ll
	$CC main.o default.o
fi
