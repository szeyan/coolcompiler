; ModuleID = 'test.c'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-n8:16:32-S128"
target triple = "i386-pc-linux-gnu"

%struct.class_Any = type { i8*, %struct.obj_String* (%struct.obj_Any*)*, %struct.obj_Boolean* (%struct.obj_Any*, %struct.obj_Any*)* }
%struct.obj_String = type { %struct.class_String*, %struct.obj_Int*, i8* }
%struct.class_String = type { i8*, %struct.obj_String* (%struct.obj_String*)*, %struct.obj_Boolean* (%struct.obj_String*, %struct.obj_Any*)*, %struct.obj_Int* (%struct.obj_String*)*, %struct.obj_String* (%struct.obj_String*, %struct.obj_String*)*, %struct.obj_String* (%struct.obj_String*, %struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_String*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_String*, %struct.obj_String*)* }
%struct.obj_Boolean = type { %struct.class_Boolean*, i32 }
%struct.class_Boolean = type { i8*, %struct.obj_String* (...)*, %struct.obj_Boolean* (%struct.obj_Boolean*, %struct.obj_Any*)* }
%struct.obj_Any = type { %struct.class_Any* }
%struct.obj_Int = type { %struct.class_Int*, i32 }
%struct.class_Int = type { i8*, %struct.obj_String* (%struct.obj_Int*)*, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Any*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Int*)*, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Int*)* }
%struct.class_IO = type { i8*, void (%struct.obj_IO*, %struct.obj_String*)*, %struct.obj_IO* (%struct.obj_IO*, %struct.obj_String*)*, %struct.obj_Boolean* (%struct.obj_IO*, %struct.obj_Any*)*, %struct.obj_IO* (%struct.obj_IO*, %struct.obj_Any*)*, %struct.obj_String* (%struct.obj_IO*)*, %struct.obj_Symbol* (%struct.obj_IO*, %struct.obj_String*)*, %struct.obj_String* (%struct.obj_IO*, %struct.obj_Symbol*)* }
%struct.obj_IO = type { %struct.class_IO* }
%struct.obj_Symbol = type { %struct.class_Symbol*, i32, %struct.obj_String*, %struct.obj_Int* }
%struct.class_Symbol = type { i8*, %struct.obj_String* (%struct.obj_Symbol*)*, %struct.obj_Int* (%struct.obj_Symbol*)* }
%struct.class_Unit = type { i8* }
%struct.obj_Unit = type { %struct.class_Unit* }

@CLASS_ANY = global %struct.class_Any { i8* null, %struct.obj_String* (%struct.obj_Any*)* @Any_toString, %struct.obj_Boolean* (%struct.obj_Any*, %struct.obj_Any*)* @Any_equals }, align 4
@CLASS_BOOLEAN = global %struct.class_Boolean { i8* bitcast (%struct.class_Any* @CLASS_ANY to i8*), %struct.obj_String* (...)* bitcast (%struct.obj_String* (%struct.obj_Boolean*)* @Boolean_toString to %struct.obj_String* (...)*), %struct.obj_Boolean* (%struct.obj_Boolean*, %struct.obj_Any*)* @Boolean_equals }, align 4
@CLASS_INT = global %struct.class_Int { i8* bitcast (%struct.class_Any* @CLASS_ANY to i8*), %struct.obj_String* (%struct.obj_Int*)* @Int_toString, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Any*)* @Int_equals, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)* @Int_plus, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)* @Int_minus, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)* @Int_mult, %struct.obj_Int* (%struct.obj_Int*, %struct.obj_Int*)* @Int_div, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Int*)* @Int_lt, %struct.obj_Boolean* (%struct.obj_Int*, %struct.obj_Int*)* @Int_le }, align 4
@CLASS_IO = global %struct.class_IO { i8* bitcast (%struct.class_Any* @CLASS_ANY to i8*), void (%struct.obj_IO*, %struct.obj_String*)* @IO_abort, %struct.obj_IO* (%struct.obj_IO*, %struct.obj_String*)* @IO_out, %struct.obj_Boolean* (%struct.obj_IO*, %struct.obj_Any*)* @IO_is_null, %struct.obj_IO* (%struct.obj_IO*, %struct.obj_Any*)* @IO_out_any, %struct.obj_String* (%struct.obj_IO*)* @IO_in, %struct.obj_Symbol* (%struct.obj_IO*, %struct.obj_String*)* @IO_symbol, %struct.obj_String* (%struct.obj_IO*, %struct.obj_Symbol*)* @IO_symbol_name }, align 4
@CLASS_STRING = global %struct.class_String { i8* bitcast (%struct.class_Any* @CLASS_ANY to i8*), %struct.obj_String* (%struct.obj_String*)* @String_toString, %struct.obj_Boolean* (%struct.obj_String*, %struct.obj_Any*)* @String_equals, %struct.obj_Int* (%struct.obj_String*)* @String_length, %struct.obj_String* (%struct.obj_String*, %struct.obj_String*)* @String_concat, %struct.obj_String* (%struct.obj_String*, %struct.obj_Int*, %struct.obj_Int*)* @String_substring, %struct.obj_Int* (%struct.obj_String*, %struct.obj_Int*)* @String_charAt, %struct.obj_Int* (%struct.obj_String*, %struct.obj_String*)* @String_indexOf }, align 4
@CLASS_SYMBOL = global %struct.class_Symbol { i8* bitcast (%struct.class_Any* @CLASS_ANY to i8*), %struct.obj_String* (%struct.obj_Symbol*)* @Symbol_toString, %struct.obj_Int* (%struct.obj_Symbol*)* @Symbol_hashCode }, align 4
@.str = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@CLASS_UNIT = global %struct.class_Unit { i8* bitcast (%struct.class_Any* @CLASS_ANY to i8*) }, align 4

declare %struct.obj_String* @Any_toString(%struct.obj_Any*)

declare %struct.obj_Boolean* @Any_equals(%struct.obj_Any*, %struct.obj_Any*)

define %struct.obj_Any* @Any_constructor() nounwind {
  %self = alloca %struct.obj_Any*, align 4
  %1 = call noalias i8* @calloc(i32 1, i32 4) nounwind
  %2 = bitcast i8* %1 to %struct.obj_Any*
  store %struct.obj_Any* %2, %struct.obj_Any** %self, align 4
  %3 = load %struct.obj_Any** %self, align 4
  %4 = getelementptr inbounds %struct.obj_Any* %3, i32 0, i32 0
  store %struct.class_Any* @CLASS_ANY, %struct.class_Any** %4, align 4
  %5 = load %struct.obj_Any** %self, align 4
  ret %struct.obj_Any* %5
}

declare noalias i8* @calloc(i32, i32) nounwind

declare %struct.obj_String* @Boolean_toString(%struct.obj_Boolean*)

declare %struct.obj_Boolean* @Boolean_equals(%struct.obj_Boolean*, %struct.obj_Any*)

define %struct.obj_Boolean* @Boolean_constructor(i32 %value) nounwind {
  %1 = alloca i32, align 4
  %self = alloca %struct.obj_Boolean*, align 4
  store i32 %value, i32* %1, align 4
  %2 = call noalias i8* @calloc(i32 1, i32 8) nounwind
  %3 = bitcast i8* %2 to %struct.obj_Boolean*
  store %struct.obj_Boolean* %3, %struct.obj_Boolean** %self, align 4
  %4 = load %struct.obj_Boolean** %self, align 4
  %5 = getelementptr inbounds %struct.obj_Boolean* %4, i32 0, i32 0
  store %struct.class_Boolean* @CLASS_BOOLEAN, %struct.class_Boolean** %5, align 4
  %6 = load i32* %1, align 4
  %7 = icmp ne i32 %6, 0
  br i1 %7, label %8, label %11

; <label>:8                                       ; preds = %0
  %9 = load %struct.obj_Boolean** %self, align 4
  %10 = getelementptr inbounds %struct.obj_Boolean* %9, i32 0, i32 1
  store i32 1, i32* %10, align 4
  br label %14

; <label>:11                                      ; preds = %0
  %12 = load %struct.obj_Boolean** %self, align 4
  %13 = getelementptr inbounds %struct.obj_Boolean* %12, i32 0, i32 1
  store i32 0, i32* %13, align 4
  br label %14

; <label>:14                                      ; preds = %11, %8
  %15 = load %struct.obj_Boolean** %self, align 4
  ret %struct.obj_Boolean* %15
}

declare %struct.obj_String* @Int_toString(%struct.obj_Int*)

declare %struct.obj_Boolean* @Int_equals(%struct.obj_Int*, %struct.obj_Any*)

declare %struct.obj_Int* @Int_plus(%struct.obj_Int*, %struct.obj_Int*)

declare %struct.obj_Int* @Int_minus(%struct.obj_Int*, %struct.obj_Int*)

declare %struct.obj_Int* @Int_mult(%struct.obj_Int*, %struct.obj_Int*)

declare %struct.obj_Int* @Int_div(%struct.obj_Int*, %struct.obj_Int*)

declare %struct.obj_Boolean* @Int_lt(%struct.obj_Int*, %struct.obj_Int*)

declare %struct.obj_Boolean* @Int_le(%struct.obj_Int*, %struct.obj_Int*)

define %struct.obj_Int* @Int_constructor(i32 %value) nounwind {
  %1 = alloca i32, align 4
  %self = alloca %struct.obj_Int*, align 4
  store i32 %value, i32* %1, align 4
  %2 = call noalias i8* @calloc(i32 1, i32 8) nounwind
  %3 = bitcast i8* %2 to %struct.obj_Int*
  store %struct.obj_Int* %3, %struct.obj_Int** %self, align 4
  %4 = load %struct.obj_Int** %self, align 4
  %5 = getelementptr inbounds %struct.obj_Int* %4, i32 0, i32 0
  store %struct.class_Int* @CLASS_INT, %struct.class_Int** %5, align 4
  %6 = load i32* %1, align 4
  %7 = load %struct.obj_Int** %self, align 4
  %8 = getelementptr inbounds %struct.obj_Int* %7, i32 0, i32 1
  store i32 %6, i32* %8, align 4
  %9 = load %struct.obj_Int** %self, align 4
  ret %struct.obj_Int* %9
}

declare void @IO_abort(%struct.obj_IO*, %struct.obj_String*)

declare %struct.obj_IO* @IO_out(%struct.obj_IO*, %struct.obj_String*)

declare %struct.obj_Boolean* @IO_is_null(%struct.obj_IO*, %struct.obj_Any*)

declare %struct.obj_IO* @IO_out_any(%struct.obj_IO*, %struct.obj_Any*)

declare %struct.obj_String* @IO_in(%struct.obj_IO*)

declare %struct.obj_Symbol* @IO_symbol(%struct.obj_IO*, %struct.obj_String*)

declare %struct.obj_String* @IO_symbol_name(%struct.obj_IO*, %struct.obj_Symbol*)

define %struct.obj_IO* @IO_constructor() nounwind {
  %self = alloca %struct.obj_IO*, align 4
  %1 = call noalias i8* @calloc(i32 1, i32 4) nounwind
  %2 = bitcast i8* %1 to %struct.obj_IO*
  store %struct.obj_IO* %2, %struct.obj_IO** %self, align 4
  %3 = load %struct.obj_IO** %self, align 4
  %4 = getelementptr inbounds %struct.obj_IO* %3, i32 0, i32 0
  store %struct.class_IO* @CLASS_IO, %struct.class_IO** %4, align 4
  %5 = load %struct.obj_IO** %self, align 4
  ret %struct.obj_IO* %5
}

declare %struct.obj_String* @String_toString(%struct.obj_String*)

declare %struct.obj_Boolean* @String_equals(%struct.obj_String*, %struct.obj_Any*)

declare %struct.obj_Int* @String_length(%struct.obj_String*)

declare %struct.obj_String* @String_concat(%struct.obj_String*, %struct.obj_String*)

declare %struct.obj_String* @String_substring(%struct.obj_String*, %struct.obj_Int*, %struct.obj_Int*)

declare %struct.obj_Int* @String_charAt(%struct.obj_String*, %struct.obj_Int*)

declare %struct.obj_Int* @String_indexOf(%struct.obj_String*, %struct.obj_String*)

define %struct.obj_String* @String_constructor(i8* %s) nounwind {
  %1 = alloca i8*, align 4
  %self = alloca %struct.obj_String*, align 4
  %len = alloca i32, align 4
  store i8* %s, i8** %1, align 4
  %2 = call noalias i8* @calloc(i32 1, i32 12) nounwind
  %3 = bitcast i8* %2 to %struct.obj_String*
  store %struct.obj_String* %3, %struct.obj_String** %self, align 4
  %4 = load %struct.obj_String** %self, align 4
  %5 = getelementptr inbounds %struct.obj_String* %4, i32 0, i32 0
  store %struct.class_String* @CLASS_STRING, %struct.class_String** %5, align 4
  %6 = call %struct.obj_Int* @Int_constructor(i32 0)
  %7 = load %struct.obj_String** %self, align 4
  %8 = getelementptr inbounds %struct.obj_String* %7, i32 0, i32 1
  store %struct.obj_Int* %6, %struct.obj_Int** %8, align 4
  %9 = load i8** %1, align 4
  %10 = call i32 @strlen(i8* %9) nounwind readonly
  store i32 %10, i32* %len, align 4
  %11 = load i32* %len, align 4
  %12 = add nsw i32 %11, 1
  %13 = mul i32 1, %12
  %14 = call noalias i8* @malloc(i32 %13) nounwind
  %15 = load %struct.obj_String** %self, align 4
  %16 = getelementptr inbounds %struct.obj_String* %15, i32 0, i32 2
  store i8* %14, i8** %16, align 4
  %17 = load %struct.obj_String** %self, align 4
  %18 = getelementptr inbounds %struct.obj_String* %17, i32 0, i32 2
  %19 = load i8** %18, align 4
  %20 = load i8** %1, align 4
  %21 = load i32* %len, align 4
  %22 = add nsw i32 %21, 1
  %23 = call i8* @strncpy(i8* %19, i8* %20, i32 %22) nounwind
  %24 = load %struct.obj_String** %self, align 4
  ret %struct.obj_String* %24
}

declare i32 @strlen(i8*) nounwind readonly

declare noalias i8* @malloc(i32) nounwind

declare i8* @strncpy(i8*, i8*, i32) nounwind

declare %struct.obj_String* @Symbol_toString(%struct.obj_Symbol*)

declare %struct.obj_Int* @Symbol_hashCode(%struct.obj_Symbol*)

define %struct.obj_Symbol* @Symbol_constructor(%struct.obj_String* %str) nounwind {
  %1 = alloca %struct.obj_String*, align 4
  %self = alloca %struct.obj_Symbol*, align 4
  store %struct.obj_String* %str, %struct.obj_String** %1, align 4
  %2 = call noalias i8* @calloc(i32 1, i32 16) nounwind
  %3 = bitcast i8* %2 to %struct.obj_Symbol*
  store %struct.obj_Symbol* %3, %struct.obj_Symbol** %self, align 4
  %4 = load %struct.obj_Symbol** %self, align 4
  %5 = getelementptr inbounds %struct.obj_Symbol* %4, i32 0, i32 0
  store %struct.class_Symbol* @CLASS_SYMBOL, %struct.class_Symbol** %5, align 4
  %6 = call %struct.obj_String* @String_constructor(i8* getelementptr inbounds ([1 x i8]* @.str, i32 0, i32 0))
  %7 = load %struct.obj_Symbol** %self, align 4
  %8 = getelementptr inbounds %struct.obj_Symbol* %7, i32 0, i32 2
  store %struct.obj_String* %6, %struct.obj_String** %8, align 4
  %9 = call %struct.obj_Int* @Int_constructor(i32 0)
  %10 = load %struct.obj_Symbol** %self, align 4
  %11 = getelementptr inbounds %struct.obj_Symbol* %10, i32 0, i32 3
  store %struct.obj_Int* %9, %struct.obj_Int** %11, align 4
  %12 = load %struct.obj_Symbol** %self, align 4
  ret %struct.obj_Symbol* %12
}

define %struct.obj_Unit* @Unit_constructor() nounwind {
  %self = alloca %struct.obj_Unit*, align 4
  %1 = call noalias i8* @calloc(i32 1, i32 4) nounwind
  %2 = bitcast i8* %1 to %struct.obj_Unit*
  store %struct.obj_Unit* %2, %struct.obj_Unit** %self, align 4
  %3 = load %struct.obj_Unit** %self, align 4
  %4 = getelementptr inbounds %struct.obj_Unit* %3, i32 0, i32 0
  store %struct.class_Unit* @CLASS_UNIT, %struct.class_Unit** %4, align 4
  %5 = load %struct.obj_Unit** %self, align 4
  ret %struct.obj_Unit* %5
}

define i32 @main() nounwind {
  %1 = alloca i32, align 4
  %i = alloca %struct.obj_Int*, align 4
  store i32 0, i32* %1
  %2 = call %struct.obj_Int* @Int_constructor(i32 5)
  store %struct.obj_Int* %2, %struct.obj_Int** %i, align 4
  ret i32 0
}
