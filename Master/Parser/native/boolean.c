#ifndef _BOOLEAN_C
#define _BOOLEAN_C

#include <stdlib.h>

#include "natives.c"
#include "any.c"
#include "string.c"

// COOL COMPILER - Boolean
// See natives.c for struct Boolean

// Boolean equals:
// override def equals(other : Any) : Boolean = native;
// Returns a newly-allocated Boolean with the value of the comparison
Boolean* Boolean_equals(Boolean *self, Any *other) {
	Boolean *b= (Boolean*) other;
	Boolean *ret= Boolean_constructor(false);
	
	if (self->value == b->value) {
		b->value= true;
	}
	
	return ret;
}

// ----- Prototyping C-defined convenience Methods ----- //
// Boolean not:
// Not COOL defined
Boolean* Boolean_not(Boolean *self) {
	int res= !(self->value);
	Boolean *ret= Boolean_constructor( res );
	return ret;
}
// ----- End Prototype C-defined convenience Methods ----- //

#endif
