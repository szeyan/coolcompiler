/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.typeCheck.OLD;

import beaver.Symbol;
import cool.Terminals;
import cool.ast.*;
import cool.typeCheck.ClassObj;
import cool.typeCheck.M;
import cool.typeCheck.MethodObj;
import cool.typeCheck.SymbolTable;
import cool.typeCheck.VarAttribute;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author kgarsjo
 */
public class TypeChecker {
    
    public static class TypeException extends Exception {
        
        public TypeException(String error) {
            super(error);
        }
    }
    
    private M mtable;
    private SymbolTable stable;
    private ClassObj Any= null;
    private ClassObj Nothing= null;
    private ClassObj Unit= null;
    private ClassObj Null= null;
    private ClassObj Boolean= null;
    private ClassObj Integer= null;
    private ClassObj String= null;
    
    public TypeChecker(M mtable, SymbolTable stable) {
        this.mtable= mtable;
        this.stable= stable;
        Any= mtable.getClass("Any");
        Nothing= mtable.getClass("Nothing");
        Unit= mtable.getClass("Unit");
        Null= mtable.getClass("Null");
        Boolean= mtable.getClass("Boolean");
        Integer= mtable.getClass("Integer");
        String= mtable.getClass("String");
    }
    
    public void checkType(ClassObj expected, Node body) throws TypeException {
        ClassObj actual= eval(body);
        if (!expected.equals(actual) || conformsTo(expected, actual)) {
            // TODO Generate more informative type error messages with row/column values
            throw new TypeException(
                "Type " + expected.getName() + " did not match type " + actual.getName() +
                " evaluated from body " + body.toString()
            );
        }
    }
    
    // TODO Do actual evaluation
    public ClassObj eval(Symbol body) throws TypeException {
        
        // Edge case, null node
        if (body == null) {
            return null;
        }
        
        // Use terminal IDs to type literals
        int term= body.getId();
        
        // Use node child lists to type complex expressions
        LinkedList children= null;
        Node currNode= null;
        
        
        // Literals and Other Easy Cases
        switch (term) {
            case Terminals.BOOLEAN:
                return Boolean;
            case Terminals.INTEGER:
                return Integer;
            case Terminals.NULL:
                return Null;
            case Terminals.STRING:
                return String;
            case Terminals.ID:
            case Terminals.THIS:    // TODO Make sure THIS is being stored that way
                //UNCOMMENT//return stable.table.get(body.value).type;
            case Terminals.NATIVE:
                return Any;
        }
        
        
        // Binary Arithmetic Expression Subtypes
        if (body instanceof PlusNode        // Plus Expr
         || body instanceof MinusNode       // Minus Expr
         || body instanceof TimesNode       // Mult Expr
         || body instanceof DivideNode) {   // Div Expr
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(Integer) && t2.equals(Integer)) {
                return Integer;
            }
            throw new TypeException("Arithmetic undefined for " + t1.getName() + " and " + t2.getName());
        }
        
        
        // Binary Boolean Expression Subtypes
        if (body instanceof LessThanNode        // Less Than
         || body instanceof LessEqualNode) {       // Less than or Equals
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(Integer) && t2.equals(Integer)) {
                return Boolean;
            }
            throw new TypeException("Relationals undefined for " + t1.getName() + " and " + t2.getName());
        }
        
        
        // Binary Equality Type
        if (body instanceof EqualNode) {
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(t2)) {
                if (t1.equals(Boolean) || t1.equals(Integer) || t1.equals(String)) {
                    return Boolean;
                }
            }
            throw new TypeException("Equality undefined for " + t1.getName() + " and " + t2.getName());
        }
        
        
        // Unary Expression Subtypes
        if (body instanceof NotNode) {          // Not Expr
            currNode= (Node) body;
            ClassObj t1= eval( (Symbol) currNode.getList().get(0) );
            if (t1.equals(Boolean)) { return Boolean; }
            throw new TypeException("Boolean negation undefined for " + t1.getName());
        }
        if (body instanceof UnaryMinusNode) {    // Integer Negation Expr
            currNode= (Node) body;
            ClassObj t1= eval( (Symbol) currNode.getList().get(0) );
            if (t1.equals(Integer)) { return Integer; }
            throw new TypeException("Integer negation undefined for " + t1.getName());
        }
        
        
        // Control Expression Subtype
        if (body instanceof IfElseNode) {       // If Statement
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            ClassObj t3= eval( (Symbol) children.get(2) );
            if (t1.equals(Boolean)) { 
                return join(t2, t3);
            }
            throw new TypeException("If condition undefined for type " + t1.getName());
        }
        if (body instanceof WhileNode) {        // While Loop
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(Boolean)) {
                // TODO Check some specific type rules for LOOP
                return Unit;
            }
            throw new TypeException("While condition undefined for type " + t1.getName());
        }
        
        
        // Dot Access Expression Subtype
        if (body instanceof DotNode) {
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj cls= eval( (Symbol) children.get(0) );
            Symbol member= (Symbol) children.get(1);
            
            if (member instanceof PrimaryNode) {
                currNode= (Node) body;
                children= currNode.getList();
                if (children.size() == 2) {         // Static Method Dispatch
                   
                    // Fetch method, continue if not null
                    String id= (String) ((Symbol) children.get(0)).value;
                    MethodObj method= cls.getMethod(id);
                    if (method != null) {
                        LinkedHashMap<String, String> args= method.getParams();
                        
                        // Parse out the given arguments from nested Nodes
                        Node head= (ActualsListNode) children.get(1);
                        LinkedList<ExprNode> given= new LinkedList<ExprNode>();
                        while (!(head instanceof CasesNode)) {
                            given.addFirst( (ExprNode) head.getList().get(1) );
                            head= (Node) head.getList().get(0);
                        }
                        given.addFirst( (ExprNode) head );
                        
                        // Check that all given args conform to expected args
                        if (given.size() == args.size()) {
                            for (ExprNode e : given) {
                                ClassObj t1= eval(e);
                                ClassObj t2= mtable.getClass(method.getReturnType());
                                if (conformsTo(t1, t2)) {
                                    return t2;
                                }
                            }
                        }
                    }
                    
                } else {                            // Attribute Access
                    // Not sure what do
                }
            }
            throw new TypeException("Unknown member access for " + member.value);
        }
        
        
        if (body instanceof MatchNode) {            // Match 
            currNode= (Node) body;
            children= currNode.getList();
            
            // Parse out all the case nodes
            Node head= (CasesListNode) children.get(1);
            LinkedList<CasesNode> cases= new LinkedList<CasesNode>();
            while (!(head instanceof CasesNode)) {
                cases.addFirst( (CasesNode) head.getList().get(1) );
                head= (Node) head.getList().get(0);
            }
            cases.addFirst( (CasesNode) head );
            
            // Evaluate all case expressions for their types,
            // accounting for the local declarations
            Stack<ClassObj> stack= new Stack<ClassObj>();
            for (CasesNode node : cases) {
                if (node.getList().size() == 4) {
                    String name= (String) ((Symbol) node.getList().get(1)).value;
                    String type= (String) ((Symbol) node.getList().get(2)).value;
                    //UNCOMMENT//stable.table.put(name, new VarAttribute(mtable.getClass(type), 0, node));
                    stack.push( eval( (Symbol) node.getList().get(3) ) );
                }
            }
            
            // Apply join to all stack pairs
            while (stack.size() > 1) {
                stack.push( join(stack.pop(), stack.pop()) );
            }
            
            return stack.pop();
            
        }
        
        
        // Block Node Types
        if (body instanceof BlockListNode) {
            currNode= (Node) body;
            children= currNode.getList();
            if (children.size() == 0) {         // Empty Block
                return Unit;
            }
            // ------------------------------>  // Block-One handled by Expr
            //                                  // Unsure how to handle BlockExpr
        }
        if (body instanceof BlockNode) {        // Block Local (Let Statement)
            currNode= (Node) body;
            children= currNode.getList();
            String type= (String) ((Symbol) children.get(0)).value;
            ClassObj t1= mtable.getClass(type);
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (conformsTo(t2, t1)) {
                return t1;
            }
            throw new TypeException("Assignment undefined from type " + t2.getName() + " to " + t1.getName());
        }
                
        
        // Primary Node Types
        if (body instanceof PrimaryNode) {
            currNode= (Node) body;
            children= currNode.getList();
            if (children.size() == 0) { return null; }  // Empty LPAREN / RPAREN pair
            
            Symbol s1= (Symbol) children.get(0);
            if (children.size() == 1) {
                return eval(s1);
            }
            
            if (s1.getId() == Terminals.NEW) {       // New 
                String type= ((Symbol) children.get(1)).value.toString();
                if (!type.equals("Any") && !type.equals("Integer")
                 && !type.equals("Unit") && !type.equals("Boolean")
                 && !type.equals("String")) {
                    return mtable.getClass(type);
                }
                throw new TypeException("Cannot instantiate class " + type);
            }
            
            if (s1.getId() == Terminals.SUPER) {    // Superclass Dispatch
                
            }
            
            if (s1.getId() == Terminals.ID) {       // Regular Dispatch
                
            }
        }
        
        // TODO Block-Expr   
        
        // TODO Attribute Definition
        // TODO Method Override Dispatch
        
        // Undefined Node Subtype
        return null;
    }
    
    // TODO Implement conformation checking as described in the COOL manual
    public boolean conformsTo(ClassObj c1, ClassObj c2) {
        if (c2.equals(Any)) {
            return true;
        }
        
        while (!c1.equals(Any)) {
            if (c1.equals(c2)) {
                return true;
            }
            c1= mtable.getClass( c1.getParentName() );
        }
        
        return false;
    }
    
    // TODO Implement join checking as described in the COOL Manual
    public ClassObj join(ClassObj c1, ClassObj c2) {
        return null;
    }
    
}
