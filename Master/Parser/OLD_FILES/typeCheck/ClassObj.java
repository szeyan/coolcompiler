package cool.typeCheck;

/**
 * This is a helper file that stores information about a class in the method
 * table
 *
 * @author Sze Yan Li, Kevin Garsjo
 */
import cool.ast.Node;
import java.util.LinkedHashMap;

public class ClassObj2 {

    private String programName;
    private String name;
    private String parent;
    private LinkedHashMap<String, MethodObj> methods;
    private LinkedHashMap<String, AttributeObj> attributes;
    private Node node;

    /**
     * constructor takes in the name of the class and the name of its super
     * class
     *
     * @param name name of class
     * @param parent name of super class
     * @param programName the name of the program in which this class belongs to
     * @param node the class node associated with this class
     */
    public ClassObj(String name, String parent, String programName, Node node) {
        this.programName = programName;
        this.name = name;
        this.parent = parent;
        this.node = node;
        attributes = new LinkedHashMap<String, AttributeObj>();
        methods = new LinkedHashMap<String, MethodObj>();
    }

    /**
     * Constructor for default types already implemented in cool,such as...
     * Integer
     * Boolean
     * String
     * Any
     * Nothing
     * Null
     *
     * @param name the name of the class
     */
    public ClassObj(String name) {
        this.name = name;
        this.programName = null;
        this.parent = null;
        this.node = null;
        methods = new LinkedHashMap<String, MethodObj>();

    }

    /**
     * @return the name of this class
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the name of the super class
     */
    public String getParentName() {
        return this.parent;
    }

    /**
     * @return the name of the program in which this class belongs to
     */
    public String getProgramName() {
        return this.programName;
    }

    /**
     * @return the node matching this class
     */
    public Node getNode() {
        return this.node;
    }

    /**
     * @return the constructor for this class
     */
    public MethodObj getConstructor() {
        return this.methods.get(this.getName());
    }

    /**
     * Looks for a method within this class
     *
     * @param methodName the method to look for
     * @return the MethodObj associated with the given method name
     */
    public MethodObj getMethod(String methodName) {
        return this.methods.get(methodName);
    }

    /**
     * @return the LinkedHashMap of methods for this class
     */
    public LinkedHashMap getMethods() {
        return this.methods;
    }

    /**
     * @return the LinkedHashMap of attributes for this class
     */
    public LinkedHashMap getAttributes() {
        return this.attributes;
    }

    /**
     * adds an attribute to the list of attributes contained in this class
     *
     * @param attributeName name of the method
     * @param ao MethodObj to store
     */
    public void addAttribute(String attributeName, AttributeObj ao) {
        this.attributes.put(attributeName, ao);
    }

    /**
     * finds an attribute belonging in this class
     *
     * @return the attribute object found, null if not found
     */
    public void addAttribute(String attributeName) {
        this.attributes.get(attributeName);
    }

    /**
     * adds a method to the list of methods contained in this class
     *
     * @param methodName name of the method
     * @param mo MethodObj to store
     */
    public void addMethod(String methodName, MethodObj mo) {
        this.methods.put(methodName, mo);
    }

    /**
     * compares this ClassObj and another ClassObj to make sure they are equal
     *
     * @param o the other ClassObj to compare to
     */
    public boolean equals(ClassObj o) {
        if (o == null) {
            return false;
        }

        boolean result = true;
        result = result && this.name.equals(o.name);

        //if(this.parent !=null){
        //result = result && this.parent.equals(o.parent);
        //}

        //result = result && this.node == o.node;
        //result = result && this.methods == o.methods;
        //result = result && this.programName.equals(o.programName);

        return result;
    }
}
