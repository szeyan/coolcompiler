package cool.typeCheck;

import java.util.List;
import java.util.Stack;

import cool.Terminals;
import cool.ast.*;
import cool.typeCheck.ClassMaster.KTreeTraverse;

import beaver.Symbol;


public class SymbolBuilder /*extends KTreeTraverse*/ {
    Symbol cur;
    Symbol prev;
    Stack<SymbolTable> stack = new Stack();
    SymbolTable curTable;
    public SymbolBuilder(Node root, M mtable){
        curTable = new SymbolTable(mtable);
        this.walk(root);
    }
    private String walk(Node curNode) {
        //init the first symbolTable
        Class c = curNode.getClass();
        if(c == ClassNode.class){
            this.classNode((ClassNode) curNode);
        }else if(c == VarformalsNode.class){
            this.formalVar((VarformalsNode) curNode);
        }else if(c == FeatureNode.class){
            this.feature((FeatureNode) curNode);
        }else if(c == BlockNode.class){
            return this.block((BlockNode) curNode);
        }else if(c == ExprNode.class){
            return this.expr((ExprNode) curNode);
        }else if(c == PrimaryNode.class){
            return this.primary((PrimaryNode) curNode);
        }else if(c == FormalsNode.class){
            return this.formals(curNode);
        }else if(c == MatchNode.class
            || c == LessEqualNode.class
            || c == LessThanNode.class
            || c == EqualNode.class
            || c == NotNode.class){
            this.boolType(curNode);
        }else if(c == TimesNode.class
            || c == DivideNode.class
            || c == UnaryMinusNode.class
            || c == PlusNode.class
            || c == MinusNode.class){
            this.intType(curNode);
        }else{
            return this.walkChildren(curNode);
        }
        return "";
        
    }
    
    public String walkChildren(Node node){
        for (int i = 0; i < node.getList().size(); i++) {
            Object child = node.getList().get(i);
            this.walk((Node) child);
        }
        return "";
    }
    //the icecream man would be proud 
    public void pushPopWalk(Node node){
        //push the old symboltable and continue with copy of it
        this.push();
        //continue recursing with this symbolTable
        this.walkChildren(node);
        //when we return pop the old symboltable and continue on
        this.pop();
    }
    public void classNode(ClassNode node){
        List<Node> list = node.getList();
        //push old table
        this.push();

        //think this is the type for class
        String type = list.get(0).value.toString();

        curTable.add("this",type,node);

        //continue recursing with this symbolTable
        this.walkChildren(node);
        //pop old table and use it
        this.pop();

    }
    public void formalVar(VarformalsNode node){
        List<Node> list = node.getList();
        String var = list.get(0).value.toString();
        //think this is the type for formal var
        String type = list.get(2).value.toString();
        curTable.add(var,"this",type,node);
    }
    public void feature(FeatureNode node){
        List<Node> list = node.getList();
        //if we have an id def
        if(Terminals.NAMES[list.get(0).getId()].equals("ID")){
            String var = list.get(0).value.toString();
            //this handles adding a entry into the symbol table
            this.varId(var,list.get(1));
        //if otherwise we have a function or a block 
        }else{
            this.pushPopWalk(node);
        }
        
    }
    private String formals(Node node) {
         List<Node> list = node.getList();
         String var = list.get(0).value.toString();
         String type = list.get(1).value.toString();
         curTable.add(var,type,node);
         return type;
    }
    private void varId(String name,Node node) {
        List<Node> list = node.getList();
        if(Terminals.NAMES[list.get(0).getId()].equals("NATIVE")){
            //TODO do i do anything here
            curTable.add(name,"native",list.get(0));
        }else{
            curTable.add(name,list.get(0).value.toString(),list.get(1));
        }
    }
    public String block(BlockNode node){
        List<Node> list = node.getList();
        //if we have a var as the first symbol then we have a id def
        String var = list.get(0).value.toString();
        String type = list.get(1).value.toString();
        Node expr = list.get(2);
        curTable.add(var,type,expr);
        return type;
    }
    public String expr(ExprNode node){
        // switch(Terminals.Names[node.getList(0).getId()]){
        //     case "ID":
        // }
        List<Node> list = node.getList();

        String var = list.get(0).value.toString();
        
        //here we need to check the other expr for its type
        String type = this.walk( list.get(1));

        curTable.add(var,type,node);
        return type;
    }
    public String primary(PrimaryNode node){
        List<Node> list = node.getList();

        if( list.size() >1 && Terminals.NAMES[list.get(0).getId()] == "ID"){
            //stuff with method call and actuals
            // String var = node.getList(2).value;
            // String type = node.getList(4).type;
            // curTable.add(var,type,node);
            return this.walk(list.get(1));
        }else if(Terminals.NAMES[list.get(1).getId()] == "ID"){
            //stuff with method call and actuals
            // String var = node.getList(2).value;
            // String type = node.getList(4).type;
            // curTable.add(var,type,node);
            return this.walk(list.get(2));
        }else if(Terminals.NAMES[list.get(0).getId()] == "NEW"){
            this.walk(list.get(2));
            return list.get(0).getType();
        }else{
            return list.get(0).getType();
        }
        //TODO other stuff?
    }
    private String intType(Node node) {
        this.walkChildren(node);
        return "INTEGER";
    }
    private String boolType(Node node) {
        this.walkChildren(node);
        return "BOOLEAN";
    }
    public String whileFunc(WhileNode node){
        List<Node> list = node.getList();
        return this.walk(list.get(1));
    }
    public String ifElse(Node node){
        List<Node> list = node.getList();
        return this.walk(list.get(1));
    }
    public void push(){
        //push the old table on the stack
        this.stack.push(curTable);
        //create a copy of the old table
        this.curTable = new SymbolTable(this.curTable);
    }
    public void pop(){
        this.curTable = this.stack.pop();
    }
}
