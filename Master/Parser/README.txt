# ################################
# A Cool Compiler
# CIS461
# ################################

AUTHORS
	Sze Yan Li, Noah Cooper, Liam Maier, Kevin Garsjo
	

THANKS/CREDITS
	Beaver - a LALR Parser Generator
		http://beaver.sourceforge.net/index.html
		
	JFlex - a Scanner Generator for Java
		http://jflex.de/
	
	Apache Commons - a library for String manipulation methods, etc
		http://commons.apache.org/proper/commons-lang/
	
	Nick Chaimov - Freaking great resource for everything Type Checker

RELEVANT FILEPATHS/FOLDERS
	/etc 		- contains .flex, .grammar, and tar-triple.conf file
	/lib		- jar libraries used
	/native		- any Basic.cool class that's implemented in C
	/src		- java source files
	/test		- test files
	
BUILD/RUN
	
	BUILD:
	'make'		- To build the whole project
	'make jflex'	- To build only the JFlex Lexer
	'make beaver'	- To build only the Beaver Parser
	'make java' 	- To compile all java source files
	'make clean'	- To remove all make-created files

	RUN:
	1) Ensure etc/target_triple.conf consists of just your target triple string
	2) Invoke ./run.sh <file1.cool> [<file2.cool> ...]

	RUN NOTE:
		run.sh expects to be able to use LLVM commands from the path variable (i.e. `llc` or `clang`).
		If these don't work, you'll have to manually set the variables to point to the appropriate binaries.


TEST FILES AND PURPOSES
	Inside test/
	
	t1_helloCool.cool	# Demonstrates String alloc and IO out
	t2_echo.cool		# Demonstrates String concat, dynamic method dispatch, and IO in
	t3_ints.cool		# Demonstrates Int alloc, Int toString
	t4_arithmetic.cool	# Demonstrates all arithmetic operators with all permutations of Int literal versus Int identifier


PROGRESS AND COMMENTS
=== Pros ===
	We were able to allow the dynamic allocation of Strings, IO objects, and Ints.
	We have a successful IO implementation, complete with inputs and outputs.
	IO, String, and Int native methods work fully and as expected (except comparisons).
	Dispatch to those methods works accordingly, following a dynamic dispatch model.
	Int arithmetic is fully-functional; plus, div, minus, mult all work as expected with either literals or identifiers.
	We have skeleton LLVM-like generation code and generation structures not yet linked to the program, such as `while` or `if`.
		^ Check CodeGenerator.java or GenerateSampleLLVM.java for examples.
=== Cons ===
	Complex statements in almost any form (i.e. x.y().z(); a + b / c - 23 * e;) do not work. 
		This can be circumvented by breaking complex statements into single-variable chunks.
	Non-native methods and user-defined methods currently return only pointers to allocated return objects.
	Lots of high-level syntactic sugar left unimplemented.
