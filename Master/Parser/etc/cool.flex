package cool;

import beaver.Symbol;
import beaver.Scanner;

//Terminals.java is located same directory

/**
 * lexer for COOL
 * CIS461
 * Sze Yan Li
 */
%%

%class CoolScanner
%extends Scanner
%unicode
%function nextToken
%type Symbol
%yylexthrow Scanner.Exception
%eofval{
	return token(Terminals.EOF, "end-of-file");
%eofval}
%line
%column

%{
	//saves the string when strings are found
	private StringBuffer string = new StringBuffer(128);
			
	//save the starting line and col. used for states
	private int line = 0;
	private int col = 0;
	
	//Create an ErrReport
	ErrReport err = new ErrReport();
	
	//returns the ErrReport
	public ErrReport getErrReport(){
		return this.err;
	}
	
	//Create a Symbol with just the ID
	private Symbol token(short id){
		return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
	}

	//Create a Symbol with the ID and obj value
	private Symbol token(short id, Object value){
		return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), value);
	}
	
	//Create a Symbol with id, obj value, and custom line and column
	private Symbol token(short id,int line,int col, Object value){
		return new Symbol(id, this.line, this.col, yylength(), value);
	}
%}

/* spaces */
InputCharacter 	= [^\r\n]
WhiteSpace 		= {LineTerminator} | [ \t\f]
LineTerminator 	= \r|\n|\r\n

/* comments */
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?


/* input */
AnyChar    	= . | \n

/* string literals */
StringCharacter = [^\r\n\"\\]

/* numeric literals */
Integer = 0 | [1-9][0-9]*

/* identifiers */
ObjectIdentifier  	= [a-z] ([A-Za-z0-9] | "_")*
TypeIdentifier 		= [A-Z] ([A-Za-z0-9] | "_")*

/* reserved -- illegal keywords */
IllegalWords = "abstract"|"catch"|"do"|"final"|"finally"|"for"|"forSome"|"implicit"|"import"|"lazy"|"object"|"package"|"private"|"protected"|"requires"|"return"|"sealed"|"throw"|"trait"|"try"|"type"|"val"|"with"|"yield"

/* states */
%state STRING_SINGLE, STRING_TRIPLE, COMMENTS
 
%%
<YYINITIAL> {	
	/* comments */
	{EndOfLineComment}      { /* ignore */ }
	"/*"              		{ line = yyline +1;
							  col = yycolumn + 1;
							  yybegin(COMMENTS); }

	/* whitespace */
	{WhiteSpace}    { /* ignore */ }

	/* keywords */
	"case"          { return token(Terminals.CASE); }
	"class"      	{ return token(Terminals.CLASS); }
	"def"   		{ return token(Terminals.DEF); }	
	"else"          { return token(Terminals.ELSE); }	
	"extends"      	{ return token(Terminals.EXTENDS); }
	"if"          	{ return token(Terminals.IF); }
	"match"         { return token(Terminals.MATCH); }
	"native"        { return token(Terminals.NATIVE); }
	"new"        	{ return token(Terminals.NEW); }	
	"override"      { return token(Terminals.OVERRIDE); }
	"super"         { return token(Terminals.SUPER); }
	"this"          { return token(Terminals.THIS); }	
	"var"       	{ return token(Terminals.VAR); }
	"while"       	{ return token(Terminals.WHILE); }
		
	/* separators */
	"("             { return token(Terminals.LPAREN); }
	")"             { return token(Terminals.RPAREN); }	
	"{"             { return token(Terminals.LBRACE); }
	"}"             { return token(Terminals.RBRACE); }	
	":"             { return token(Terminals.COLON); }
	","             { return token(Terminals.COMMA); }	
	"="             { return token(Terminals.ASSIGN); }
	";"             { return token(Terminals.SEMI); }	
	"=>"            { return token(Terminals.ARROW); }
	"."             { return token(Terminals.DOT); }		
	
	/* operators */	
	"+"             { return token(Terminals.PLUS); }	
	"-"             { return token(Terminals.MINUS); }	
	"*"             { return token(Terminals.TIMES); }	
	"/"             { return token(Terminals.DIV); }	
	
	"<="            { return token(Terminals.LE); }	
	"<"             { return token(Terminals.LT); }	
	"=="            { return token(Terminals.EQUALS); }	
	"!"             { return token(Terminals.NOT); }	
	
	/* boolean literals */
	"true"		   	{ return token(Terminals.BOOLEAN, new Boolean(true)); }
	"false"	    	{ return token(Terminals.BOOLEAN,  new Boolean(false)); }
	
	 /* null literal */
	"null"          { return token(Terminals.NULL); }
	
	/* string literals */
	\"\"\"			{ line = yyline +1;
					  col = yycolumn + 1;
					  yybegin(STRING_TRIPLE); string.setLength(0); }
					  
	\"              { line = yyline +1;
					  col = yycolumn + 1;
					  yybegin(STRING_SINGLE); string.setLength(0); }
		
	/*numeric literals */
	{Integer}    	{ return token(Terminals.INTEGER, new Integer(yytext())); }
			
	/* illegal keywords */
	{IllegalWords}  { err.send("Illegal keyword \""+yytext()+"\"", (yyline + 1), (yycolumn + 1)); }
	
	/* identifiers */ 
	{TypeIdentifier}    	{ return token(Terminals.TYPE, yytext()); }
	{ObjectIdentifier}    	{ return token(Terminals.ID, yytext()); }
	
}

<COMMENTS> {
  "*"+"/"        				 {yybegin(YYINITIAL);}

  {LineTerminator} 			  	 { /* ignore */ }
   
  [^*\n]*        				 { /* eat anything that's not a '*' */ } 
  "*"+[^*/\n]*   			 	 { /* eat up '*'s not followed by '/'s */ }
 
  <<EOF>>                        { err.send("Unterminated comment", line, col); 
								   return token(Terminals.EOF, "end-of-file"); }

}



<STRING_SINGLE> {
  \"                             { yybegin(YYINITIAL); 
                                   return token(Terminals.STRING, line, col,
                                   string.toString()); }
								   
  {StringCharacter}+             { string.append(yytext() ); }

  /* escape sequences */
  "\\0"                          { string.append('\0'); }  
  "\\b"                          { string.append( '\b' ); }
  "\\t"                          { string.append( '\t' ); }
  "\\n"                          { string.append( '\n' ); }
  "\\f"                          { string.append( '\f' ); }
  "\\r"                          { string.append( '\r' ); }
  "\\\""                         { string.append( '\"' ); }
  "\\\\"                         { string.append( '\\' ); }
  
  /* error cases */
  \\.                            { yybegin(YYINITIAL); //end state and continue processing but report the error's exact location
								   err.send("Illegal escape sequence \""+yytext()+"\"", (yyline + 1), (yycolumn + 1));
								   return token(Terminals.STRING, line, col, string.toString()); }
  
  {LineTerminator}               { yybegin(YYINITIAL); //end state and continue processing but report the start of the error
								   err.send("Unterminated string at end of line \""+string.toString()+"\"", line, col);
								   return token(Terminals.STRING, line, col, string.toString()); }
  
  <<EOF>>                        { yybegin(YYINITIAL); //end state and continue processing but report the start of the error
								   err.send("EOF encountered before end of string \""+string.toString()+"\"", line, col); 
								   return token(Terminals.EOF, "end-of-file"); }
}

<STRING_TRIPLE> {
  \"\"\"                         { yybegin(YYINITIAL); 
                                   return token(Terminals.STRING, 
                                   string.toString()); }
								   
  {AnyChar}             		 { string.append(yytext() ); }
			
  /* error case(s) */
  <<EOF>>                        { yybegin(YYINITIAL); //end state and continue processing but report the start of the error
								   err.send("EOF encountered before end of string \""+string.toString()+"\"", line, col); 
								   return token(Terminals.EOF, "end-of-file"); }
}


/* error fallback */
{AnyChar}           			{ err.send("Unrecognized character '" + yytext() + "'", (yyline + 1), (yycolumn + 1)); }
























