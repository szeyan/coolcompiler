package cool;

import beaver.Symbol;
import cool.typeCheck.VariableObj;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Controls and keeps track of all ErrReport objects and all errors found while compiling a COOL
 *
 * @author Sze Yan Li
 */
public class ErrorMaster {

    private LinkedList<ErrReport> erList;
    private ErrReport currER;
    public int TotalErrorCount;
    public int TotalScannorErrors;
    private boolean updated;
    public LinkedHashMap<String, String> classtofile;

    /**
     * Constructor
     */
    public ErrorMaster() {
        this.erList = new LinkedList<ErrReport>();
        this.currER = new ErrReport();
        this.TotalErrorCount = 0;
        this.TotalScannorErrors = 0;
        this.updated = true;
        this.classtofile = new LinkedHashMap<String, String>();
    }

    /**
     * Set the current ErrReport object
     */
    public void setCurrER(ErrReport er) {
        this.erList.add(er);
        this.currER = er;
        this.updated = false;
    }

    /**
     * Gets total error count
     */
    public int getErrorCount() {
        if (!updated) {
            this.TotalErrorCount = this.TotalErrorCount - this.TotalScannorErrors;

            this.TotalScannorErrors = 0;
            //since an ErrReport was added, must obtain current count of errors
            for (int i = 0; i < erList.size(); i++) {
                int count = erList.get(i).ErrCount;
                this.TotalScannorErrors += count;
            }

            this.updated = true;

            this.TotalErrorCount += this.TotalScannorErrors;
        }

        return this.TotalErrorCount;
    }

    /**
     * @param className key to map
     * @return file path associated with class if found, null if not found
     */
    public String getFilePath(String className) {
        return this.classtofile.get(className);
    }

    /**
     * adds filename and class pair into hashmap
     */
    public void addC2F(String className, String fileName) {
        if (!this.classtofile.containsKey(className)) {
            this.classtofile.put(className, fileName);
        }

    }

    //send an error
    public void send(String msg, String filePath, String className, VariableObj x, VariableObj y) {
        this.TotalErrorCount++;
        String s = "";

        if (!filePath.isEmpty()) {
            s += "Error in file \"" + filePath + "\"";
        } else if (this.classtofile.get(className) != null) {
            s += "Error in file \"" + this.classtofile.get(className) + "\"";
        } else {
            s += "Error";
        }

        if (!className.isEmpty()) {
            s += " in class \"" + className + "\"";
        }

        if (y.node != null) {
            s += " (" + this.getLine(y.node) + ", " + this.getCol(y.node) + "): ";
        } else {
            s += ": ";
        }

        s += msg;

        if (y.node != null && x.node != null) {
            s += " of " + x.toString() + " for " + y.toString();
        }

        System.err.println(s);
    }

    //send an error
    public void send(String msg, String filePath, String className, Symbol sym) {
        this.TotalErrorCount++;
        String s = "";

        if (!filePath.isEmpty()) {
            s += "Error in file \"" + filePath + "\"";
        } else if (this.classtofile.get(className) != null) {
            s += "Error in file \"" + this.classtofile.get(className) + "\"";
        } else {
            s += "Error";
        }

        if (!className.isEmpty()) {
            s += " in class \"" + className + "\"";
        }

        if (sym != null) {
            s += " (" + this.getLine(sym) + ", " + this.getCol(sym) + "): ";
        } else {
            s += ": ";
        }

        s += msg;

        System.err.println(s);
    }

    public void send(String msg, String className, Symbol sym) {
        this.send(msg, "", className, sym);
    }

    /**
     * gets the starting line of a symbol
     */
    private int getLine(Symbol s) {

        return (s.getLine(s.getStart()));
    }

    /**
     * gets the starting column of a symbol
     */
    private int getCol(Symbol s) {
        return (s.getColumn(s.getStart()));
    }
}
