/*
 * Utility to get the llvm data types
 * corresponding to Cool basic classes
 *
 * @author Noah Cooper
 */

package cool.codegen;

import java.util.ArrayList;
import java.util.AbstractList;

public class LLTypes {

	private AbstractList<String> basicClasses = new ArrayList<String>();
    // We don't want to generate definitions for thsese classes,
    // because we're emitting their code in C.
    public LLTypes() {
        basicClasses.add("Any");
        basicClasses.add("Int");
        basicClasses.add("Unit");
        basicClasses.add("String");
        basicClasses.add("Boolean");
        basicClasses.add("ArrayAny");
        basicClasses.add("IO");
        basicClasses.add("Symbol");
    }

	public boolean isBasicClass(String coolType) {
		return basicClasses.contains(coolType);
	}

	public String getLLType(String coolType) {
		return "%struct.obj_" + coolType + "*";
	}
}