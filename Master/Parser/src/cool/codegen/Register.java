/*
 * Helper class for code generator
 * @author Sze Yan Li
 */
package cool.codegen;

import beaver.Symbol;

public class Register {

    //pre-defined labels
    public String tmpHeader = "%tmp_";
    public String varHeader = "%var_";
    public String formalHeader = "%formal_";
    public String attribHeader = "%attrib_";
    public String paramHeader = "%param_";  
    //register information
    public String header = "";
    public String name = "";
    public Symbol type = null;
    public String typeStr= null;
    public String value = "";
    public String jump = "; <label>:";
    //extraneous information
    public int size = 32; // ?
    public int align = 4; // ?
    public int itemNumber = -1;
    
    public Register(){
        this("");
    }
    
    public Register(String name){
        this.name = name;
    }

    public String getName() {
        return header + name;
    }

    public String getLabel() {
        return jump + header.substring(1,header.length()) + name;
    }

    public String getObjName() {
        return emitType() + " " + header + name;
    }

    public String getPtr() {
        return emitType() + "* " + header + name;
    }
    
    public String getBaseType() {
        if (type != null) {
            return type.value.toString();
        }
        if (typeStr != null) {
            return typeStr;
        }
        return "Nothing";
    }

    public String emitType() {
        if (type != null) {
            String type = this.type.value.toString();
            return "%struct.obj_" + type + "*";
        }
        
        if (typeStr != null) {
            return "%struct.obj_" + typeStr + "*";
        }
        
        return "i32";
    }

    public String toString() {
        return getName();
    }
}
