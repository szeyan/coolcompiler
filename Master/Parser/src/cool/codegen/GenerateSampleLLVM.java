/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Sample LLVM Code Generator
 * @author Sze Yan Li, (Kevin)
 * CIS461
 * 
 * The following IS NOT correct llvm format.  
 * Though it is structurally correct.
 * 
 * This file purely gives a sample of how to walk the tree.  
 * Everything is written to system.err. when it is finished.
 * 
 * Testfile: t5.cool
 * SampleOutput of all cleared cases: tests/t5output/block*.text 
 * 
 * Notes:
 * CodeGenerator.java is what contains the actual cool->llvm code generation.  
 * It contains a simple dispatch which is not implemented in here (along with many other things).
 * 
 * Cases cleared:
 * - block 0
 * - block 1
 * - block 2
 * - block 3
 * - block 4
 * - block 5
 * - block 6
 *
 * Handles:
 * - Ints
 * - Booleans
 * - Created a lookup for registers
 * - Strings (implemented by Kevin)
 * - New (implemented by Kevin)
 *
 * Includes:
 * - int declaration
 * - boolean declaration
 * - variable assignment
 * - complex arithmetic
 * - unary minus
 * - basic if statements (not ,<,<=,==)
 * - while loops
 *
 * To-do:
 * - Strings
 * - new
 * - super
 * - cases
 * - scoping?
 * - chain dispatch
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.codegen;

import beaver.Symbol;
import cool.ErrorMaster;
import cool.Terminals;
import java.io.*;
import cool.ast.*;
import cool.typeCheck.*;
import java.util.*;

public class GenerateSampleLLVM {
    
    //imports
    private M m;
    private MasterTable masterTable;
    private ErrorMaster errorMaster;
    //class variables
    private int regCount = 0;
    private String ret = "";
    private String currClass = "";
    private String i32 = "i32";
    private LinkedHashMap<String, Register> memory = new LinkedHashMap<String, Register>();
    private LinkedHashMap<String, String> strLiterals = new LinkedHashMap<String, String>();
    
    //**
    //**-- CURRENTLY TESTING
    private int testingBlockNum = 6;

    public GenerateSampleLLVM(M m, MasterTable masterTable, ErrorMaster errorMaster) {
        this.m = m;
        this.masterTable = masterTable;
        this.errorMaster = errorMaster;

        this.generate();
    }

    public void generate() {
        ClassObj co = m.getClass("Main");
        Node coNode = co.getNode();
        LinkedList<Node> executionBlocks = co.getExecBlocks();

        //check example block ;
        Node block = (Node) executionBlocks.get(testingBlockNum);

        //walk and flatten the block
        block = new TreeWalker().flatten(block);
        System.err.println(new TreeWalker().toStringFlat(block));

        //emit code from this block
        this.currClass = co.getName();
        emitBody((Symbol) block, null);
        System.err.println("--Example--");
        System.err.println(this.ret);

        System.err.println("Registers in use{");
        Iterator<Register> rs = memory.values().iterator();
        while (rs.hasNext()) {
            System.err.println("\t" + rs.next());
        }
        System.err.println("}");

    }
    /*
     * look up for register
     *
     * Note: linkedhashmap overwrites when adding, so the most recent instance of a register name will be retrieved...
     *
     */

    private Register lookup(String name, String header) {
        Register reg = null;

        Collection<Register> registers = memory.values();
        Iterator<Register> itr = registers.iterator();
        while (itr.hasNext()) {
            Register r = itr.next();
            if (r.header.equals(header) && r.name.equals(name)) {
                return r;
            }
        }

        return reg;
    }

    //emit example: %tmp_3 =
    private Register newTmp() {
        Register reg = new Register();
        reg.header = reg.tmpHeader;
        reg.name = Integer.toString(this.regCount);

        this.regCount++;

        return reg;
    }

    private String assign(Register reg) {
        String ret = "";
        ret += reg.getName();
        ret += " =";
        return ret;
    }

    //emit example: %var_a = alloca i32, align 4
    private String alloca(Register reg) {
        String ret = "";

        ret += "alloca ";       //allocate memory on heap
        ret += reg.emitType();   //for type
        ret += ", ";
        ret += "align ";
        ret += reg.align;       //with alignment of 'align' bytes

        return ret;
    }

    //emit example: store i32 10, i32* %var_a, align 4
    private String storeInt(Register storeTo, String value) {
        String ret = "";

        ret += "store ";
        ret += i32; //D:?
        ret += " ";
        ret += value;
        ret += ", ";
        ret += storeTo.getPtr();
        ret += ", ";
        ret += "align ";
        ret += storeTo.align;

        return ret;
    }

    private String storeInt(Register storeTo, Register from) {
        String ret = "";

        ret += "store ";
        ret += from.getObjName();
        ret += ", ";
        ret += storeTo.getPtr();
        ret += ", ";
        ret += "align ";
        ret += storeTo.align;

        return ret;
    }

    //emit example: %tmp_2 = load i32* %a, align 4
    private String loadInt(Register reg) {
        String ret = "";

        ret += "load ";
        ret += reg.getPtr();
        ret += ", ";
        ret += "align ";
        ret += reg.align;

        return ret;
    }

    //emit example:  add nsw i32 %tmp_2, 2
    private String binOp(String op, Register r1, Register r2) {
        String ret = "";

        ret += op;
        ret += " ";
        ret += i32; //D:?
        ret += " ";
        ret += r1.getName();
        ret += ", ";
        ret += r2.getName();

        return ret;
    }

    //emit example:   br i1 %3, label %4, label %7
    private String branch(Register base, Register branchTrue, Register branchFalse) {
        String ret = "";

        ret += "br il";
        ret += " ";
        ret += base.getName();
        ret += ", ";
        ret += "label";
        ret += " ";
        ret += branchTrue.getName();
        ret += ", ";
        ret += "label";
        ret += " ";
        ret += branchFalse.getName();

        return ret;
    }

    //emit example:   br label <dest> ; Unconditional branch
    private String branchUncond(Register jumpTo) {
        String ret = "";

        ret += "br label";
        ret += " ";
        ret += jumpTo.getName();

        return ret;
    }

    private void writeC(String s) {
        ret += s + " \n";
    }

    private void writeI(String s) {
        ret += s + " ";
    }

    /*
     * Melody: sample on how to walk a block
     * Blocks can be found as just static execution blocks or found as the body of a method
     *
     * Note: uses .equals() instead of instanceof because instance of accepts all superclasses as an instance,
     * thus causing multiple walks (?)
     *
     * Status: may not contain all walks
     *
     * @param block is the current symbol or node we're evaluating
     * @param className the class the block belongs to
     *
     * @return void
     */
    private void emitBody(Symbol body, Register currReg) {
        int terminal = body.getId();
        LinkedList<Symbol> children = null;
        Node currNode = null;

        //-- emit only if not null
        if (body == null) {
            return;
        }


        //-- main body (BlockListNode)
        if (body.toString().equals("BLOCKLIST")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //print start of a block
            writeC("; -**- START BLOCK -**-");

            //emit code for each child
            for (int i = 0; i < children.size(); i++) {
                emitBody(children.get(i), currReg);
            }

            //print end of a block
            writeC("\n; -**- END BLOCK -**-");
        }

        //-- var def (BlockNode)
        if (body.toString().equals("BLOCK")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //write header
            writeC("\n; //-- WRITE variable declarion");

            //make a new register
            Register reg = new Register();
            reg.header = reg.varHeader;

            //first child = identifier
            reg.name = children.get(0).value.toString();

            //second child = type
            reg.type = children.get(1);

            //-- write allocation
            writeI(assign(reg));
            writeC(alloca(reg));
            
            //store the register into memory of all registers (including temps)
            memory.put(reg.name, reg);

            //third child = evaluation / definition
            emitBody(children.get(2), reg);

            //-- write store
            //To-do: further checking of this so called "value" is needed
            writeC(storeInt(reg, reg.value));       

            //store the register into the var attribute associated with it
            VariableObj va = masterTable.get(reg.name, currNode);
            va.setRegister(reg);
            reg.itemNumber = va.itemNumber; //associate the register to the varattribute in mastertable
        }

        //-- primaries (PrimaryNode)
        //includes: integer, string, new, super, etc
        if (body.toString().equals("PRIMARY")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //check if terminal: INTEGER/STRING
            Symbol s1 = (Symbol) children.get(0);
            emitBody(s1, currReg);
            
            //-- NEW case          
            if (s1.value.toString().equals("new")) {
                writeC("\n //- Write NEW");
                String type = children.get(1).value.toString();
                Register reg = newTmp();
                memory.put(reg.name, reg);

                //Write to err
                writeI(assign(reg));
                writeC("call %struct.obj_" + type + "* @" + type + "_constructor()");
                writeC("store %struct.obj_" + type + "* " + reg.getName() + ", %struct.obj_" + type + "** " + currReg.getName() + ", align 4");

            }

            //To-do: check if SUPER

            //Second child for Dot, etc
            if (children.size() == 2) {
                Symbol s2 = (Symbol) children.get(1);
                emitBody(s2, currReg);

            }
        }

        //-- binary expressions (ExprNode)
        if (body.toString().equals("EXPR")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //first child = identifier
            String id = children.get(0).value.toString();

            //lookup and retrieve register from masterTable
            Register reg = new Register();
            reg = lookup(id, reg.varHeader);
            memory.put(reg.name, reg);

            //second child = assignment
            Symbol rExpr = children.get(1);

            writeC("\n; //-- WRITE variable assignment");

            //check if direct assignment
            if (rExpr.toString().equals("PRIMARY")) {

                Register r1 = newTmp();
                memory.put(r1.name, r1);
                writeI(assign(r1));
                emitBody(rExpr, r1);

                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }

                //store into variable reg from assignment value r1
                writeC(storeInt(reg, r1));

            } else {
                //evaluate the assignment
                emitBody(rExpr, reg);
            }
        }

        //-- plus (PlusNode)
        //-- minus (MinusNode)
        //-- times (TimesNode)
        //-- divide (DivideNode)
        if (body.toString().equals("PLUS")
                || body.toString().equals("MINUS")
                || body.toString().equals("TIMES")
                || body.toString().equals("DIVIDE")) {
            //no need to flatten node (recursively checks)
            currNode = (Node) body;
            //grab children
            children = currNode.getList();

            //get operation
            String op = "";
            if (body.toString().equals("PLUS")) {
                op = "add";
            } else if (body.toString().equals("MINUS")) {
                op = "sub";
            } else if (body.toString().equals("TIMES")) {
                op = "mul";
            } else if (body.toString().equals("DIVIDE")) {
                op = "sdiv";
            }

            //write header
            writeC("; -- start " + op + " instruction");

            //first child = first item
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1);
            }


            //second child = second item
            Symbol c2 = children.get(1);
            Register r2 = newTmp();
            memory.put(r2.name, r2);
            if (c2.toString().equals("PRIMARY")) {
                writeI(assign(r2));
                emitBody(c2, r2);
                if (!r2.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r2));
                    writeC(storeInt(r2, r2.value));
                }
            } else {
                writeI(assign(r2));
                writeC(alloca(r2));
                emitBody(c2, r2);
            }


            //operate on first and second child
            Register r3 = newTmp();
            memory.put(r3.name, r3);
            writeI(assign(r3)); //either load register or allocate and store a constant
            writeC(binOp(op, r1, r2));

            //store into original
            writeC(storeInt(currReg, r3));

            //write new line
            writeC("; -- end " + op + " instruction");


        }

        //-- unary minus (UnaryMinusNode)
        if (body.toString().equals("UNARYMINUS")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("; -- start unary minus ");

            //first child -- integer expr to negate
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1);
            }

            //write zero register
            Register valueOfZero = newTmp();
            memory.put(valueOfZero.name, valueOfZero);
            writeI(assign(valueOfZero));
            writeC(alloca(valueOfZero));
            writeC(storeInt(valueOfZero, "0"));

            //subtract 0 - first child to get negation
            Register r3 = newTmp();
            memory.put(r3.name, r3);
            writeI(assign(r3)); //either load register or allocate and store a constant
            writeC(binOp("sub", valueOfZero, r1));

            //store into original
            writeC(storeInt(currReg, r3));

            writeC("; -- end unary minus ");
        }

        //-- if else (IfElseNode)
        if (body.toString().equals("IFELSE")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("\n; //-- WRITE if-else statement");

            writeC("; -- start boolean evaluation");
            //first child = (boolean expression to evaluate)
            Symbol s1 = (Symbol) children.get(0);

            //write result register
            Register result = newTmp();
            memory.put(result.name, result);

            //check boolean expr
            if (s1.toString().equals("PRIMARY")) {
                //either true or false or load a single variable
                Register t1 = newTmp();
                memory.put(t1.name, t1);
                writeI(assign(t1));
                emitBody(s1, t1);

                if (!t1.value.isEmpty()) {
                    //allocate true or false
                    writeC(alloca(t1));
                    writeC(storeInt(t1, t1.value));
                }

                //write compareTo register
                Register valueOfOne = newTmp();
                memory.put(valueOfOne.name, valueOfOne);
                writeI(assign(valueOfOne));
                writeC(alloca(valueOfOne));
                writeC(storeInt(valueOfOne, "1"));

                //write comparison instruction
                writeI(assign(result));
                writeC(binOp("icmp eq", t1, valueOfOne)); //is t1 == true?
            } else {
                //left side and right side of bool expr must be computed
                emitBody(s1, result); //evaluate <, <=, or ==
            }
            writeC("; -- end boolean evaluation");

            //write branches
            Register branchTrue = newTmp();
            memory.put(branchTrue.name, branchTrue);

            Register branchFalse = newTmp();
            memory.put(branchFalse.name, branchFalse);

            Register branchEnd = newTmp();
            memory.put(branchEnd.name, branchEnd);

            //write where to branch
            writeC(branch(result, branchTrue, branchFalse));

            //second child = true case
            writeC("\n" + branchTrue.getLabel() + "\t\t\t\t; True Branch");
            Symbol s2 = (Symbol) children.get(1);
            emitBody(s2, currReg);
            writeC(branchUncond(branchEnd));

            //third child = false case
            writeC("\n" + branchFalse.getLabel() + "\t\t\t\t; False Branch");
            Symbol s3 = (Symbol) children.get(2);
            emitBody(s3, currReg);
            writeC(branchUncond(branchEnd));

            //get out of the branch
            writeC("; //-- END if-else statement");
            writeC("\n" + branchEnd.getLabel() + "\t\t\t\t; Exit If-Else");

            //To-do: if else if else, multiple operands, and
        }

        //-- less than (LessThanNode)
        //-- less than or equal (LessEqualNode)
        //-- equal  (EqualNode)
        if (body.toString().equals("LESSTHAN")
                || body.toString().equals("LESSEQUAL")
                || body.toString().equals("EQUAL")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //first child - left side
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1);
            }


            //second child - right side
            Symbol c2 = children.get(1);
            Register r2 = newTmp();
            memory.put(r2.name, r2);
            if (c2.toString().equals("PRIMARY")) {
                writeI(assign(r2));
                emitBody(c2, r2);
                if (!r2.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r2));
                    writeC(storeInt(r2, r2.value));
                }
            }

            //write comparison instruction
            writeI(assign(currReg));

            if (body.toString().equals("LESSTHAN")) {
                writeC(binOp("icmp slt", r1, r2));
            } else if (body.toString().equals("LESSEQUAL")) {
                writeC(binOp("icmp sle", r1, r2));
            } else if (body.toString().equals("EQUAL")) {
                writeC(binOp("icmp eq", r1, r2));
            }
        }

        //-- not (NotNode)
        if (body.toString().equals("NOT")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("; -- start boolean negation ");

            //first child - the boolean expr to negate
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1);
            }


            //write compareTo register
            Register valueOfOne = newTmp();
            memory.put(valueOfOne.name, valueOfOne);
            writeI(assign(valueOfOne));
            writeC(alloca(valueOfOne));
            writeC(storeInt(valueOfOne, "1"));

            //write comparison instruction
            writeI(assign(currReg));
            writeC(binOp("icmp ne", r1, valueOfOne)); //is t1 != true?

            writeC("; -- end boolean negation ");
        }

        //-- while (WhileNode)
        if (body.toString().equals("WHILE")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            writeC("\n; //-- WRITE while loop");

            //write branches
            Register branchStart = newTmp();
            memory.put(branchStart.name, branchStart);

            Register branchBody = newTmp();
            memory.put(branchBody.name, branchBody);

            Register branchEnd = newTmp();
            memory.put(branchEnd.name, branchEnd);

            //jump to loop start
            writeC(branchUncond(branchStart));

            //first child - boolean expr to evaluate
            writeC("\n" + branchStart.getLabel() + "\t\t\t\t; Loop Check Branch");
            Symbol s1 = (Symbol) children.get(0);

            //write result register
            Register result = newTmp();
            memory.put(result.name, result);

            //check boolean expr
            if (s1.toString().equals("PRIMARY")) {
                //either true or false or load a single variable
                Register t1 = newTmp();
                memory.put(t1.name, t1);
                writeI(assign(t1));
                emitBody(s1, t1);

                if (!t1.value.isEmpty()) {
                    //allocate true or false
                    writeC(alloca(t1));
                    writeC(storeInt(t1, t1.value));
                }

                //write compareTo register
                Register valueOfOne = newTmp();
                memory.put(valueOfOne.name, valueOfOne);
                writeI(assign(valueOfOne));
                writeC(alloca(valueOfOne));
                writeC(storeInt(valueOfOne, "1"));

                //write comparison instruction
                writeI(assign(result));
                writeC(binOp("icmp eq", t1, valueOfOne)); //is t1 == true?
            } else {
                //left side and right side of bool expr must be computed
                emitBody(s1, result); //evaluate <, <=, or ==
            }

            //write where to branch
            writeC(branch(result, branchBody, branchEnd));

            //second child - body of the loop
            writeC("\n" + branchBody.getLabel() + "\t\t\t\t; Loop Body Branch");
            Symbol s2 = (Symbol) children.get(1);
            emitBody(s2, currReg);
            writeC(branchUncond(branchStart)); //jump back to loop start

            //write ending loop label
            writeC("\n" + branchEnd.getLabel() + "\t\t\t\t;  Exit Loop Branch");
        }

        //case

        //-- dot (DotNode)
        if (body.toString().equals("DOT")) {
            //don't flatten nonlists
            currNode = (Node) body;
            //grab children
            children = currNode.getList();

            writeC("\n; //-- Start Dot expression");

            //first child = id 
            Symbol c1 = children.get(0);
            Register r1 = newTmp();
            memory.put(r1.name, r1);
            if (c1.toString().equals("PRIMARY")) {
                writeI(assign(r1));
                emitBody(c1, r1);
                if (!r1.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r1));
                    writeC(storeInt(r1, r1.value));
                }
            } else {
                writeI(assign(r1));
                writeC(alloca(r1));
                emitBody(c1, r1);
            }

            //second child = (after the dot) eval (to be continued in primary node)
            Symbol c2 = children.get(1);
            Register r2 = newTmp();
            memory.put(r2.name, r2);
            if (c2.toString().equals("PRIMARY")) {
                writeI(assign(r2));
                emitBody(c2, r2);
                if (!r2.value.isEmpty()) {
                    //allocate a constant
                    writeC(alloca(r2));
                    writeC(storeInt(r2, r2.value));
                }
            } else {
                writeI(assign(r2));
                writeC(alloca(r2));
                emitBody(c2, r2);
            }
        }

        //-- main body (ActualsListNode)
        if (body.toString().equals("ACTUALSLIST")) {
            //flatten node before getting its children
            currNode = new TreeWalker().flatten((Node) body);
            //grab children
            children = currNode.getFlatList();

            //print start of a block
            writeC("\n; //--  Start actuals list");

            //emit code for each child
            for (int i = 0; i < children.size(); i++) {
                Symbol c1 = children.get(i);

                if (c1.toString().equals("PRIMARY")) {
                    //get single arguments                 
                    Register r1 = newTmp();
                    memory.put(r1.name, r1);
                    writeI(assign(r1));
                    emitBody(c1, r1);
                    if (!r1.value.isEmpty()) {
                        //allocate a constant
                        writeC(alloca(r1));
                        writeC(storeInt(r1, r1.value));
                    }
                } else {
                    emitBody(children.get(i), currReg);
                }
            }
        }

        //-- terminals/literals
        Register reg = newTmp();
        //memory.put(reg.name, reg);
        switch (terminal) {
            case Terminals.BOOLEAN:
                if (body.value.toString().equals("true")) {
                    //1 for true
                    currReg.value = "1";
                } else {
                    //body.value == false
                    //0 for false
                    currReg.value = "0";
                }
                break;
            case Terminals.INTEGER:
                currReg.value = body.value.toString();
                break;
            case Terminals.NULL:
            //return Null;
            case Terminals.STRING:
                String strLiteral = body.value.toString();
                String strLabel = strLiterals.get(strLiteral);
                int size = strLiteral.length() + 1;

                writeI("" + assign(reg));
                writeC("call %struct.obj_String* @String_constructor(i8* getelementptr inbounds (["
                        + size + " x i8]* " + strLabel + ", i32 0, i32 0))");
                writeC("store %struct.obj_String* " + reg.getName() + ", %struct.obj_String** " + currReg.getName() + ", align 4");

                //throw new CodeGeneratorException("Bogus Error to view application stack");
                memory.put(reg.name, reg);
                break;
            case Terminals.ID:
                String name = body.value.toString();
                reg = new Register();
                reg = lookup(name, reg.varHeader);

                //look up in master table (as a quick fix)
                if (reg == null) {
                    reg = new Register();

                    reg.name = name;
                    //reg.name = mtable.getClass(classObj.getName()).getMethod(name).getName();
                    reg.header = reg.varHeader;

                    //write something else if it's a method call
                    writeC(loadInt(reg));
                } else {
                    writeC(loadInt(reg));
                }
                
                memory.put(reg.name, reg);
                break;
            case Terminals.THIS:
            //if this.(id)
            //look up varformals or varattributes of the class for the register
            case Terminals.NATIVE:
            //return ?;
        }
    }
}