package cool;

/**
 * This class lists terminals used by the
 * grammar specified in "parser.grammar".
 */
public class Terminals {
	static public final short EOF = 0;
	static public final short LPAREN = 1;
	static public final short ID = 2;
	static public final short LBRACE = 3;
	static public final short NULL = 4;
	static public final short MINUS = 5;
	static public final short SUPER = 6;
	static public final short NEW = 7;
	static public final short INTEGER = 8;
	static public final short STRING = 9;
	static public final short BOOLEAN = 10;
	static public final short THIS = 11;
	static public final short NOT = 12;
	static public final short IF = 13;
	static public final short WHILE = 14;
	static public final short RPAREN = 15;
	static public final short TYPE = 16;
	static public final short VAR = 17;
	static public final short COLON = 18;
	static public final short RBRACE = 19;
	static public final short SEMI = 20;
	static public final short ASSIGN = 21;
	static public final short DEF = 22;
	static public final short NATIVE = 23;
	static public final short COMMA = 24;
	static public final short EQUALS = 25;
	static public final short TIMES = 26;
	static public final short DIV = 27;
	static public final short CASE = 28;
	static public final short ARROW = 29;
	static public final short CLASS = 30;
	static public final short DOT = 31;
	static public final short OVERRIDE = 32;
	static public final short PLUS = 33;
	static public final short ELSE = 34;
	static public final short EXTENDS = 35;
	static public final short MATCH = 36;
	static public final short LE = 37;
	static public final short LT = 38;

	static public final String[] NAMES = {
		"EOF",
		"LPAREN",
		"ID",
		"LBRACE",
		"NULL",
		"MINUS",
		"SUPER",
		"NEW",
		"INTEGER",
		"STRING",
		"BOOLEAN",
		"THIS",
		"NOT",
		"IF",
		"WHILE",
		"RPAREN",
		"TYPE",
		"VAR",
		"COLON",
		"RBRACE",
		"SEMI",
		"ASSIGN",
		"DEF",
		"NATIVE",
		"COMMA",
		"EQUALS",
		"TIMES",
		"DIV",
		"CASE",
		"ARROW",
		"CLASS",
		"DOT",
		"OVERRIDE",
		"PLUS",
		"ELSE",
		"EXTENDS",
		"MATCH",
		"LE",
		"LT"
	};
}
