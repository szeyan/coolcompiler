package cool.typeCheck;

import beaver.Symbol;
import cool.ast.Node;
import cool.codegen.Register;

public class VariableObj {

    public Boolean inherited;
    public String name;
    public ClassObj thisClass;
    public ClassObj type;
    public Node node;
    public Register reg;
    public int itemNumber;
    
    public VariableObj(){
        this("",null,null,null);
    }

    public VariableObj(String name, ClassObj type, ClassObj cls, Node node) {
        this.name = name;
        this.inherited = false;
        this.type = type;
        this.thisClass = cls;
        this.node = node;
        this.reg = null;
        this.itemNumber = -1;
    }

    public void setRegister(Register reg) {
        this.reg = reg;
    }

    public void setItemNumber(int num) {
        this.itemNumber = num;
    }

    public void setThisClass(ClassObj co) {
        this.thisClass = co;
    }

    /**
     * @return if this method was inherited from a superclass
     */
    public boolean isInherited() {
        return this.inherited;
    }

    /**
     * sets if this method is inherited
     */
    public void setInherited(Boolean inherit) {
        this.inherited = inherit;
    }

    /**
     * compares this VariableObj and another VariableObj to make sure they are equal
     *
     * @param o the other VariableObj to compare to
     */
    public boolean equals(VariableObj o) {
        if (o == null) {
            return false;
        }
        boolean result = true;
        result = result && this.name.equals(o.name);
        result = result && this.thisClass.equals(o.thisClass);
        result = result && this.type.equals(o.type);
        result = result && this.node == o.node;
        return result;
    }

    public String toString() {
        String s = "[";

        Symbol symbol = (Symbol) node;
        s += "Name: " + name;
        s += ", ";
        s += "Type: " + type.getName();


        s += ", ";
        s += "Class: " + thisClass.getName();


        if (symbol != null) {
            s += ", ";
            s += "At Line: " + symbol.getLine(symbol.getStart());
            s += ", ";
            s += "At Column: " + symbol.getColumn(symbol.getStart());
        } // */
        s += "]";

        return s;
    }

    /**
     * Copy constructor
     */
    public VariableObj copy(VariableObj source) {
        VariableObj va = new VariableObj(source.name, source.type, source.thisClass, source.node);
        va.inherited = false;
        va.reg = null;
        va.itemNumber = -1;
        
        return va;
    }
}