package cool.typeCheck;

/**
 * This is a helper file that stores information about a class in the method
 * table
 *
 * @author Sze Yan Li, Kevin Garsjo
 */
import cool.ErrorMaster;
import cool.ast.Node;
import java.util.LinkedHashMap;
import cool.typeCheck.M.ClassException;
import java.util.LinkedList;

public class ClassObj {

    private String programName;
    private String name;
    private String parent;
    private LinkedHashMap<String, MethodObj> methods;
    private LinkedHashMap<String, VariableObj> attributes;
    private LinkedHashMap<String, VariableObj> varformals;
    private LinkedList<Node> execBlocks;
    private Node node;

    /**
     * constructor takes in the name of the class and the name of its super
     * class
     *
     * @param name name of class
     * @param parent name of super class
     * @param programName the name of the program in which this class belongs to
     * @param node the class node associated with this class
     */
    public ClassObj(String name, String parent, String programName, Node node) {
        this.programName = programName;
        this.name = name;
        this.parent = parent;
        this.node = node;
        this.attributes = new LinkedHashMap<String, VariableObj>();
        this.methods = new LinkedHashMap<String, MethodObj>();
        this.varformals = new LinkedHashMap<String, VariableObj>();
        this.execBlocks = new LinkedList<Node>();
    }

    /**
     * Constructor for default types already implemented in cool,such as...
     * Integer
     * Boolean
     * String
     * Any
     * Nothing
     * Null
     *
     * @param name the name of the class
     */
    public ClassObj(String name) {
        this.name = name;
        this.programName = null;
        this.parent = null;
        this.node = null;
        methods = new LinkedHashMap<String, MethodObj>();

    }

    /**
     * @return the name of this class
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the name of the super class
     */
    public String getParentName() {
        return this.parent;
    }

    /**
     * @return the name of the program in which this class belongs to
     */
    public String getProgramName() {
        return this.programName;
    }

    /**
     * @return the node matching this class
     */
    public Node getNode() {
        return this.node;
    }

    /**
     * @return the constructor for this class
     */
    public MethodObj getConstructor() {
        return this.methods.get(this.getName());
    }
    
        /**
     * adds a block into the list of execution blocks in this class
     */
    public void addExecutionBlock(Node node) {
        this.execBlocks.add(node);
    }
    
    /**
     * @return the execution blocks of this class
     */
    public LinkedList<Node> getExecBlocks() {
        return this.execBlocks;
    }
    /**
     * Looks for a method within this class
     *
     * @param methodName the method to look for
     * @return the MethodObj associated with the given method name
     */
    public MethodObj getMethod(String methodName) {
        return this.methods.get(methodName);
    }

    /**
     * @return the LinkedHashMap of methods for this class
     */
    public LinkedHashMap<String, MethodObj> getMethods() {
        return this.methods;
    }

    /**
     * adds a method to the list of methods contained in this class
     *
     * @param methodName name of the method
     * @param mo MethodObj to store
     */
    public void addMethod(String methodName, MethodObj mo) {
        this.methods.put(methodName, mo);
    }

    /**
     * @return the LinkedHashMap of attributes for this class
     */
    public LinkedHashMap<String, VariableObj> getAttributes() {
        return this.attributes;
    }

    /**
     * adds an attribute to the list of attributes contained in this class
     *
     * @param attributeName name of the attribute
     * @param va VariableObj to store
     */
    public void addAttribute(String attributeName, VariableObj va, ErrorMaster errorMaster) throws ClassException {
        if (this.attributes.containsKey(attributeName)) {
            errorMaster.send("Attribute \"" + attributeName + "\" already defined", errorMaster.getFilePath(name), name, node);
            throw new ClassException("");
        }
        this.attributes.put(attributeName, va);
    }

    /**
     * finds an attribute belonging in this class
     *
     * @return the matching varattribute if found, null if not found
     */
    public VariableObj getAttribute(String attributeName) {
        return this.attributes.get(attributeName);
    }

    /**
     * finds an attribute's type belonging in this class
     *
     * @return the attribute's type if found, null if not found
     */
    public String getAttributeType(String attributeName) {
        return this.attributes.get(attributeName).type.getName();
    }

    /**
     * @return the LinkedHashMap of varformals for this class
     */
    public LinkedHashMap<String, VariableObj> getVarformals() {
        return this.varformals;
    }

    /**
     * adds an attribute to the list of varformals contained in this class
     *
     * @param vfName name of the attribute
     * @param va of VariableObj to store
     */
    public void addVarformal(String vfName, VariableObj va, ErrorMaster errorMaster) throws ClassException {
        if (this.varformals.containsKey(vfName)) {
            errorMaster.send("Varformal \"" + vfName + "\" already defined", errorMaster.getFilePath(name), name, node);
            throw new ClassException("");
        }
        this.varformals.put(vfName, va);
    }

    /**
     * finds an Varformal belonging in this class
     *
     * @return the Varformal's type if found, null if not found
     */
    public VariableObj getVarformal(String vfName) {
        return this.varformals.get(vfName);
    }

    /**
     * compares this ClassObj and another ClassObj to make sure they are equal
     *
     * @param o the other ClassObj to compare to
     */
    public boolean equals(ClassObj o) {
        if (o == null) {
            return false;
        }

        boolean result = true;
        result = result && this.name.equals(o.name);

        return result;
    }
}
