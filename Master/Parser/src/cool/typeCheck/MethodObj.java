package cool.typeCheck;

import cool.ast.Node;
import java.util.LinkedHashMap;

/**
 * This is a helper file that stores information about a method in the method
 * table
 *
 * @author Sze Yan Li
 */
public class MethodObj {

    private boolean inherited;
    private String className;
    private String name;
    private String returnType;
    private LinkedHashMap<String, String> params;
    private Boolean overrides;
    private Boolean returnTypeDeclared;
    private Node node;

    /**
     * constructor takes in the name of the method
     *
     * @param name name of the method
     */
    public MethodObj(String name) {
        this.inherited = false;
        this.className = "";
        this.name = name;
        this.returnType = "";
        this.overrides = false;
        this.returnTypeDeclared = false;
        this.params = new LinkedHashMap<String, String>();
        this.node = null;
    }

    /**
     * @return the name of this class
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return if this method was inherited from a superclass
     */
    public boolean isInherited() {
        return this.inherited;
    }

    /**
     * sets if this method is inherited
     */
    public void setInherited(Boolean inherit) {
        this.inherited = inherit;
    }

    /**
     * @return if this method overrides
     */
    public boolean isOverride() {
        return this.overrides;
    }

    /**
     * sets if this method overrides
     */
    public void setOverride(Boolean overrides) {
        this.overrides = overrides;
    }

    /**
     * @return the return type for this method if the return type has been set else returns an error message
     */
    public String getReturnType() {
        if (this.returnTypeDeclared) {
            return this.returnType;
        }
        return "Return type has not been declared";
    }

    /**
     * @return the list of parameters (already in insertion order)
     */
    public LinkedHashMap getParams() {
        return this.params;
    }

    /**
     * add an parameter to this method
     *
     * @param name of the parameter
     * @param type of the parameter
     */
    public void addParam(String name, String type) {
        this.params.put(name, type);
    }

    /**
     * sets the class this method belongs to
     */
    public void setClassName(String name) {
        this.className = name;
    }

    /**
     * sets the return type for this method
     *
     * @param type is the return type for this method
     */
    public void setReturnType(String type) {
        this.returnType = type;
        this.returnTypeDeclared = true;
    }

    public Node getNode() {
        return this.node;
    }

    public void setNode(Node n) {
        this.node = n;
    }

    public String getClassName() {
        return this.className;
    }

    /**
     * Copy constructor
     */
    public MethodObj copy(MethodObj source) {
        MethodObj mo = new MethodObj(source.name);
        mo.className = "";
        mo.returnType = source.returnType;
        mo.returnTypeDeclared = source.returnTypeDeclared;
        mo.node = source.node;
        mo.params = source.params;
        mo.overrides = false;
        mo.inherited = false;

        return mo;
    }

    public String toString() {
        String s = "";

        s += "[";
        s += "MethodName: " + this.name + "\n";
        s += "ClassName: " + this.className + "\n";
        s += "overrides: " + this.overrides + "\n";
        s += "inherited: " + this.inherited + "\n";
        s += "ReturnType: " + this.returnType + "\n";
        s += "Params: " + this.params;
        s += "]";

        return s;
    }
}
