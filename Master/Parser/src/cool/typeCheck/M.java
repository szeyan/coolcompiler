package cool.typeCheck;

/**
 * Method table for cool parser
 *
 * COOL method rules:
 * //- methods must be unique within a class
 * //- methods can have 0 or more formal parameters
 * //- override must be used to 'override' an extended method
 * //- extended method must contain the same return type
 * //- extended method must contain the same type arguments
 *
 * @author Sze Yan Li
 */
import beaver.Symbol;
import cool.ErrorMaster;
import cool.ast.Node;
import cool.ast.ProgramNode;
import cool.ast.TreeWalker;
import cool.typeCheck.ClassMaster.KTree;
import cool.typeCheck.ClassMaster.KTreeTraverse;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

public class M {

    public static class ClassException extends Exception {

        public ClassException(String error) {
            super(error);
        }
    }
    private ErrorMaster errorMaster;
    public ClassMaster cm;
    private ClassObj Any;

    /**
     * constructor
     */
    public M(ErrorMaster errorMaster) {
        this.errorMaster = errorMaster;
        this.cm = new ClassMaster(this.errorMaster);
        this.Any = new ClassObj("Any");

    }

    /**
     * extracts the classes from a given program and adds the classes to the method table
     */
    public void add(ProgramNode pgm) throws ClassException {
        this.cm.add(pgm);
    }

    /**
     * builds and fills the method table based on given program files
     */
    public void build() throws ClassException {
        //--build the class hierarchy 
        this.cm.check();

        //--build the methods of each class
        //get the string[] representation of the class hierarchy in cm
        LinkedList classList = this.cm.getClassList();

        //loop through the classes within the classList
        for (int i = 0; i < classList.size(); i++) {
            String className = (String) classList.get(i);

            //skip native because it's not actually a class
            if (className.equals("native")) {
                continue;
            }

            //obtain the ClassObj matched with the className
            ClassObj co = (ClassObj) this.getClass(className);

            //obtain all the methods from an inherited class
            this.inheritMethods(co);

            //filter through all the FeatureNodes 
            TreeWalker tw = new TreeWalker(co.getNode());
            LinkedList features = tw.filter("FEATURE");

            //loop through each feature and search each method
            for (int m = 0; m < features.size(); m++) {
                Node featureNode = (Node) features.get(m);

                //get the method
                TreeWalker tw2 = new TreeWalker(featureNode);
                Node methodNode = tw2.get("METHOD");

                //if this feature contained a method check the method
                if (methodNode != null) {

                    //check if there's an override node (should be the first child)
                    Boolean overrideExists = false;
                    Symbol orSymbol = (Symbol) featureNode.getList().get(0);
                    if (orSymbol.value.toString().equals("override")) {
                        //an override exists for this method
                        overrideExists = true;
                    }

                    //get the id of the MethodNode (method name) 
                    Symbol methodID = (Symbol) methodNode.getList().get(0); //method name is always the first child of in a MethodNode
                    String methodName = methodID.value.toString();

                    //create a MethodObj with the id
                    MethodObj mo = new MethodObj(methodName);
                    mo.setNode(methodNode);
                    mo.setClassName(co.getName());

                    //filter for all the FormalsNodes
                    TreeWalker tw3 = new TreeWalker(methodNode);
                    LinkedList formals = tw3.filter("FORMALS");

                    //for each formal param found, get the id and type
                    for (int k = 0; k < formals.size(); k++) {
                        Node formalsNode = (Node) formals.get(k);

                        //get the id and type (the formal param)
                        Symbol paramID = (Symbol) formalsNode.getList().get(0);
                        String paramName = paramID.value.toString();
                        Symbol typeID = (Symbol) formalsNode.getList().get(1);
                        String typeName = typeID.value.toString();

                        //add id and type as an argument of the method
                        //make sure param isn't already defined
                        if (mo.getParams().containsKey(paramName)) {
                            errorMaster.send("parameter \"" + paramName + "\" in method \"" + mo.getName() + "\" has already been defined.", "", co.getName(), co.getNode());
                            throw new ClassException("");
                        } else {
                            mo.addParam(paramName, typeName);
                        }

                    }

                    //get the ReturnTypeNode and set the return type in mo
                    Node returnTypeNode = tw3.get("RETURNTYPE");
                    Symbol rtTypeID = (Symbol) returnTypeNode.getList().get(0);
                    mo.setReturnType(rtTypeID.value.toString());

                    //check to see if this method was defined by the parent class
                    ClassObj coParent = (ClassObj) this.getClass(co.getParentName());

                    //checks for case where the Any extends native
                    if (coParent != null) {
                        //get the method of the parent
                        MethodObj moParent = coParent.getMethod(methodName);

                        //regardless if method was found: check if OVERRIDE keyword was used by subclass
                        if ((overrideExists && moParent == null) || (!overrideExists && moParent != null)) {
                            //if method wasn't found but OVERRIDE was used: error
                            //if method was found but override wasn't used: error
                            errorMaster.send("OVERRIDE usage incorrect in method \"" + mo.getName() + "\"", "", co.getName(), co.getNode());

                            throw new ClassException("");
                        } else if (overrideExists && moParent != null) {
                            //method was found and OVERRIDE was used

                            //check to make sure parent's return type and child's return types are the same
                            if (!mo.getReturnType().equals(moParent.getReturnType())) {
                                //check for conformity of return types
                                if (!this.conformsTo(this.getClass(mo.getReturnType()), this.getClass(moParent.getReturnType()))) {
                                    //return types differ (or do not conform)
                                    errorMaster.send(
                                            "method \"" + mo.getName() + "\"'s return type does not match its super class's: ("
                                            + coParent.getName() + ", " + moParent.getName() + ")",
                                            "", co.getName(), mo.getNode());

                                    throw new ClassException("");
                                }
                            }

                            //get the list of arguments from the parent class
                            LinkedHashMap moParentArgs = moParent.getParams();

                            //get the list of arguments from this class
                            LinkedHashMap moArgs = mo.getParams();

                            //check if methods arg size matches
                            if (moArgs.size() != moParentArgs.size()) {
                                //non-matching args
                                errorMaster.send(
                                        "method \"" + mo.getName() + "\"'s arguments do no match its super class's: ("
                                        + coParent.getName() + ", " + moParent.getName() + ")",
                                        "", co.getName(), mo.getNode());

                                throw new ClassException("");
                            }

                            //loop through (using parent as a base) and make sure each class's (key,value) and parent's (key,value) pair matches                           
                            Set mpa = moParentArgs.keySet();
                            Iterator itr1 = mpa.iterator();
                            while (itr1.hasNext()) {
                                //get the argument name
                                String parentArgName = (String) itr1.next();
                                //get the matching argument type
                                String parentArgType = (String) moParentArgs.get(parentArgName);

                                Set ma = moArgs.keySet();
                                Iterator itr2 = ma.iterator();
                                while (itr2.hasNext()) {
                                    //get the argument name
                                    String argName = (String) itr2.next();
                                    //get the matching argument type
                                    String argType = (String) moArgs.get(argName);

                                    //check that the arg types are the same
                                    if (!parentArgType.equals(argType)) {
                                        //non-matching args
                                        errorMaster.send(
                                                "method \"" + mo.getName() + "\"'s arguments do no match its super class's: ("
                                                + coParent.getName() + ", " + moParent.getName() + ")",
                                                "", co.getName(), mo.getNode());

                                        throw new ClassException("");
                                    }
                                }
                            }
                        }
                    }

                    //if no override exists but the method has already been defined
                    if (!overrideExists && co.getMethods().containsKey(methodName)) {
                        //error: this method has already been defined in this class
                        errorMaster.send("Method has already been defined.", "", co.getName(), mo.getNode());
                        throw new ClassException("");
                    } else if (overrideExists && co.getMethods().containsKey(methodName)) {
                        //check if current method is overriding a method currently defined in the class
                        if (co.getMethod(methodName).getClassName().equals(mo.getClassName())) {
                            errorMaster.send("Method has already been defined.", "", co.getName(), mo.getNode());
                            throw new ClassException("");
                        }

                        //an override exists for this method, so we have to overwrite the inherited method
                        co.getMethods().remove(methodName);
                        mo.setNode(methodNode);
                        mo.setOverride(true);
                        co.addMethod(methodName, mo);
                    } else {
                        //hasn't been defined, just add it
                        mo.setNode(methodNode);
                        co.addMethod(methodName, mo);
                    }
                }
            }
        }


        //--build in the constructor method for all classes
        this.addConstructors();
    }

    /**
     * Adds in constructors for all classes into the M table
     */
    private void addConstructors() throws ClassException {
        LinkedList classList = this.cm.getClassList();
        for (int i = 0; i < classList.size(); i++) {
            String className = (String) classList.get(i);

            //basic classes and native or main should not have a constructor     
            String basic[] = {"Main", "native", "Any", "IO", "Statistics", "Symbol", "Unit", "Int", "Boolean", "String"};

            boolean add = true;

            for (int j = 0; j < basic.length; j++) {
                if (className.equals(basic[j])) {
                    add = false;
                    break;
                }
            }

            if (add) {
                //obtain the ClassObj matched with the className
                ClassObj co = (ClassObj) this.getClass(className);

                //create constructor method
                MethodObj constructor = new MethodObj(co.getName());
                constructor.setReturnType(co.getName());

                LinkedList varformals = new TreeWalker(co.getNode()).filter("VARFORMALS");

                for (int j = 0; j < varformals.size(); j++) {
                    Node formalsNode = (Node) varformals.get(j);

                    //get the id and type (the formal param)
                    Symbol paramID = (Symbol) formalsNode.getList().get(0);
                    String paramName = paramID.value.toString();
                    Symbol typeID = (Symbol) formalsNode.getList().get(1);
                    String typeName = typeID.value.toString();

                    //make sure param isn't already defined (shouldn't be)
                    if (constructor.getParams().containsKey(paramName)) {
                        errorMaster.send("parameter \"" + paramName + "\" in constructor has already been defined.", "", constructor.getName(), constructor.getNode());
                        throw new ClassException("");
                    } else {
                        constructor.addParam(paramName, typeName);
                    }

                }

                //add constructor method to the class
                co.addMethod(co.getName(), constructor);


            }

        }

    }

    /**
     * Puts all inherited methods of a class into its list of methods
     *
     * @param co class object that needs inherited methods
     */
    private void inheritMethods(ClassObj co) {
        //get the parent name
        String pName = co.getParentName();

        //skip native because it's not actually a class
        if (!pName.equals("native")) {
            //get parent obj
            ClassObj pClass = (ClassObj) this.getClass(pName);

            //get parent methods
            LinkedHashMap pMethods = pClass.getMethods();

            //add all the methods to this class
            Iterator<MethodObj> itr = pMethods.values().iterator();
            
            while(itr.hasNext()){
                MethodObj original = itr.next();
                
                MethodObj mo = new MethodObj("");
                mo = mo.copy(original);
                mo.setInherited(true);
                //don't set the className for mo... keep it as is
                co.getMethods().put(mo.getName(), mo);
                
                //System.err.println("original" + original.toString());
                //System.err.println();
                //System.err.println("copyTo" + mo.toString());
                //System.err.println();
            }
        }


    }

    /**
     * Check for conformity. If c1 conforms to c2
     *
     * @author Kevin Garsjo
     */
    private boolean conformsTo(ClassObj c1, ClassObj c2) {
        if (c2.equals(Any)) {
            return true;
        }

        while (!c1.equals(Any)) {
            if (c1.equals(c2)) {
                return true;
            }
            c1 = this.getClass(c1.getParentName());
        }

        return false;
    }

    /**
     * @param className name of class
     * @param methodName name of method
     *
     * @return the matching method name. Null if not found
     */
    public MethodObj getMethod(String className, String methodName) {
        return this.getClass(className).getMethod(methodName);
    }

    /**
     * @param className name of the class to find
     *
     * @return the class object matched. Null if not found
     */
    public ClassObj getClass(String className) {
        return (ClassObj) this.cm.getClassMap().get(className);
    }

    /**
     * @return a linked hash map of all the classes
     */
    public LinkedHashMap getClasses() {
        return this.cm.getClassMap();
    }

    /**
     * preserving order at all times, prints the list of classes in M table
     */
    public void printClasses() {
        System.out.println(this.cm.getClassList().toString());
    }

    /**
     * preserving order at all times, prints each class and its methods including the args and return type
     */
    public void printAll() {
        LinkedList classList = this.cm.getClassList();
        for (int i = 0; i < classList.size(); i++) {
            String className = (String) classList.get(i);



            //skip native because it's not actually a class
            if (className.equals("native")) {
                System.out.println("special: native");
                System.out.println();
                continue;
            }

            System.out.println("class: " + className);

            //obtain the ClassObj matched with the className
            ClassObj co = (ClassObj) this.getClass(className);

            System.out.println("methods (in order): ");

            Set keys = co.getMethods().keySet();
            Iterator itr = keys.iterator();
            while (itr.hasNext()) {
                String mn = (String) itr.next();
                System.out.println("\t" + mn);

                System.out.print("\t\targs: ");
                System.out.println(co.getMethod(mn).getParams().toString());

                System.out.println("\t\treturn type: " + co.getMethod(mn).getReturnType());
            }
            System.out.println();
        }
    }
}
