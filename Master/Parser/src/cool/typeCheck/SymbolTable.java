package cool.typeCheck;

import cool.ErrorMaster;
import java.util.LinkedHashMap;
import cool.ast.Node;
import cool.typeCheck.TypeCheck.TypeException;
import java.util.Collection;
import java.util.Iterator;

public class SymbolTable {
    private ErrorMaster errorMaster;
    M mtable = null;
    LinkedHashMap<String, VariableObj> table;

    public SymbolTable(M mtable, ErrorMaster errorMaster) {
        this.mtable = mtable;
        this.errorMaster = errorMaster;
        this.table = new LinkedHashMap<String, VariableObj>();
    }

    public SymbolTable(SymbolTable another) {
        this.table = another.table;
    }

    public VariableObj add(Boolean suppressError, String name, VariableObj va) throws TypeException {
        VariableObj temp = this.table.get(name);
        if (temp == null || name.equals("this") || suppressError) {
            return this.table.put(name, va);
        } else {
            errorMaster.send("Duplicate Variable Declaration", "", va.thisClass.getName(), temp, va);
            throw new TypeException("");
        }

    }

    public VariableObj add(String name, VariableObj va) throws TypeException {
        VariableObj temp = this.table.get(name);
        if (temp == null || name.equals("this")) {
            return this.table.put(name, va);
        } else {
            errorMaster.send("Duplicate Variable Declaration", "", va.thisClass.getName(), temp, va);
            throw new TypeException("");
        }

    }

    public VariableObj lookup(String name) {
        return this.table.get(name);
    }

    public String toString() {
        String s = "Symbol Table: " + "{\n";
        if (this.table != null) {
            Collection<VariableObj> vs = this.table.values();
            Iterator<VariableObj> itr = vs.iterator();
            while(itr.hasNext()){
                VariableObj v = itr.next();
                s += "\t" + v.toString();
                if (itr.hasNext()) {
                    s += ", \n";
                }
            }

            s += "\n}";
        }
        return s;

    }
}