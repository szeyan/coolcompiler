package cool;

import beaver.Symbol;
import cool.typeCheck.VariableObj;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Error Reporting Class for Cool Scanner
 * CIS461
 * Sze Yan Li
 */
public class ErrReport {

    public int ErrCount;
    private String fileName;

    //Create ErrReport with a file name
    public ErrReport(String fileName) {
        this.ErrCount = 0;
        this.fileName = fileName;
    }

    //Create ErrReport
    public ErrReport() {
        this.ErrCount = 0;
        this.fileName = "";
    }

    //Set the current filename
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    //Send a scanner error
    public void send(String s, int line, int col) {
        ErrCount++;
        System.err.println("Error in file \"" + this.fileName + "\" (" + line + ", " + col + "): " + s);
    }
}
