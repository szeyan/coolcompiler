/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;
import cool.Terminals;
import java.util.LinkedList;

public class ProgramNode extends Node {

    public boolean isFlat;
    public String filePath;
    private String fileName;
    public String type;
    public LinkedList list;
    public LinkedList flatList;

    /*
     * Constructor for a node which creates a list of children based on an abitrary list of Symbols
     */
    public ProgramNode(Symbol... s) {
        super();
        this.isFlat = false;
        this.filePath = "";
        this.fileName = "";
        this.type = "";
        this.flatList = new LinkedList();
        //add all the Symbols (or Nodes) as children
        this.list = new LinkedList<Object>();
        for (Object child : s) {
            this.list.add(child);

            //check and get TYPE
            if (!(child instanceof Node)) {
                Symbol sym = (Symbol) child;

                if (Terminals.NAMES[sym.getId()].equals("TYPE")) {
                    this.type = sym.value.toString();
                } else if (Terminals.NAMES[sym.getId()].equals("NATIVE")) {
                    this.type = sym.value.toString();
                }
            }
        }

        this.intermediate();
    }

    /*
     * Retrieves the list of children of the node
     */
    public LinkedList getList() {
        return this.list;
    }

    /*
     * checks if the children are already flattened
     */
    public boolean isFlat() {
        return this.isFlat;
    }

    /*
     * set the boolean if the children are already flattened
     */
    public void setFlat(boolean flat) {
        this.isFlat = flat;
    }

    /*
     * Retrieves the list of children of the node (flattened)
     */
    public LinkedList getFlatList() {
        return this.flatList;
    }

    /*
     * Sets the flattened list
     * @param flat is the list to replace flatList
     */
    public void setFlatList(LinkedList flat) {
        this.flatList = flat;
    }


    /*
     * adds a number of Symbols to the current list of children of the node
     */
    public void add(Symbol... s) {
        for (Object child : s) {
            this.list.add(child);
        }
    }

    /*
     * sets the name of the program (file name) that is being parsed
     *
     * Courtesy of: http://stackoverflow.com/questions/1011287/get-file-name-from-a-file-location-in-java
     * for stripping folder locations and just retrieving the file name
     */
    public void setFN(String s) {
        //retrieve the last index where the last / occured
        int deleteIndex = s.replaceAll("//", "/").lastIndexOf("/");

        //if the index is greater than zero then retrieve the substring
        //else set the program name to just the parameter S
        this.fileName = deleteIndex >= 0 ? s.substring(deleteIndex + 1) : s;
        this.filePath = s;
    }

    /*
     * @return the name of the program (file name) that is being parsed
     */
    public String getFN() {
        return this.fileName;
    }

    /*
     * @return the name of the program (file name) that is being parsed (full version)
     */
    public String getFP() {
        return this.filePath;
    }

    /*
     * @return the name of the program (file name) that is being parsed, stripping the .cool extension
     */
    public String getBaseFN() {
        return (this.getFN().split("\\.cool$"))[0];
    }

    /*
     * @return a String representation for the class name of the node
     */
    public String toString() {
        String temp = this.getClass().getSimpleName();
        return temp.substring(0, temp.length() - 4).toUpperCase();
    }


    /*
     * Alters a production rule to its intermediate form
     */
    public void intermediate() {
    }

    /*
     * returns the TYPE of a node
     */
    public String getType() {
        return this.type;
    }
}