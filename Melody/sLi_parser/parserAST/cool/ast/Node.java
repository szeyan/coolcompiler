/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;
import cool.Terminals;
import java.util.LinkedList;
import java.util.Vector;

public abstract class Node extends Symbol {

    /*
     * Retrieves the list of children of the node
     */
    public abstract LinkedList getList();

    /*
     * adds a number of Symbols to the current list of children of the node
     */
    public abstract void add(Symbol... s);

    /*
     * @return a String representation for the class name of the node
     */
    public abstract String toString();

    /*
     * Alters a production rule to its intermediate form
     */
    public abstract void intermediate();

    /*
     * returns the TYPE of a node
     */
    public abstract String getType();
}