/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;

public class LessEqualNode extends ExprNode {

    /*
     * Constructor for a node which creates a list of children based on an abitrary list of Symbols
     */
    public LessEqualNode(Symbol... s) {
        super(s);
    }
}