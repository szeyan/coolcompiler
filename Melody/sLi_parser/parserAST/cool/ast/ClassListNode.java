/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;

public class ClassListNode extends ProgramNode {

    /*
     * Constructor for a node which creates a list of children based on an abitrary list of Symbols 
     */
    public ClassListNode(Symbol... s) {
        super(s);
    }
}