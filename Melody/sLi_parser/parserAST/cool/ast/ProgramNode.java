/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;
import cool.Terminals;
import java.util.LinkedList;

public class ProgramNode extends Node {

    private String programName;
    public String type;
    public LinkedList list;

    /*
     * Constructor for a node which creates a list of children based on an abitrary list of Symbols 
     */
    public ProgramNode(Symbol... s) {
        super();
        programName = "";
        type = "";
        //add all the Symbols (or Nodes) as children
        this.list = new LinkedList<Object>();
        for (Object child : s) {
            this.list.add(child);
            
            //check and get TYPE
            if(!(child instanceof Node)){
                Symbol sym = (Symbol) child;
                
                if(Terminals.NAMES[sym.getId()].equals("TYPE")){
                   this.type = sym.value.toString(); 
                }else if(Terminals.NAMES[sym.getId()].equals("NATIVE")){
                   this.type = sym.value.toString(); 
                }             
            }
        }

        this.intermediate();
    }

    /*
     * Retrieves the list of children of the node
     */
    public LinkedList getList() {
        return this.list;
    }

    /*
     * adds a number of Symbols to the current list of children of the node
     */
    public void add(Symbol... s) {
        for (Object child : s) {
            this.list.add(child);
        }
    }

    /*
     * sets the name of the program that is being parsed
     * 
     * Courtesy of: http://stackoverflow.com/questions/1011287/get-file-name-from-a-file-location-in-java
     * for stripping folder locations and just retrieving the file name
     */
    public void setPN(String s) {
        //retrieve the last index where the last / occured
        int deleteIndex = s.replaceAll("//", "/").lastIndexOf("/");

        //if the index is greater than zero then retrieve the substring
        //else set the program name to just the parameter S
        this.programName = deleteIndex >= 0 ? s.substring(deleteIndex + 1) : s;
    }

    /*
     * @return the name of the program that is being parsed
     */
    public String getPN() {
        return "\"" + this.programName + "\"";
    }

    /*
     * @return a String representation for the class name of the node 
     */
    public String toString() {
        String temp = this.getClass().getSimpleName();
        return temp.substring(0, temp.length() - 4).toUpperCase();
    }

    /*
     * Alters a production rule to its intermediate form
     */
    public void intermediate() {
    }
       
    /*
     * returns the TYPE of a node
     */
    public String getType(){
        return this.type;      
    }
}