/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is part of CoolParser
 *
 * @author Sze Yan Li
 * CIS461
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package cool.ast;

import beaver.Symbol;
import cool.Terminals;

public class ExtendsNode extends ClassNode {
    /*
     * Constructor for a node which creates a list of children based on an abitrary list of Symbols 
     */
    public ExtendsNode(Symbol... s) {
        super(s);   
    }
    
    public void intermediate(){
        //All classes extends Any by default
        if (this.getList().isEmpty() && this.toString().equals("EXTENDS")) {
            String name = "TYPE";
            this.type = "Any";
            short id = -1;

            //loop through terminals list (insures even if terminals change, we will still get the terminal id
            for (int i = 0; i < Terminals.NAMES.length; i++) {
                if (Terminals.NAMES[i].equals(name)) {
                    id = (short) i;
                }
            }

            //just to make sure we found our terminal
            if (id != -1) {
                this.add(new Symbol(id, type));
            }

        }
    }
    
           
    /*
     * returns the TYPE of a node
     */
    public String getType(){
        return this.type;      
    }
}