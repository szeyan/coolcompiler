package cool;

/**
 * Error Reporting Class for Cool Scanner
 * CIS461
 * Sze Yan Li
 */
public class ErrReport {
    private int ErrCount;
    private String fileName;

    //Create ErrReport with a file name
    public ErrReport(String fileName){
        ErrCount = 0;
        this.fileName = fileName;
    }

    //Create ErrReport
    public ErrReport(){
        ErrCount = 0;
        this.fileName = "";
    }

    //Set the filename
    public void setFileName(String fileName){
        this.fileName = fileName;
    }

    //Send an error
    public void send(String s, int line, int col){
        ErrCount++;
        System.err.println("Error in file \""+fileName+"\" ("+line+", "+col+"): "+s);
    }
}
