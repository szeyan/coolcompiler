/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.typeCheck;

import beaver.Symbol;
import cool.Terminals;
import cool.ast.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author kgarsjo
 */
public class TypeCheck2 {
    
    public static class TypeException extends Exception {
        
        public TypeException(String error) {
            super(error);
        }
    }
    
    private M mtable;
    private SymbolTable stable;
    private SymbolStack stack;
    private ClassObj Any= null;
    private ClassObj Nothing= null;
    private ClassObj Unit= null;
    private ClassObj Null= null;
    private ClassObj Boolean= null;
    private ClassObj Integer= null;
    private ClassObj String= null;
    private ClassObj Unhandled= new ClassObj("Unhandled");
    private ClassObj Native= new ClassObj("Native");
    
    public TypeCheck2(M mtable, SymbolTable stable, SymbolStack stack) {
        this.mtable= mtable;
        this.stable= stable;
        this.stack= stack;
        Any= mtable.getClass("Any");
        Nothing= mtable.getClass("Nothing");
        Unit= mtable.getClass("Unit");
        Null= mtable.getClass("Null");
        Boolean= mtable.getClass("Boolean");
        Integer= mtable.getClass("Int");
        String= mtable.getClass("String");
    }
    
    public void checkType() throws TypeException {
        checkMethods();
    }
    
    public void checkMethods() throws TypeException {
        Collection<ClassObj> classes= mtable.getClasses().values();
        
        for (ClassObj cls : classes) {
            stack.pushClone();
            stack.addToCurrent("this", cls.getName(), cls.getNode());
            getAndCheckAttributes(cls.getNode());
            Collection<MethodObj> methods= cls.getMethods().values();
            
            for (MethodObj method : methods) {
                ClassObj rettype= mtable.getClass( method.getReturnType() );
                
                LinkedHashMap<String, String> args= method.getArgs();
                stack.pushClone();
                for (String arg : args.keySet()) {
                    stack.addToCurrent(arg, args.get(arg), null);
                }
                
                Symbol body= (Symbol) method.getNode().getList().get(3);
                if (body.value.equals("native")) {
                    continue;
                }
                checkType(rettype, (Node) body);
                
                stack.pop();
            }
            stack.pop();
        }
    }
    
    public void getAndCheckAttributes(Node classnode) throws TypeException {
        // Parse and capture varformals
        Node varformals= (Node) classnode.getList().get(1);
        if (varformals instanceof VarformalsNode) {
            String ident= (String) varformals.getList().get(0);
            String type= (String)  varformals.getList().get(1);
            stack.addToCurrent(ident, type, varformals);
        } else if (varformals.getList().size() > 1) {
            Node head= (VarformalsListNode) varformals;
            while (!(head instanceof VarformalsNode)) {
                VarformalsNode vf= (VarformalsNode) head.getList().get(1);
                String ident= (String) vf.getList().get(0);
                String type= (String)  vf.getList().get(1);
                stack.addToCurrent(ident, type, vf);
                head= (Node) head.getList().get(0);
            }
            String ident= (String) head.getList().get(0);
            String type= (String)  head.getList().get(1);
            stack.addToCurrent(ident, type, head);
        }
        
        // Parse and capture classbody feature declarations
        Node features= (Node) classnode.getList().get(3);
        if (features instanceof FeatureListNode) {
            
            for (Object obj : features.getList()) {
                FeatureNode feature= (FeatureNode) obj;
                Symbol id= (Symbol) feature.getList().get(0);
                
                if (id != null && id.getId() == Terminals.ID) {
                    Node vbody= (Node) feature.getList().get(1);
                    if (vbody.getList().size() > 1) {   // Distinguish between native assn and actual type
                        String type= (String) ((Symbol) vbody.getList().get(0)).value;
                        ExprNode e= (ExprNode) vbody.getList().get(1);
                        ClassObj t1= mtable.getClass(type);
                        ClassObj t2= eval(e);
                        if (!conformsTo(t2, t1)) {
                            throw new TypeException("Variable declaration for " + id.toString() + " invalid for type " + t2.getName());
                        }
                        stack.addToCurrent((String)id.value, type, feature);
                    } else {
                        stack.addToCurrent((String)id.value, Native.getName(), feature);
                    }
                    
                }
            }
        }
    }
    
    public void checkType(ClassObj expected, Node body) throws TypeException {
        ClassObj actual= eval(body);
        if (!expected.equals(actual)) {
            throw new TypeException(
                "Type " + expected.getName() + " did not match type " + actual.getName() +
                " evaluated from body " + body.toString()
            );
        }
    }
    
    // TODO Do actual evaluation
    public ClassObj eval(Symbol body) throws TypeException {
        
        // Edge case, null node
        if (body == null) {
            return null;
        }
        
        // Use terminal IDs to type literals
        int term= body.getId();
        
        // Use node child lists to type complex expressions
        LinkedList children= null;
        Node currNode= null;
        
        
        // Literals and Other Easy Cases
        switch (term) {
            case Terminals.BOOLEAN:
                return Boolean;
            case Terminals.INTEGER:
                return Integer;
            case Terminals.NULL:
                return Null;
            case Terminals.STRING:
                return String;
            case Terminals.ID:
            case Terminals.THIS:    // TODO Make sure THIS is being stored that way
                VarAttribute t1= stack.lookupCurrent((String) body.value);
                if (t1 == null) {
                    throw new TypeException("Identifer not found in symbol table: " + body.value);
                }
                return stack.lookupCurrent((String) body.value).type;
            case Terminals.NATIVE:
                return Any;
        }
        
        
        // Binary Arithmetic Expression Subtypes
        if (body instanceof PlusNode        // Plus Expr
         || body instanceof MinusNode       // Minus Expr
         || body instanceof TimesNode       // Mult Expr
         || body instanceof DivideNode) {   // Div Expr
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(Integer) && t2.equals(Integer)) {
                return Integer;
            }
            throw new TypeException("Arithmetic undefined for " + t1.getName() + " and " + t2.getName());
        }
        
        
        // Binary Boolean Expression Subtypes
        if (body instanceof LessThanNode        // Less Than
         || body instanceof LessEqualNode) {    // Less than or Equals
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(Integer) && t2.equals(Integer)) {
                return Boolean;
            }
            throw new TypeException("Relationals undefined for " + t1.getName() + " and " + t2.getName());
        }
        
        
        // Binary Equality Type
        if (body instanceof EqualNode) {
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(t2)) {
                if (t1.equals(Boolean) || t1.equals(Integer) || t1.equals(String)) {
                    return Boolean;
                }
            }
            throw new TypeException("Equality undefined for " + t1.getName() + " and " + t2.getName());
        }
        
        
        // Unary Expression Subtypes
        if (body instanceof NotNode) {          // Not Expr
            currNode= (Node) body;
            ClassObj t1= eval( (Symbol) currNode.getList().get(0) );
            if (t1.equals(Boolean)) { return Boolean; }
            throw new TypeException("Boolean negation undefined for " + t1.getName());
        }
        if (body instanceof UnaryMinusNode) {    // Integer Negation Expr
            currNode= (Node) body;
            ClassObj t1= eval( (Symbol) currNode.getList().get(0) );
            if (t1.equals(Integer)) { return Integer; }
            throw new TypeException("Integer negation undefined for " + t1.getName());
        }
        
        
        // Control Expression Subtype
        if (body instanceof IfElseNode) {       // If Statement
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            ClassObj t3= eval( (Symbol) children.get(2) );
            if (t1.equals(Boolean)) { 
                return join(t2, t3);
            }
            throw new TypeException("If condition undefined for type " + t1.getName());
        }
        if (body instanceof WhileNode) {        // While Loop
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj t1= eval( (Symbol) children.get(0) );
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (t1.equals(Boolean)) {
                // TODO Check some specific type rules for LOOP
                return Unit;
            }
            throw new TypeException("While condition undefined for type " + t1.getName());
        }
        
        
        // Dot Access Expression Subtype
        if (body instanceof DotNode) {
            currNode= (Node) body;
            children= currNode.getList();
            ClassObj cls= eval( (Symbol) children.get(0) );
            Symbol member= (Symbol) children.get(1);
            
            if (member instanceof PrimaryNode) {
                currNode= (Node) member;
                children= currNode.getList();
                if (children.size() == 2) {         // Static Method Dispatch
                   
                    // Fetch method, continue if not null
                        String id= (String) ((Symbol) children.get(0)).value;
                    MethodObj method= cls.getMethod(id);
                    if (method != null) {
                        LinkedHashMap<String, String> args= method.getArgs();
                        LinkedList<Node> given= new LinkedList<Node>();

                        // Parse out the given arguments from nested Nodes
                        // Handle Single Case First
                        if (args.size() == 1 && children.size() == 2) {
                            given.addFirst( (Node) children.get(1) );
                        }

                        // Handle multi-case second
                        if (children.size() > 2 && args.size() > 1){
                            Node head= (ActualsListNode) children.get(1);
                            while (!(head instanceof CasesNode)) {
                                given.addFirst( (ExprNode) head.getList().get(1) );
                                head= (Node) head.getList().get(0);
                            }
                            given.addFirst( (ExprNode) head );
                        }

                        // Check that all given args conform to expected args
                        if (given.size() == args.size()) {
                            for (String t2str : args.values()) {
                                ClassObj t1= eval(given.getFirst());
                                ClassObj t2= mtable.getClass(t2str);
                                given.removeFirst();
                                if (!conformsTo(t1, t2)) {
                                    throw new TypeException("Invalid parameters for " + method.getName());
                                }
                            }
                            return mtable.getClass( method.getReturnType() );
                        }
                    }
                    
                } else {                            // Attribute Access
                    // Not sure what do
                }
            }
            throw new TypeException("Unknown member access for " + member.value);
        }
        
        
        if (body instanceof MatchNode) {            // Match 
            currNode= (Node) body;
            children= currNode.getList();
            
            // Parse out all the case nodes
            Node head= (CasesListNode) children.get(1);
            LinkedList<CasesNode> cases= new LinkedList<CasesNode>();
            while (!(head instanceof CasesNode)) {
                cases.addFirst( (CasesNode) head.getList().get(1) );
                head= (Node) head.getList().get(0);
            }
            cases.addFirst( (CasesNode) head );
            
            // Evaluate all case expressions for their types,
            // accounting for the local declarations
            Stack<ClassObj> classStack= new Stack<ClassObj>();
            for (CasesNode node : cases) {
                // If match has its own arguments, load them and evaluate
                if (node.getList().size() == 4) {
                    String name= (String) ((Symbol) node.getList().get(1)).value;
                    String type= (String) ((Symbol) node.getList().get(2)).value;
                    stack.pushClone();
                    stack.addToCurrent(name, type, node);
                    classStack.push( eval( (Symbol) node.getList().get(3) ) );
                    stack.pop();
                // If match's args are null, just evaluate as-is
                } else {
                    classStack.push( eval( (Symbol) node.getList().get(2) ) );
                }
            }
            
            // Apply join to all stack pairs
            while (classStack.size() > 1) {
                classStack.push( join(classStack.pop(), classStack.pop()) );
            }
            
            return classStack.pop();
            
        }
        
        
        // Block Node Types
        if (body instanceof BlockListNode) {
            currNode= (Node) body;
            children= currNode.getList();
            if (children.size() == 0) {         // Empty Block
                return Unit;
            }
            
            // ------------------------------>  // Block Multi-Expr
            LinkedList<Symbol> leaves= new LinkedList<Symbol>();
            Node head= (BlockListNode) body;
            Symbol last= null;
            while (head instanceof BlockListNode) {
                leaves.addFirst( (Symbol) head.getList().get(1) );
                try {
                    head= (Node) head.getList().get(0);
                } catch (Exception e) {
                    last= (Symbol) head;
                    break;
                }
            }
            if (last == null) {
                leaves.addFirst( (Symbol) head );
            } else {
                leaves.addFirst(last);
            }
            
            stack.pushClone();
            ClassObj retclass= null;
            for (Symbol snode: leaves) {
                if (snode instanceof BlockNode) {
                    BlockNode bnode= (BlockNode) snode;
                    String ident= (String) ((Symbol) bnode.getList().get(0)).value;
                    String type= (String) ((Symbol) bnode.getList().get(1)).value;
                    ClassObj t1= mtable.getClass(type);
                    ClassObj t2= eval( (Symbol) bnode.getList().get(2) );
                    if (!conformsTo(t2, t1)) {
                        throw new TypeException("Local variable assignment problem, cannot cast " + t2.getName() + " to " + t1.getName());
                    }
                    stack.addToCurrent(ident, type, bnode);
                    retclass= t1;
                } else {
                    retclass= eval(snode);
                }
            }
            stack.pop();
            return retclass;
            
        }
        /* if (body instanceof BlockNode) {        // Block Local (Let Statement)
            currNode= (Node) body;
            children= currNode.getList();
            String type= (String) ((Symbol) children.get(0)).value;
            ClassObj t1= mtable.getClass(type);
            ClassObj t2= eval( (Symbol) children.get(1) );
            if (conformsTo(t2, t1)) {
                return t1;
            }
            throw new TypeException("Assignment undefined from type " + t2.getName() + " to " + t1.getName());
        }*/
                
        
        // Primary Node Types
        if (body instanceof PrimaryNode) {
            currNode= (Node) body;
            children= currNode.getList();
            if (children.size() == 0) { return null; }  // Empty LPAREN / RPAREN pair
            
            Symbol s1= (Symbol) children.get(0);
            if (children.size() == 1) {
                return eval(s1);
            }
            
            if (s1.getId() == Terminals.NEW) {       // New 
                String type= ((Symbol) children.get(1)).value.toString();
                if (!type.equals("Any") && !type.equals("Integer")
                 && !type.equals("Unit") && !type.equals("Boolean")
                 && !type.equals("String")) {
                    return mtable.getClass(type);
                }
                throw new TypeException("Cannot instantiate class " + type);
            }
            
            if (s1.getId() == Terminals.SUPER) {    // Superclass Dispatch
                
            }
            
            if (s1.getId() == Terminals.ID) {       // Regular Dispatch
                String mname= (String) s1.value;
                ClassObj ths= stack.lookupCurrent("this").type;
                MethodObj method= mtable.getMethod(ths.getName(), mname);
                if (method != null) {
                    LinkedHashMap<String, String> args= method.getArgs();
                    LinkedList<Node> given= new LinkedList<Node>();

                    // Parse out the given arguments from nested Nodes
                    // Handle Single Case First
                    if (args.size() == 1 && children.size() == 2) {
                        given.addFirst( (Node) children.get(1) );
                    }
                    
                    // Handle multi-case second
                    if (children.size() >= 2 && args.size() > 1){
                        Node head= (ActualsListNode) children.get(1);
                        while (head instanceof ActualsListNode) {  // TODO Problem area
                            given.addFirst( (ExprNode) head.getList().get(1) );
                            head= (Node) head.getList().get(0);
                        }
                        given.addFirst( (ExprNode) head );
                    }

                    // Check that all given args conform to expected args
                    if (given.size() == args.size()) {
                        for (String t2str : args.values()) {
                            ClassObj t1= eval(given.getFirst());
                            ClassObj t2= mtable.getClass(t2str);
                            given.removeFirst();
                            if (!conformsTo(t1, t2)) {
                                throw new TypeException("Invalid parameters for " + method.getName());
                            }
                        }
                        return mtable.getClass( method.getReturnType() );
                    }
                }
                
            }
        }
        
        // TODO Block-Expr   
        
        // TODO Attribute Definition
        // TODO Method Override Dispatch
        
        // Undefined Node Subtype
        return Unhandled;
    }
    
    // TODO Implement conformation checking as described in the COOL manual
    public boolean conformsTo(ClassObj c1, ClassObj c2) {
        if (c1 == null || c2 == null) {
            return false;
        }
        
        if (c2.equals(Any)) {
            return true;
        }
        
        while (!c1.equals(Any)) {
            if (c1.equals(c2)) {
                return true;
            }
            c1= mtable.getClass( c1.getParentName() );
        }
        
        return false;
    }
    
    // TODO Implement join checking as described in the COOL Manual
    public ClassObj join(ClassObj c1, ClassObj c2) {
        HashSet<ClassObj> seen= new HashSet<ClassObj>();
        
        while(true) {
            if (seen.contains(c1) && !Any.equals(c1)) {
                return c1;
            }
            seen.add(c1);
            if (!Any.equals(c1)) {
                c1= mtable.getClass( c1.getParentName() );
            }
            
            if (seen.contains(c2) && !Any.equals(c2)) {
                return c2;
            }
            seen.add(c2);
            if (!Any.equals(c2)) {
                c2= mtable.getClass( c2.getParentName() );
            }
            
            if (c1.equals(c2)) {
                return c1;
            }
        }
        
    }
    
}
