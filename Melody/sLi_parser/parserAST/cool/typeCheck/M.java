package cool.typeCheck;

/**
 * Method table for cool parser
 *
 * COOL method rules:
 * //- methods must be unique within a class
 * //- methods can have 0 or more formal parameters
 * //- override must be used to 'override' an extended method
 * //- extended method must contain the same return type
 * //- extended method must contain the same type arguments
 *
 * @author Sze Yan Li
 */
import beaver.Symbol;
import cool.ast.Node;
import cool.ast.ProgramNode;
import cool.ast.TreeWalker;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

public class M {

    public ClassMaster cm;

    /**
     * constructor
     */
    public M() {
        this.cm = new ClassMaster();
    }

    /**
     * extracts the classes from a given program and adds the classes to the method table
     */
    public void add(ProgramNode pgm) {
        this.cm.add(pgm);
    }

    /**
     * builds and fills the method table based on given program files
     */
    public void build() {
        //--build the class hierarchy 
        this.cm.check();

        //--build the methods of each class
        //get the string[] representation of the class hierarchy in cm
        LinkedList classList = this.cm.getClassList();

        //loop through the classes within the classList
        for (int i = 0; i < classList.size(); i++) {
            String className = (String) classList.get(i);

            //skip native because it's not actually a class
            if (className.equals("native")) {
                continue;
            }

            //obtain the ClassObj matched with the className
            ClassObj co = (ClassObj) this.getClass(className);

            //filter through all the FeatureNodes 
            TreeWalker tw = new TreeWalker(co.getNode());
            LinkedList features = tw.filter("FEATURE");

            //loop through each feature and search each method
            for (int m = 0; m < features.size(); m++) {
                Node featureNode = (Node) features.get(m);

                //get the method
                TreeWalker tw2 = new TreeWalker(featureNode);
                Node methodNode = tw2.get("METHOD");

                //if this feature contained a method check the method
                if (methodNode != null) {

                    //check if there's an override node (should be the first child)
                    Boolean overrideExists = false;
                    Symbol orSymbol = (Symbol) featureNode.getList().get(0);
                    if (orSymbol.value.toString().equals("override")) {
                        //an override exists for this method
                        overrideExists = true;
                    }

                    //get the id of the MethodNode (method name) 
                    Symbol methodID = (Symbol) methodNode.getList().get(0); //method name is always the first child of in a MethodNode
                    String methodName = methodID.value.toString();

                    //create a MethodObj with the id
                    MethodObj mo = new MethodObj(methodName);
                    mo.setNode(methodNode);

                    //filter for all the FormalsNodes
                    TreeWalker tw3 = new TreeWalker(methodNode);
                    LinkedList formals = tw3.filter("FORMALS");

                    //for each formal param found, get the id and type
                    for (int k = 0; k < formals.size(); k++) {
                        Node formalsNode = (Node) formals.get(k);

                        //get the id and type (the formal param)
                        Symbol paramID = (Symbol) formalsNode.getList().get(0);
                        String paramName = paramID.value.toString();
                        Symbol typeID = (Symbol) formalsNode.getList().get(1);
                        String typeName = typeID.value.toString();

                        //add id and type as an argument of the method
                        mo.addArg(paramName, typeName);
                    }

                    //get the ReturnTypeNode and set the return type in mo
                    Node returnTypeNode = tw3.get("RETURNTYPE");
                    Symbol rtTypeID = (Symbol) returnTypeNode.getList().get(0);
                    mo.setReturnType(rtTypeID.value.toString());

                    //check to see if this method was defined by the parent class
                    ClassObj coParent = (ClassObj) this.getClass(co.getParentName());

                    //checks for case where the Any extends native
                    if (coParent != null) {
                        //get the method of the parent
                        MethodObj moParent = coParent.getMethod(methodName);

                        //regardless if method was found: check if OVERRIDE keyword was used by subclass
                        if ((overrideExists && moParent == null) || (!overrideExists && moParent != null)) {
                            //if method wasn't found but OVERRIDE was used: error
                            //if method was found but override wasn't used: error
                            System.out.println("OVERRIDE usage incorrect in (" + co.getName() + ", " + mo.getName() + ")");
                            System.exit(1);
                        } else if (overrideExists && moParent != null) {
                            //method was found and OVERRIDE was used

                            //check to make sure parent's return type and child's return types are the same
                            if (!mo.getReturnType().equals(moParent.getReturnType())) {
                                //return types differ
                                System.out.println("(" + co.getName() + ", " + mo.getName() + ")'s return type does not match its super class's: (" + coParent.getName() + ", " + moParent.getName() + ")");
                                System.exit(1);
                            }

                            //get the list of arguments from the parent class
                            LinkedHashMap moParentArgs = moParent.getArgs();

                            //get the list of arguments from this class
                            LinkedHashMap moArgs = mo.getArgs();

                            //check if methods arg size matches
                            if (moArgs.size() != moParentArgs.size()) {
                                //non-matching args
                                System.out.println("(" + co.getName() + ", " + mo.getName() + ")'s arguments do no match its super class's: (" + coParent.getName() + ", " + moParent.getName() + ")");
                                System.exit(1);
                            }

                            //loop through (using parent as a base) and make sure each class's (key,value) and parent's (key,value) pair matches                           
                            Set mpa = moParentArgs.keySet();
                            Iterator itr1 = mpa.iterator();
                            while (itr1.hasNext()) {
                                //get the argument name
                                String parentArgName = (String) itr1.next();
                                //get the matching argument type
                                String parentArgType = (String) moParentArgs.get(parentArgName);

                                Set ma = moArgs.keySet();
                                Iterator itr2 = ma.iterator();
                                while (itr2.hasNext()) {
                                    //get the argument name
                                    String argName = (String) itr2.next();
                                    //get the matching argument type
                                    String argType = (String) moArgs.get(argName);

                                    //check that the arg types are the same
                                    if (!parentArgType.equals(argType)) {
                                        //non-matching args
                                        System.out.println("(" + co.getName() + ", " + mo.getName() + ")'s arguments do no match its super class's: (" + coParent.getName() + ", " + moParent.getName() + ")");
                                        System.exit(1);
                                    }
                                }
                            }
                        }
                    }

                    //check if mo is a unique entry in the list of methods in this class
                    if (co.getMethods().containsKey(methodName)) {
                        //error: this method has already been defined in this class
                        System.out.println("method in class \'" + co.getName() + "\' has already been defined.");
                        System.exit(1);
                    } else {
                        //it's okay to add the method to this class
                        co.addMethod(methodName, mo);
                    }
                }
            }     
        }
    }

    /**
     * @param className name of class
     * @param methodName name of method
     *
     * @return the matching method name. Null if not found
     */
    public MethodObj getMethod(String className, String methodName) {
        return this.getClass(className).getMethod(methodName);
    }

    /**
     * @param className name of the class to find
     *
     * @return the class object matched. Null if not found
     */
    public ClassObj getClass(String className) {
        return (ClassObj) this.cm.getClassMap().get(className);
    }

    /**
     * @return a linked hash map of all the classes
     */
    public LinkedHashMap getClasses() {
        return this.cm.getClassMap();
    }
    
        /**
     * preserving order at all times, prints each class and its methods including the args and return type
     */
    public void printAll() {       
        LinkedList classList = this.cm.getClassList();
        for (int i = 0; i < classList.size(); i++) {
            String className = (String) classList.get(i);

            System.out.println("class: " + className);

            //skip native because it's not actually a class
            if (className.equals("native")) {
                continue;
            }

            //obtain the ClassObj matched with the className
            ClassObj co = (ClassObj) this.getClass(className);

            System.out.println("methods (in order): ");

            Set keys = co.getMethods().keySet();
            Iterator itr = keys.iterator();
            while (itr.hasNext()) {
                String mn = (String) itr.next();
                System.out.println("\t" + mn);

                System.out.print("\t\targs: ");
                System.out.println(co.getMethod(mn).getArgs().toString());
                
                System.out.println("\t\treturn type: "+co.getMethod(mn).getReturnType());
            }
            System.out.println();
        }
    }
}
