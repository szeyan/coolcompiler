package cool.typeCheck;

import cool.ast.Node;

public class VarAttribute {
	public ClassObj parent;
	public ClassObj type;
	public Node node;
	public int address;
	// public CoolClass parent;

	public VarAttribute(ClassObj type, int address,Node node) {
		this.node = node;
		this.address = address;
		this.type = type;
	}
	public VarAttribute(ClassObj type,ClassObj parent, int address,Node node) {
		this.parent = parent;
		this.node = node;
		this.address = address;
		this.type = type;
	}

	public String toString() {
		return " Type: "+ type + "Adress: "+address;
	}
}