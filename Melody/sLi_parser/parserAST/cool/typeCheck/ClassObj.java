package cool.typeCheck;

/**
 * This is a helper file that stores information about a class in the method
 * table
 *
 * @author Sze Yan Li, Kevin Garsjo
 */
import cool.ast.Node;
import java.util.LinkedHashMap;

public class ClassObj {

    private String programName;
    private String name;
    private String parent;
    private LinkedHashMap<String, MethodObj> methods;
    private Node node;

    /**
     * constructor takes in the name of the class and the name of its super
     * class
     *
     * @param name name of class
     * @param parent name of super class
     * @param programName the name of the program in which this class belongs to
     * @param node the class node associated with this class
     */
    public ClassObj(String name, String parent, String programName, Node node) {
        this.programName = programName;
        this.name = name;
        this.parent = parent;
        this.node = node;

        methods = new LinkedHashMap<String, MethodObj>();
    }

    /**
     * Constructor for default types already implemented in cool,such as...
     *  Integer
     *  Boolean
     *  String
     *  Any
     *  Nothing
     *  Null
     * @param name  the name of the class
     */
    public ClassObj(String name) {
        this.name= name;
    }

    /**
     * @return the name of this class
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the name of the super class
     */
    public String getParentName() {
        return this.parent;
    }

    /**
     * @return the name of the program in which this class belongs to
     */
    public String getProgramName() {
        return this.programName;
    }

    /**
     * @return the node matching this class
     */
    public Node getNode() {
        return this.node;
    }

    /**
     * Looks for a method within this class
     *
     * @param methodName the method to look for
     * @return the MethodObj associated with the given method name
     */
    public MethodObj getMethod(String methodName) {
        return this.methods.get(methodName);
    }

    /**
     * @return the LinkedHashMap of methods for this class
     */
    public LinkedHashMap getMethods() {
        return this.methods;
    }

    /**
     * adds a method to the list of methods contained in this class
     *
     * @param methodName name of the method
     * @param mo MethodObj to store
     */
    public void addMethod(String methodName, MethodObj mo) {
        this.methods.put(methodName, mo);
    }

    /**
     * compares this ClassObj and another ClassObj to make sure they are equal
     *
     * @param o the other ClassObj to compare to
     */
    public boolean equals(ClassObj o) {
        if (o == null) { return false; }
        
        boolean result= true;
        result= result && this.name == o.name;
        result= result && this.parent == o.parent;
        result= result && this.node == o.node;
        result= result && this.methods == o.methods;
        result= result && this.programName == o.programName;

        return result;
    }
}
