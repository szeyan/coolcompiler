/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cool.typeCheck;

import cool.ast.Node;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author kgarsjo
 */
public class SymbolStack {
    M mtable;
    
    private Stack<SymbolTable> stack= new Stack<SymbolTable>();
    
    public SymbolStack(M mtable) {
        this.mtable= mtable;
        stack.push( new SymbolTable(mtable) );
    }
    
    public void push(SymbolTable table) {
        stack.push(table);
    }
    
    public void pushClone() {
        if (stack.peek() == null) {
            return;
        }
        
        SymbolTable curr= stack.peek();
        SymbolTable next= new SymbolTable(mtable);
        Set<String> keys= curr.table.keySet();
        for (String key : keys) {
            VarAttribute v= curr.lookup(key);
            VarAttribute add = next.add(key, v.type.getName(), v.node);
        }
        
        stack.push(next);
    }
    
    public SymbolTable pop() {
        return stack.pop();
    }
    
    public void addToCurrent(String name, String type, Node node) {
        SymbolTable curr= stack.peek();
        curr.add(name, type, node);
    }
    
    public VarAttribute lookupCurrent(String name) {
        SymbolTable curr= stack.peek();
        return curr.lookup(name);
    }
    
}
