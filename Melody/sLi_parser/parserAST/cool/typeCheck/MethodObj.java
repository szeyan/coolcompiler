package cool.typeCheck;

import cool.ast.Node;
import java.util.LinkedHashMap;

/**
 * This is a helper file that stores information about a method in the method
 * table
 *
 * @author Sze Yan Li
 */
public class MethodObj {

    private String name;
    private String returnType;
    private LinkedHashMap<String, String> args;
    private Boolean overrides;
    private Boolean returnTypeDeclared;
    private Node    node;

    /**
     * constructor takes in the name of the method
     *
     * @param name name of the method
     */
    public MethodObj(String name) {
        this.name = name;
        this.returnType = "";
        this.overrides = false;
        this.returnTypeDeclared = false;
        this.args = new LinkedHashMap<String, String>();
    }

    /**
     * @return the name of this class
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the return type for this method if the return type has been set else returns an error message
     */
    public String getReturnType() {
        if (this.returnTypeDeclared) {
            return this.returnType;
        }
        return "Return type has not been declared";
    }

    /**
     * @return the list of arguments (already in insertion order)
     */
    public LinkedHashMap getArgs() {
        return this.args;
    }

    /**
     * add an argument to this method
     *
     * @param name of the argument
     * @param type of the argument
     */
    public void addArg(String name, String type) {
        this.args.put(name, type);
    }

    /**
     * sets the return type for this method
     *
     * @param type is the return type for this method
     */
    public void setReturnType(String type) {
        this.returnType = type;
        this.returnTypeDeclared = true;
    }
    
    public Node getNode() {
        return node;
    }
    
    public void setNode(Node n) {
        node= n;
    }
}
