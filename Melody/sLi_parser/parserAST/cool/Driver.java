package cool;

/**
 * A Driver Class for Cool CIS461
 *
 * @author Sze Yan Li
 */
import cool.ast.*;
import cool.typeCheck.ClassObj;
import cool.typeCheck.M;
import cool.typeCheck.SymbolStack;
import cool.typeCheck.TypeCheck2;
import cool.typeCheck.TypeCheck2.TypeException;
import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class Driver {

    public static void main(String[] args) {
        //create method table
        M m = new M();
        SymbolStack stack= new SymbolStack(m);

        for (int i = 0; i < args.length; i++) {
            try {
                // Get tokens and display each on a line
                CoolScanner scanner = new CoolScanner(new FileReader(args[i]));

                //Get the error reporting class from the scanner
                ErrReport err = scanner.getErrReport();
                err.setFileName(args[i]);

                //Create a parser
                CoolParser parser = new CoolParser();
                //retrieve AST from parser
                ProgramNode pgm = (ProgramNode) parser.parse(scanner);
                pgm.setPN(args[i]);

                //walk the ast with a pre-order traversal
                TreeWalker tw = new TreeWalker(pgm);
                //System.out.println(tw.toString());
                //System.out.println(tw.toString(tw.get("PROGRAM")));

                //add classes from this file to the method table
                m.add(pgm);

            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.exit(1);
            }
        }
        m.build();
        
        // Attempt type-checking
        TypeCheck2 typeChecker= new TypeCheck2(m, null, stack);
        try {
            typeChecker.checkType();
        } catch (TypeException e) {
            e.printStackTrace();
        }

        //testing method table:
        //checking Any (native isn't actually a node, so there is no classobj for it)
        //System.out.println(m.getClass("Any").getParentName());
        //see if we can get the parent's name
        //System.out.println(m.getClass("Sieve").getParentName());
        //print the class hierarchy in json (preorder)
        //System.out.println(m.cm.toString());
        //print the class hierarchy as a string array (preorder)
        //System.out.println(m.cm.getClassList().toString());
        //walk a node and print out the tree
        //System.out.println(new TreeWalker(m.getClass("Statistics").getNode()).toString());
        //print all the methods in each class
        m.printAll();
    }
}